package com.example.mrmoney

import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.data.model.entity.Transaction
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

fun main() {
    val text = "2022-01-06 20:30:45"
    val pattern = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")
    val localDateTime = LocalDateTime.parse(text, pattern)
}

fun getDateTime(s: String): String? {
    try {
        val sdf = SimpleDateFormat("MM/dd/yyyy")
        val netDate = Date((s.toLong() * 1000))
        return sdf.format(netDate)
    } catch (e: Exception) {
        return e.toString()
    }
}