package com.example.mrmoney

import android.app.Application
import android.os.Build
import android.util.Log
import android.util.SparseBooleanArray
import androidx.annotation.RequiresApi
import androidx.lifecycle.*
import com.example.mrmoney.data.model.DateFilterType
import com.example.mrmoney.data.model.ExchangeRate
import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.data.model.entity.*
import com.example.mrmoney.data.respository.DBConnection
import com.example.mrmoney.utils.CurrencyUltils
import com.example.mrmoney.utils.DateUtils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.launch
import java.text.NumberFormat
import java.time.LocalDate
import java.time.temporal.TemporalAdjusters
import java.time.temporal.TemporalField
import java.time.temporal.WeekFields
import java.util.*
import kotlin.collections.ArrayList

@RequiresApi(Build.VERSION_CODES.O)
class RevenueAnalysisViewModel(application: Application) : AndroidViewModel(application) {
    private val database = FirebaseDatabase.getInstance()
    private val dbRootRef = database.reference
    private val firebaseAuth = FirebaseAuth.getInstance()
    private var currentUser = firebaseAuth.currentUser
    var transactions = MutableLiveData<ArrayList<Transaction?>?>()
    var categories = MutableLiveData<ArrayList<Category?>?>()

    var revenueTotal = MutableLiveData<Double>()
    var expenseTotal = MutableLiveData<Double>()
    var itemStateArray = MutableLiveData<SparseBooleanArray>()

    var categoryFilter = MutableLiveData<ArrayList<Category>>()
    var walletFilters = MutableLiveData<ArrayList<Wallet>>()
    var startDay = MutableLiveData<LocalDate>()
    var endDay = MutableLiveData<LocalDate>()
    var submit = MutableLiveData<Boolean>()

    init {
        categoryFilter.value = ArrayList()
        itemStateArray.value = SparseBooleanArray(0)
        categories.value = ArrayList()
        startDay.value = LocalDate.now().minusDays(7)
        endDay.value = LocalDate.now()
        getTransactions()
        getCategory()
    }


    fun setStartDate(date: LocalDate) {
        startDay.value = date
    }

    fun setEndDate(date: LocalDate) {
        endDay.value = date
    }

    fun getTransactions() {
        viewModelScope.launch {
            dbRootRef.child("transactions")
                .child(currentUser!!.uid)
                .addValueEventListener(object : ValueEventListener {
                    @RequiresApi(Build.VERSION_CODES.O)
                    override fun onDataChange(snapshot: DataSnapshot) {
                        var arr = ArrayList<Transaction?>()
                        if (snapshot.exists()) {
                            for (ds: DataSnapshot in snapshot.children) {
                                var temp = ds.getValue(Transaction::class.java)
                                arr.add(temp)
                                Log.i("HomeFagment", "get transaction sucess")
                            }

                            filterByRange(arr, startDay.value!!, endDay.value!!)
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.d("thong", "${error.details}")
                    }
                })

        }
    }

    fun getCategory() {
        viewModelScope.launch {
            dbRootRef.child("categories")
                .child(currentUser!!.uid)
                .addValueEventListener(object : ValueEventListener {
                    @RequiresApi(Build.VERSION_CODES.O)
                    override fun onDataChange(snapshot: DataSnapshot) {
                        var arr = ArrayList<Category?>()
                        if (snapshot.exists()) {
                            for (ds: DataSnapshot in snapshot.children) {
                                var temp = ds.getValue(Category::class.java)
                                arr.add(temp)
                            Log.i("HomeFagment", temp.toString())
                            }
                            categories.value = arr
                            itemStateArray.value = SparseBooleanArray(arr.size)
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.d("thong", "${error.details}")
                    }
                })

        }
    }

    fun onSubmitButtonClick() {
        getTransactions()
    }

    fun setFilterWallet(wallets: LiveData<ArrayList<Wallet>>) {
        wallets.value?.let {
            walletFilters.value?.removeAll(walletFilters.value!!)
            walletFilters.value = ArrayList()
            walletFilters.value!!.addAll(it)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun filterByRange(lst: ArrayList<Transaction?>?, from: LocalDate, to: LocalDate) {
        val arr = lst

        if (arr != null) {
            var expense = 0.0
            var revenue = 0.0
            for (it in arr) {
                val date = DateUtils.covertTimestampToDatetime(it!!.time)

                Log.i("HomeFragment", date.toString())
                if (date.toLocalDate().isBefore(from) && date.toLocalDate().isAfter(to) && filterByCate(it.category)) {
                    arr!!.remove(it)
                    continue
                }
                if (it.transactionType.equals(TransactionType.EXPENSE.name))
                    expense += it.amount
                if (it.transactionType.equals(TransactionType.INCOME.name))
                    revenue += it.amount
            }

            revenueTotal.value = revenue
            expenseTotal.value = expense
            transactions.value = arr
        } else transactions.value = null

    }

    fun onDateFilterClick(menuItem: MenuItem) {
        transactions.value = null
        expenseTotal.value = 0.0
        revenueTotal.value = 0.0
        getTransactions()
    }

    fun setCateFilter(){
        categoryFilter.value = ArrayList()

        for(i in 0..categories.value!!.size-1){
            if(itemStateArray.value!!.get(i, false)){
                categoryFilter.value!!.add(categories.value!!.get(i)!!)
            }
        }
    }

    fun setOnSubmitFilter(){
        submit.value = true
    }

    fun filterByCate(category: String): Boolean{
        categoryFilter.value?.let { arr->
            for (it in arr){
                if(it.name.equals(category, true)) return true
            }
            return false
        }
        return true
    }
}