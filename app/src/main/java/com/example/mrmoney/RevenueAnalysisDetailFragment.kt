package com.example.mrmoney

import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.mrmoney.databinding.FragmentRevenueAnalysisDetailBinding
import com.example.mrmoney.utils.DateUtils
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*


class RevenueAnalysisDetailFragment : Fragment() {

    private lateinit var binding: FragmentRevenueAnalysisDetailBinding
    private lateinit var navController: NavController

    companion object {
        fun newInstance() = RevenueAnalysisDetailFragment()
    }

    private lateinit var viewModel: RevenueAnalysisViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRevenueAnalysisDetailBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(RevenueAnalysisViewModel::class.java)

        binding.revenAnalToolbar.setNavigationIcon(R.drawable.ic_round_arrow_back_24)
        binding.revenAnalToolbar.setOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.layoutAnalysisDetail.etStartDate.setOnClickListener {
            clickDatePicker(viewModel.startDay)
        }
        binding.layoutAnalysisDetail.etEndDate.setOnClickListener {
            clickDatePicker(viewModel.endDay)
        }

        viewModel.startDay.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            val day = it.dayOfMonth.toInt()
            val month = it.month.value
            val year = it.year
            binding.layoutAnalysisDetail.etStartDate.setText("$day/$month/$year")
        })

        viewModel.endDay.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            val day = it.dayOfMonth.toInt()
            val month = it.month.value
            val year = it.year
            binding.layoutAnalysisDetail.etEndDate.setText("$day/$month/$year")
        })

        binding.layoutAnalysisDetail.etCategory.setOnClickListener {
            navController.navigate(R.id.action_revenueAnalysisDetailFragment_to_fragmentChoseCategory)
        }

        binding.layoutAnalysisDetail.btnSubmit.setOnClickListener {
            viewModel.getTransactions()
            setBarChartExpense()
        }

        viewModel.submit.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                if (it){
                    viewModel.setCateFilter()
                    var str: String = ""
                    for (it in viewModel.categoryFilter.value!!){
                        str = str+ it.name+", "
                    }
                    binding.layoutAnalysisDetail.etCategory.setText("")
                    binding.layoutAnalysisDetail.etCategory.setText(str)
                }

//                viewModel.submit.value = false


        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setBarChartExpense(){

        val entries: ArrayList<BarEntry> = ArrayList()

        var map = HashMap<String, Double>()

        val xAxisLabel: ArrayList<String> = ArrayList()

        for(it in viewModel.transactions.value!!){
            val key = DateUtils.covertTimestampToDatetime(it!!.time).toLocalDate().toString()

            if(map.containsKey(key)){
                map.put(key, map.getValue(key)+it.amount)
            }
            else {
                map.put(key, it.amount)
                xAxisLabel.add(key)
            }
        }

        xAxisLabel.add("")

        var count = 1
        for (i in map) {
            println(count)
            val barEntry = BarEntry(
                (count++).toFloat(),
                i.value.toFloat()
            ) //start always from x=1 for the first bar
            entries.add(barEntry)
        }

        binding.layoutAnalysisDetail.bcDetail.xAxis.setAxisMaximum(entries.size + 0.5f);
        binding.layoutAnalysisDetail.bcDetail.xAxis.setValueFormatter(object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                println(value)
                return xAxisLabel[value.toInt()]
            }
        })
//        for (i in 1 until 12){
//            if(map.containsKey(i)){
//                entries.add(BarEntry(i.toFloat(), map.getValue(i).toFloat()))
//            }
//            else entries.add(BarEntry(i.toFloat(), 0f))
//        }



        val barDataSet = BarDataSet(entries, "")
        barDataSet.setColors(*ColorTemplate.COLORFUL_COLORS)

        val data = BarData(barDataSet)
        binding.layoutAnalysisDetail.bcDetail.data = data

        binding.layoutAnalysisDetail.bcDetail.xAxis.setValueFormatter(object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return xAxisLabel[value.toInt()]
            }
        })

        binding.layoutAnalysisDetail.bcDetail.xAxis.enableGridDashedLine(10f, 10f, 0f);
        binding.layoutAnalysisDetail.bcDetail.xAxis.setTextSize(14f);
        binding.layoutAnalysisDetail.bcDetail.xAxis.setDrawAxisLine(true);


        binding.layoutAnalysisDetail.bcDetail.xAxis.setGranularity(1f);
        binding.layoutAnalysisDetail.bcDetail.xAxis.setGranularityEnabled(true);
        binding.layoutAnalysisDetail.bcDetail.xAxis.setAxisMinimum(0 + 0.5f); //

        //hide grid lines
        binding.layoutAnalysisDetail.bcDetail.axisLeft.setDrawGridLines(false)
        binding.layoutAnalysisDetail.bcDetail.xAxis.setDrawGridLines(false)
        binding.layoutAnalysisDetail.bcDetail.xAxis.setDrawAxisLine(false)

        //remove right y-axis
        binding.layoutAnalysisDetail.bcDetail.axisRight.isEnabled = false

        //remove legend
        binding.layoutAnalysisDetail.bcDetail.legend.isEnabled = false


        //remove description label
        binding.layoutAnalysisDetail.bcDetail.description.isEnabled = false


        //add animation
        binding.layoutAnalysisDetail.bcDetail.animateY(3000)

        binding.layoutAnalysisDetail.bcDetail.xAxis.position = XAxis.XAxisPosition.BOTTOM

        //draw chart
        binding.layoutAnalysisDetail.bcDetail.invalidate()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun clickDatePicker(date: MutableLiveData<LocalDate>) {
        val myCalendar = Calendar.getInstance()
        val year = myCalendar.get(Calendar.YEAR)
        val month = myCalendar.get(Calendar.MONTH)
        val day = myCalendar.get(Calendar.DAY_OF_MONTH)

        val dp = DatePickerDialog(
            requireContext(),
            { view, _year, _month, _day ->
                val selectedDate = "$_day/${_month + 1}/$_year"
                val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
                date.value = LocalDate.of(_year, _month, _day)
                val theDate = sdf.parse(selectedDate)
            },
            year, month, day
        )
        dp.show()
    }

}