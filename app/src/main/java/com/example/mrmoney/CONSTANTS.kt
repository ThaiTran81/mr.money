package com.example.mrmoney

const val SPLASH_DELAY     =  3000
const val REQ_CODE_GG      =  123

const val CATEGORY_UTIL = 0
const val EXPENSE_LIMIT_UTIL = 1
const val LIST_SHOPPING_UTIL = 2
const val EXCHANGE_LOOKUP = 3
const val SPLIT_BILL = 4
const val SAVINGS_DEPOSIT = 5