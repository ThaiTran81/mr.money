package com.example.mrmoney.ui.fragment.other

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.mrmoney.databinding.FragmentInterestCalculatorBinding
import com.example.mrmoney.databinding.FragmentSavingDepositBinding
import com.example.mrmoney.utils.CurrencyUltils
import kotlin.math.pow

class InterestCalculatorFragment : Fragment() {
    private lateinit var binding: FragmentInterestCalculatorBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInterestCalculatorBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var type = InterestCalculatorFragmentArgs.fromBundle(requireArguments()).type


//        Công thức tính lãi suất kép trong toán học
//
//        https://www.topcv.vn/tinh-lai-kep?tracking=1&source=gg&campaign_name=TOPCV_SEO_ADS&gclid=Cj0KCQjwpcOTBhCZARIsAEAYLuVzVJ95wwXVljj655DvriyKQzMVsT0EW-ZZa5vj3Y6mXs87cDqLl8YaAiHqEALw_wcB
//
//        Trong đó:
//
//        Fn là giá trị của khoản đầu tư trong khoảng thời gian n năm mà bạn nhận được.
//        P là giá trị khoản đầu tư hiện tại của bạn.
//        i là lãi suất hàng năm của khoản đầu tư đó. Ví dụ lãi suất 10%/năm, thì i được hiểu là 0,1.
//        n là số năm bạn dự tính đầu tư.
//        m là số lần ghép lãi trong 1 năm, nếu lãi nhận hàng năm thì m là 1.

        var P: Double
        var i: Double
        var m = 12.0
        var n: Double
        var Fn: Double

        if (type == 0) {
            binding.calculatorNameTV.text = "Tính số tiền nhận được cuối kỳ"
            binding.moneyInputLabelTV.text = "Số tiền gốc ban đầu"
            binding.calculatedMoneyLableTV.text = "Số tiền nhận được cuối kỳ"

            binding.calBtn.setOnClickListener {
                P = binding.moneyETNumberDecimal.text.toString().toDouble()
                i = binding.interestETNumber.text.toString().toDouble()/100
                n = binding.monthETNumber.text.toString().toDouble()/12
                Fn = P*(1 + (i/m)).pow(n*m)
                binding.calculatedMoneyValueTV.setText(Fn.toInt().toString())
                binding.totalProfitTV.setText((Fn.toInt() - P.toInt()).toString())
            }
        }
        else {
            binding.calculatorNameTV.text = "Tính số tiền gốc cần gửi"
            binding.moneyInputLabelTV.text = "Số tiền nhận được cuối kỳ"
            binding.calculatedMoneyLableTV.text = "Số tiền gốc cần gửi"
            binding.moneyETNumberDecimal.setTextColor(Color.parseColor("#33C9FF"))
            binding.calculatedMoneyValueTV.setTextColor(Color.parseColor("#FF3378"))
            binding.linearLayout2.visibility = View.GONE

            binding.calBtn.setOnClickListener {
                Fn = binding.moneyETNumberDecimal.text.toString().toDouble()
                i = binding.interestETNumber.text.toString().toDouble()/100
                n = binding.monthETNumber.text.toString().toDouble()/12
                P = Fn/(1 + (i/m)).pow(n*m)
                binding.calculatedMoneyValueTV.setText(CurrencyUltils.moneyFormatter(P,"VND"))
            }
        }


    }
}