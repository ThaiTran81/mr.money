package com.example.mrmoney.ui.fragment.signUp

import android.app.Application
import android.net.Uri
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.mrmoney.R
import com.example.mrmoney.data.model.entity.Category
import com.example.mrmoney.data.model.entity.User
import com.example.mrmoney.data.respository.CategoryRespository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage

class SignUpViewModel(private val myApplication: Application): AndroidViewModel(myApplication) {
    private val firebaseAuth = FirebaseAuth.getInstance()
    private val rootNode = FirebaseDatabase.getInstance().reference

    var fullname: String = ""
    var email: String = ""
    var password: String = ""

    private val _fullNameError = MutableLiveData<String>()
    val fullNameError: LiveData<String>
        get() = _fullNameError

    private val _emailError = MutableLiveData<String>()
    val emailError: LiveData<String>
        get() = _emailError

    private val _passwordError = MutableLiveData<String>()
    val passwordError: LiveData<String>
        get() = _passwordError

    private val _buttonEnabled = MutableLiveData<Boolean>().apply { value = true }
    val buttonEnabled: LiveData<Boolean>
        get() = _buttonEnabled

    private val _navigateToHome = MutableLiveData<Boolean>().apply { value = false }
    val navigateToHome: LiveData<Boolean>
        get() = _navigateToHome

    private val _signUpError = MutableLiveData<String>()
    val signUpError: LiveData<String>
        get() = _signUpError

    fun createUser() {
        when {
            email.isEmpty() -> _emailError.value =
                myApplication.getString(R.string.email_required_error)
            fullname.isEmpty() -> _fullNameError.value =
                myApplication.getString(R.string.name_required_error)
            password.isEmpty() -> _passwordError.value =
                myApplication.getString(R.string.password_required_error)
            !Patterns.EMAIL_ADDRESS.matcher(email).matches() -> _emailError.value =
                myApplication.getString(R.string.malformed_email_error)
            password.length < 6 -> _passwordError.value =
                myApplication.getString(R.string.short_password_error)
            else -> {
                _buttonEnabled.value = false
                firebaseAuth.createUserWithEmailAndPassword(email, password).addOnSuccessListener {
                    saveUserToDatabase()
                }.addOnFailureListener {
                    it.printStackTrace()
                    Toast.makeText(myApplication, "${it.message}", Toast.LENGTH_SHORT).show()
                    _signUpError.value = it.message
                    _buttonEnabled.value = true

                }
            }
        }
    }

    private fun saveUserToDatabase() {
        Log.d("thong", "db")
        val uid = firebaseAuth.currentUser!!.uid.toString()
        val user = User(
            uid,
            email,
            fullname,
            "password",
            true
        )
        rootNode.child("users").child(uid).setValue(user).addOnSuccessListener {
            for(cate: Category in Category.BigCategory) {
                CategoryRespository.getInstance().addCategory(cate)
            }
            _navigateToHome.value = true
        }.addOnFailureListener {
            _signUpError.value = it.message
            _buttonEnabled.value = true
            Log.d("thong", "db fail")
        }
    }
}