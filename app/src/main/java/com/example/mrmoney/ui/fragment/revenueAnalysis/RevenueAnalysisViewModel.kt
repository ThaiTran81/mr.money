package com.example.mrmoney.ui.fragment.revenueAnalysis

import android.app.Application
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mrmoney.R
import com.example.mrmoney.data.model.DateFilterType
import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.data.model.entity.Transaction
import com.example.mrmoney.utils.DateUtils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.temporal.TemporalAdjusters
import java.time.temporal.TemporalField
import java.time.temporal.WeekFields
import java.util.*

class RevenueAnalysisViewModel(application: Application): AndroidViewModel(application) {

    val selectedFilter = MutableLiveData<Int?>()
    var revenueTotal = MutableLiveData<Double>()
    var expenseTotal = MutableLiveData<Double>()
    var transactions = MutableLiveData<ArrayList<Transaction?>>()
    private val database = FirebaseDatabase.getInstance()
    private val dbRootRef = database.reference
    private val firebaseAuth = FirebaseAuth.getInstance()
    private var currentUser = firebaseAuth.currentUser



    init {
        getTransactions()
    }

    fun initFilter(idFilter: Int){
        selectedFilter.value = idFilter

    }
    @RequiresApi(Build.VERSION_CODES.O)
    fun filterTransactionByDate(lst : ArrayList<Transaction?>?) {
        when(selectedFilter.value){
            DateFilterType.TODAY.ordinal -> filterByRange(lst, LocalDate.now(), LocalDate.now())
            DateFilterType.THIS_WEEK.ordinal -> {
                val now = LocalDate.now()
                val fieldISO: TemporalField = WeekFields.of(Locale.getDefault()).dayOfWeek()
                filterByRange(lst,now.with(fieldISO, 1), now.with(fieldISO, 1).plusDays(6))
            }
            DateFilterType.THIS_MONTH.ordinal ->{
                val now = LocalDate.now()
                val start = now.withDayOfMonth(1)
                val end = now.withDayOfMonth(now.month.length(now.isLeapYear))
                filterByRange(lst,start, end)
            }
            DateFilterType.THIS_YEAR.ordinal ->{
                val now = LocalDate.now()
                val firstDay = now.with(TemporalAdjusters.firstDayOfYear())
                val lastDay = now.with(TemporalAdjusters.lastDayOfYear())

                filterByRange(lst,firstDay, lastDay)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun filterByRange(lst : ArrayList<Transaction?>?, from: LocalDate, to: LocalDate){
        val arr = lst

        if (arr != null) {
            var expense = 0.0
            var revenue = 0.0
            for (it in arr) {
                val date = DateUtils.covertTimestampToDatetime(it!!.time)

                Log.i("RevenAndExpenseFrag", date.toString())
                if (date.toLocalDate().isBefore(from) && date.toLocalDate().isAfter(to)) {
                    arr!!.remove(it)
                    continue
                }
                if (it.transactionType.equals(TransactionType.EXPENSE.name))
                    expense += it.amount
                if (it.transactionType.equals(TransactionType.INCOME.name))
                    revenue += it.amount
            }

            revenueTotal.value = revenue
            expenseTotal.value = expense
            transactions.value = arr!!
        }
        else transactions.value = null

    }

    fun getTransactions() {
        viewModelScope.launch {
            dbRootRef.child("transactions")
                .child(currentUser!!.uid)
                .addValueEventListener(object : ValueEventListener {
                    @RequiresApi(Build.VERSION_CODES.O)
                    override fun onDataChange(snapshot: DataSnapshot) {
                        var arr = ArrayList<Transaction?>()
                        if (snapshot.exists()) {
                            for (ds: DataSnapshot in snapshot.children) {
                                var temp = ds.getValue(Transaction::class.java)
                                arr.add(temp)
                                Log.i("HomeFagment", "get transaction sucess")
                            }
                            filterTransactionByDate(arr)
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.d("thong", "${error.details}")
                    }
                })

        }
    }
}