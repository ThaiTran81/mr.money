package com.example.mrmoney.ui.fragment.loan

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.adapter.LoanDetailRVAdapter
import com.example.mrmoney.data.model.LoanType
import com.example.mrmoney.data.model.LoanerType
import com.example.mrmoney.data.model.entity.Loan
import com.example.mrmoney.data.model.entity.LoanDetail
import com.example.mrmoney.databinding.LoanDetailBinding
import com.example.mrmoney.utils.CurrencyUltils
import java.text.DateFormat
import java.text.NumberFormat
import java.util.*

class LoanDetailFragment: Fragment() {

    companion object {
        fun newInstance() = LoanDetailFragment()
    }

    private lateinit var viewModel: LoanDetailViewModel
    private lateinit var binding: LoanDetailBinding
    private lateinit var manager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LoanDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val itemDecoration: RecyclerView.ItemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)

        manager = LinearLayoutManager(context)

        var data = LoanDetail.createDataSample()

        var adapter = LoanDetailRVAdapter(setData(data, LoanDetailFragmentArgs.fromBundle(requireArguments()).loanerName, LoanDetailFragmentArgs.fromBundle(requireArguments()).loanerType))

        binding.loanDetailLoanerNameTV.text = LoanDetailFragmentArgs.fromBundle(requireArguments()).loanerName



        binding.loanerDetailRV.apply {
            this.adapter = adapter
            layoutManager = manager
            addItemDecoration(itemDecoration)
        }
    }

    fun setSummaryData(data :List<LoanDetail>, loanerType: LoanerType) {
        var hasAmount: Double = 0.0
        var totalAmount: Double = 0.0


        if (loanerType == LoanerType.BORROWING) {
            for (item in data) {
                if (item.type == LoanType.BORROW) {
                    totalAmount += item.amount
                }
                if (item.type == LoanType.BORROW_PAY) {
                    hasAmount += item.amount
                }
            }
        }
        else {
            for (item in data) {
                if (item.type == LoanType.LEND) {
                    totalAmount += item.amount
                }
                if (item.type == LoanType.LEND_PAY) {
                    hasAmount += item.amount
                }
            }
        }

        val percent: Int = ((hasAmount/totalAmount) * 100).toInt()

        if (loanerType == LoanerType.LENDING) {
            "Tổng cho vay".also { binding.totalLoanTV.text = it }
            "Đã thu ($percent%)".also { binding.paidTV.text = it }
            "Cần thu".also { binding.remainTV.text = it }
            "Thu nợ".also { binding.payBtn.text = it }
        }
        else {
            "Tổng đi vay".also { binding.totalLoanTV.text = it }
            ("Đã trả ($percent%)").also { binding.paidTV.text = it }
            "Phải trả".also { binding.remainTV.text = it }
            "Trả nợ".also { binding.payBtn.text = it }
        }

        binding.remainValue.text = CurrencyUltils.moneyFormatter(totalAmount - hasAmount, "VND")
        binding.paidValue.text = CurrencyUltils.moneyFormatter(hasAmount, "VND")
        binding.totalLoanValue.text = CurrencyUltils.moneyFormatter(totalAmount, "VND")
    }

    fun setData(data :List<LoanDetail>, loanerName: String, loanerType: LoanerType): ArrayList<LoanDetail> {
        var final_data = ArrayList<LoanDetail>()

        for (item in data) {
            if (item.name == loanerName && item.loaner == loanerType) {
                final_data.add(item)
            }
        }
        setSummaryData(final_data, loanerType)

        return final_data
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(LoanDetailViewModel::class.java)
    }
}