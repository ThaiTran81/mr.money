package com.example.mrmoney.ui.fragment.Account

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.mrmoney.R
import com.example.mrmoney.adapter.AccountVP2Adapter
import com.example.mrmoney.databinding.FragmentAccountBinding
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

/**
 * A simple [Fragment] subclass.
 * Use the [AccountFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AccountFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var _binding: FragmentAccountBinding? = null
    private val binding get() = _binding!!

    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        inflater.inflate(R.layout.fragment_account, container, false)
        _binding =  FragmentAccountBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var accountVP2Adapter: AccountVP2Adapter = AccountVP2Adapter(activity);
        binding.viewPager2.adapter = accountVP2Adapter;

        navController = Navigation.findNavController(view)

        binding.fabAddItem.setOnClickListener {
//            navController.navigate(R.id.action_accountFragment_to_addAccountFragment)
            when(binding.tabLayout.selectedTabPosition){
                0 -> navController.navigate(R.id.action_accountFragment_to_addAccountFragment)
                1 -> navController.navigate(R.id.action_accountFragment_to_addBankBookFragment)
                2 -> navController.navigate(R.id.action_accountFragment_to_addAccumulationFragment)
            }

        }

        binding.accountToolbar.imgSearch.setOnClickListener {
            binding.accountToolbar.imgBack.visibility = View.VISIBLE
            binding.accountToolbar.tietContentSearch.visibility = View.VISIBLE
            binding.accountToolbar.imgSearch.visibility = View.INVISIBLE
            binding.accountToolbar.tvTitle.visibility = View.INVISIBLE

        }

        binding.accountToolbar.imgBack.setOnClickListener {
            binding.accountToolbar.imgBack.visibility = View.INVISIBLE
            binding.accountToolbar.tietContentSearch.visibility = View.INVISIBLE
            binding.accountToolbar.imgSearch.visibility = View.VISIBLE
            binding.accountToolbar.tvTitle.visibility = View.VISIBLE
        }



        TabLayoutMediator(binding.tabLayout, binding.viewPager2, TabLayoutMediator.TabConfigurationStrategy { tab, position ->
            when(position){
                0 ->{
                    tab.text = "Tài Khoản"
                    tab.setIcon(R.drawable.ic_balance_account)
                }
                1 ->{
                    tab.text = "Sổ Tiết Kiệm"
                    tab.setIcon(R.drawable.ic_balance_account)

                }
                2 ->{
                    tab.text = "Tích Lũy"
                    tab.setIcon(R.drawable.ic_balance_account)

                }
            }

        }).attach()

        binding.tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                when(tab!!.position){
                    0->{
                        binding.accountToolbar.
                        tietContentSearch.
                        hint = "Tìm kiếm theo tên tài khoản..."
                    }
                    1->{
                        binding.accountToolbar.
                        tietContentSearch.
                        hint = "Tìm kiếm theo tên sổ tiết kiệm..."
                    }
                    else->{
                        binding.accountToolbar.
                        tietContentSearch.
                        hint = "Tìm kiếm theo tên tích lũy..."
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                Log.d("ignoreTag", "")
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                Log.d("ignoreTag", "")

            }

        })

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}