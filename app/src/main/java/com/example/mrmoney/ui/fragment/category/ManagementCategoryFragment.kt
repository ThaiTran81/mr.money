package com.example.mrmoney.ui.fragment.category

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.mrmoney.R
import com.example.mrmoney.adapter.CategoryVP2Adapter
import com.example.mrmoney.databinding.FragmentAccountBinding
import com.example.mrmoney.databinding.FragmentManagementCategoryBinding
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ManagementCategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ManagementCategoryFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private var _binding: FragmentManagementCategoryBinding? = null
    private val binding get() = _binding!!
    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentManagementCategoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var categoryVP2Adapter: CategoryVP2Adapter = CategoryVP2Adapter(activity)
        binding.viewPager2.adapter = categoryVP2Adapter

        navController = Navigation.findNavController(view)

        binding.fabAddItem.setOnClickListener {
            var action = ManagementCategoryFragmentDirections.actionManagementCategoryFragmentToCategoryAdditionFragment()
            when(binding.tabLayout.selectedTabPosition){
                0 -> action.setTypeCategory("INCOME")
                else -> action.setTypeCategory("EXPENSE")
            }
            navController.navigate(R.id.action_managementCategoryFragment_to_categoryAdditionFragment)

        }

        TabLayoutMediator(binding.tabLayout, binding.viewPager2, TabLayoutMediator.TabConfigurationStrategy { tab, position ->
            when(position){
                0 ->{
                    tab.text = "Mục thu"
                    tab.setIcon(R.drawable.ic_balance_account)
                }
                1 ->{
                    tab.text = "Mục chi"
                    tab.setIcon(R.drawable.ic_balance_account)
                }
            }

        }).attach()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}