package com.example.mrmoney.ui.fragment.Transaction.spend

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mrmoney.databinding.SpendingTransactionFragmentBinding

class SpendingTransactionFragment : Fragment() {

    companion object {
        fun newInstance() = SpendingTransactionFragment()
    }



    private lateinit var viewModel: SpendingTransactionViewModel
    private var _binding: SpendingTransactionFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SpendingTransactionFragmentBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SpendingTransactionViewModel::class.java)
        // TODO: Use the ViewModel
    }

}