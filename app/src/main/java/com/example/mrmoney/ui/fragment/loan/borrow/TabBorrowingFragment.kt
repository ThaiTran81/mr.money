package com.example.mrmoney.ui.fragment.loan.borrow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.adapter.LoanerAdapter
import com.example.mrmoney.data.model.LoanType
import com.example.mrmoney.data.model.LoanerType
import com.example.mrmoney.data.model.entity.LoanDetail
import com.example.mrmoney.data.model.entity.Loaner
import com.example.mrmoney.databinding.TabBorrowingFragmentBinding
import com.example.mrmoney.ui.fragment.loan.LoanFragmentDirections
import com.example.mrmoney.utils.CurrencyUltils
import java.util.*

class TabBorrowingFragment : Fragment() {

    companion object {
        fun newInstance() = TabBorrowingFragment()
    }

    private lateinit var viewModel: TabBorrowingViewModel
    private lateinit var binding: TabBorrowingFragmentBinding
    private lateinit var borrowing_manager: RecyclerView.LayoutManager
    private lateinit var borrowed_manager: RecyclerView.LayoutManager

    private var borrowing_data = ArrayList<Loaner>()
    private var borrowed_data = ArrayList<Loaner>()

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = TabBorrowingFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val itemDecoration: RecyclerView.ItemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        viewModel = ViewModelProvider(this).get(TabBorrowingViewModel::class.java)
        /*var data = LoanDetail.createDataSample()
        setData(data)*/
        navController = findNavController()

        viewModel.getBorrowingDataFromDB()

        viewModel.paidBorrow.observe(viewLifecycleOwner,{
            binding.borrowingHasValueTV.text = CurrencyUltils.moneyFormatter(it,"VND")
            binding.borrowingSummaryPB.progress = it.toInt()
        })

        viewModel.totalBorrow.observe(viewLifecycleOwner,{
            binding.borrowingTotalSummaryValueTV.text = CurrencyUltils.moneyFormatter(it,"VND")
            binding.borrowingSummaryPB.max = it.toInt()
        })

        viewModel.needPaidBorrow.observe(viewLifecycleOwner,{
            binding.borrowingMustValueTV.text = CurrencyUltils.moneyFormatter(it,"VND")
        })


        borrowing_manager = LinearLayoutManager(context)
        viewModel.borrowing.observe(viewLifecycleOwner,{
            var borrowing_adapter = LoanerAdapter(it)

            borrowing_adapter.onItemClick = { loaner ->
                var action = LoanFragmentDirections.actionLoanFragmentToLoanDetailFragment(loaner.name, LoanerType.BORROWING)
                navController.navigate(action)
            }

            binding.borrowingRV.apply {
                this.adapter = borrowing_adapter
                layoutManager = borrowing_manager
                addItemDecoration(itemDecoration)
            }
        })

    }

    fun setSummaryData(data: List<LoanDetail>) {
        var hasAmount: Double = 0.0
        var totalAmount: Double = 0.0

        for (item in data) {
            if (item.type == LoanType.BORROW) {
                totalAmount += item.amount
            }
            if (item.type == LoanType.BORROW_PAY) {
                hasAmount += item.amount
            }
        }

        binding.borrowingMustValueTV.text = CurrencyUltils.moneyFormatter(totalAmount - hasAmount, "VND")
        binding.borrowingHasValueTV.text = CurrencyUltils.moneyFormatter(hasAmount, "VND")
        binding.borrowingTotalSummaryValueTV.text = CurrencyUltils.moneyFormatter(totalAmount, "VND")
        binding.borrowingSummaryPB.max = totalAmount.toInt()
        binding.borrowingSummaryPB.progress = hasAmount.toInt()
    }

    fun setData(data :List<LoanDetail>) {
        var names = ArrayList<String>()
        var temp_data = ArrayList<LoanDetail>()

        for (item in data) {
            if (item.loaner == LoanerType.BORROWING) {
                temp_data.add(item)
                names.add(item.name)
            }
        }
        names = names.distinct() as ArrayList<String>

        setSummaryData(temp_data)
        var hasAmount: Double = 0.0
        var totalAmount: Double = 0.0

        for (name in names) {
            for (item in temp_data) {
                if (item.name == name) {
                    if (item.type == LoanType.BORROW) {
                        totalAmount += item.amount
                    }
                    if (item.type == LoanType.BORROW_PAY) {
                        hasAmount += item.amount
                    }
                }
            }
            /*if (totalAmount - hasAmount > 0.0) {
                borrowing_data.add(Loaner(name, Loan(totalAmount - hasAmount, hasAmount, totalAmount, LoanerType.BORROWING)))
            }
            else {
                borrowed_data.add(Loaner(name, Loan(0.0, hasAmount, totalAmount, LoanerType.BORROWING)))
            }*/
            totalAmount = 0.0
            hasAmount = 0.0
        }
    }

//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//        viewModel = ViewModelProvider(this).get(TabBorrowingViewModel::class.java)
//    }
}