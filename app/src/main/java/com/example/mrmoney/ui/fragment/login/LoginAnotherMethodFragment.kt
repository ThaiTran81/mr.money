package com.example.mrmoney.ui.fragment.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.mrmoney.R
import com.example.mrmoney.REQ_CODE_GG
import com.example.mrmoney.data.model.entity.Category
import com.example.mrmoney.data.model.entity.User
import com.example.mrmoney.data.respository.CategoryRespository
import com.example.mrmoney.data.respository.UserRespository
import com.example.mrmoney.databinding.FragmentLoginAnotherMethodBinding
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.FacebookSdk
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.firestore.QuerySnapshot

class LoginAnotherMethodFragment: Fragment() {
    private lateinit var binding: FragmentLoginAnotherMethodBinding
    private lateinit var firebaseAuth: FirebaseAuth
    // google login
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    // fb login
    private lateinit var mCallbackManager: CallbackManager

    private val rootNode = FirebaseDatabase.getInstance().reference

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginAnotherMethodBinding.inflate(inflater, container, false)
        firebaseAuth = FirebaseAuth.getInstance()

        mCallbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().registerCallback(mCallbackManager,
            object: FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    Log.d("thong","--${loginResult.accessToken.token.toString()}")
                    binding.frameLayout2.visibility = View.VISIBLE
                    val credential = FacebookAuthProvider.getCredential(loginResult.accessToken.token)
                    firebaseHandleAuth(credential)
                }

                override fun onCancel() {
                    Log.d("thong","fb login cancel")
                    binding.frameLayout2.visibility = View.GONE
                }

                override fun onError(exception: FacebookException) {
                    Log.d("thong",exception.toString())
                    binding.frameLayout2.visibility = View.GONE
                }
            }
        )

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), gso)

        binding.ggLogin.setOnClickListener { view ->
            binding.frameLayout2.visibility = View.VISIBLE
            signInGoogle()

        }

        binding.fbLogin.setOnClickListener {
            binding.frameLayout2.visibility = View.VISIBLE
            LoginManager.getInstance().logInWithReadPermissions(this, arrayListOf("email","public_profile"))
        }

        binding.frameLayout2.visibility = View.GONE
        
        return binding.root
    }

    fun signInGoogle() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, REQ_CODE_GG)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (FacebookSdk.isFacebookRequestCode(requestCode)) {

            mCallbackManager.onActivityResult(requestCode,resultCode,data)
        } else if (requestCode == REQ_CODE_GG) {

            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            firebaseAuthWithGoogle(task)
        }
    }

    private fun firebaseAuthWithGoogle(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)!!
            if (account != null) {

                val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                firebaseHandleAuth(credential)
            }
        } catch (e: ApiException) {
            Toast.makeText(requireContext(), e.toString(), Toast.LENGTH_SHORT).show()
        }
    }



    private fun firebaseHandleAuth(credential: AuthCredential) {
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val firebaseUser = firebaseAuth.currentUser
                    val uid = firebaseUser!!.uid
                    val email = firebaseUser!!.email.toString()
                    val name = firebaseUser!!.displayName.toString()
                    rootNode.child("users").child(uid).addValueEventListener(
                        object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                if(!snapshot.exists()){
                                    val user = User(
                                        uid,
                                        email,
                                        name
                                    )
                                    rootNode.child("users").child(uid).setValue(user)
                                    for(cate: Category in Category.BigCategory) {
                                        CategoryRespository.getInstance().addCategory(cate)
                                    }
                                }else{
                                    val u = snapshot.getValue(User::class.java)
                                }
                                Log.d("thong","${UserRespository.getInstance().getCurUser()}")
                                findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
                            }
                            override fun onCancelled(error: DatabaseError) {
                                TODO("Not yet implemented")
                            }

                        }
                    )
                }else {
                    Log.w("thong", "signInWithCredential:failure", task.exception)
                }
            }
    }
}