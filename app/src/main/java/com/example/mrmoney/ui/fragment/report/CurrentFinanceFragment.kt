package com.example.mrmoney.ui.fragment.report

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.mrmoney.R
import com.example.mrmoney.databinding.FragmentCurrentFinancesBinding
import com.example.mrmoney.ui.fragment.revenueAndExpenditureSituation.RevenueAndExpenditureSituationViewModel

class CurrentFinanceFragment: Fragment() {
    private lateinit var binding: FragmentCurrentFinancesBinding
    private lateinit var viewModel: CurrentFinanceViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCurrentFinancesBinding.inflate(layoutInflater)
        return  binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        viewModel = ViewModelProvider(this).get(CurrentFinanceViewModel::class.java)
        val toolbar = binding.curFinanceToolbar

        toolbar.setNavigationIcon(R.drawable.ic_round_arrow_back_24)
        toolbar.setNavigationOnClickListener{
            activity?.onBackPressed()
        }
        super.onViewCreated(view, savedInstanceState)
    }
}