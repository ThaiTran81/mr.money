package com.example.mrmoney.ui.fragment.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mrmoney.data.model.entity.User
import com.example.mrmoney.utils.MyApplication
import com.facebook.login.LoginResult
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class LoginViewModel(): ViewModel() {
    private var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private var db: FirebaseFirestore = FirebaseFirestore.getInstance()

    var username: String = ""
        set(value) {
            field = value
            validateInput()
        }

    var password: String = ""
        set(value) {
            field = value
            validateInput()
        }

    private val _buttonEnabled = MutableLiveData<Boolean>()
    private fun validateInput() {
        _buttonEnabled.value = !(username.isEmpty() || password.isEmpty())
    }

    private val _progress = MutableLiveData<Boolean>()
    val progress: LiveData<Boolean>
        get() = _progress

    private val _errorString = MutableLiveData<String>()
    val errorString: LiveData<String>
        get() = _errorString

    private val _navigateToHome = MutableLiveData<Boolean>()
    val navigateToHome: LiveData<Boolean>
        get() = _navigateToHome

    private val _navigateToSignUp = MutableLiveData<Boolean>()
    val navigateToSignUp: LiveData<Boolean>
        get() = _navigateToSignUp

    val buttonEnabled: LiveData<Boolean>
        get() = _buttonEnabled

    fun login() {
        onStartLoading()
        mAuth.signInWithEmailAndPassword(username, password).addOnCompleteListener {
            if (it.isSuccessful) {
                getFirebaseUserData()
            } else {
                onFinishLoading()
                _errorString.value = it.exception?.message
            }
        }

    }

    fun navigateToHome() {
        _navigateToHome.value = true
    }

    fun doneHomeNavigation() {
        _navigateToHome.value = false
    }

    fun navigateToSignUp() {
        _navigateToSignUp.value = true
    }

    fun doneSignUpNavigation() {
        _navigateToSignUp.value = false
    }

    fun onStartLoading() {
        Log.d("thong","on loading")
        _buttonEnabled.value = true
        _progress.value = true
    }

    fun onFinishLoading() {
        _buttonEnabled.value = true
        _progress.value = false
    }

    fun setError(error: String) {
        _errorString.value = error
    }

    private fun getFirebaseUserData() {
        val ref = db.collection("users").document(mAuth.currentUser!!.uid)
        ref.get().addOnSuccessListener {
            val userInfo = it.toObject(User::class.java)
            MyApplication.currentUser = userInfo
            MyApplication.currentUser!!.active = true
            onFinishLoading()
            navigateToHome()
        }.addOnFailureListener {
            onFinishLoading()
            _errorString.value = it.message
        }
    }

    fun handleFacebookToken(result: LoginResult) {
        onStartLoading()
        val credential =
            FacebookAuthProvider.getCredential(result.accessToken.token.toString())
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val userId = task.result!!.user!!.uid
                    Log.d("thong","---------------$userId")
                } else {
                    onFinishLoading()
                    _errorString.value = "Authentication failed."
                }
            }
    }
}