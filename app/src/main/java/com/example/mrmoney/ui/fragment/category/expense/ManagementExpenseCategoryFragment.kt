package com.example.mrmoney.ui.fragment.category.expense

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.adapter.CategoryRVAdapter
import com.example.mrmoney.data.model.entity.Category
import com.example.mrmoney.databinding.FragmentManagementTypeCategoryBinding
import com.example.mrmoney.ui.fragment.category.CategoryAdditionFragmentArgs
import com.example.mrmoney.ui.fragment.category.CategoryEditorFragmentArgs
import com.example.mrmoney.ui.fragment.category.CategorySelectionViewModel
import com.example.mrmoney.ui.fragment.category.ManagementCategoryFragmentDirections
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class ManagementExpenseCategoryFragment: Fragment() {

    private lateinit var mCategories: ArrayList<Category>
    private var dbRootRef = Firebase.database.reference
    private var firebaseAuth = FirebaseAuth.getInstance()

    private lateinit var manager: RecyclerView.LayoutManager
    private var _binding: FragmentManagementTypeCategoryBinding? = null
    private val binding get() = _binding!!
    private lateinit var navController: NavController
    private lateinit var categorySelectionViewModel: CategorySelectionViewModel


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentManagementTypeCategoryBinding.inflate(inflater, container, false)
        Log.d("CategoryDebugData", "1")

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("CategoryDebugData", "1")

        navController = findNavController()

        categorySelectionViewModel =
            ViewModelProvider(requireActivity()).get(CategorySelectionViewModel::class.java)
                    as CategorySelectionViewModel

        getCategoryFromFirebase()


        manager = LinearLayoutManager(context)

        categorySelectionViewModel =
            ViewModelProvider(requireActivity()).get(CategorySelectionViewModel::class.java)
                    as CategorySelectionViewModel

        getCategoryFromFirebase()



    }

    private fun getCategoryFromFirebase() {
        dbRootRef.child("categories")
            .child(firebaseAuth.currentUser!!.uid)
            .addValueEventListener(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    mCategories = ArrayList()
                    Log.d("SonDeBug", "here")
                    for(ds: DataSnapshot in snapshot.children){
                        val temp: Category? = ds.getValue(Category::class.java)
                        Log.d("SonDeBug", temp.toString())

                        if(temp!!.transactionType.toString().equals("LEND")||temp!!.transactionType.toString().equals("EXPENSE")){
                            mCategories!!.add(temp!!)
                        }
                    }

                    val adapter = CategoryRVAdapter(mCategories,requireActivity())
                    binding.rcvCategory.layoutManager = manager
                    binding.rcvCategory.adapter = adapter
                    adapter.setOnItemClickListener(object: CategoryRVAdapter.onItemClicklistener{
                        override fun onItemClick(item: Category) {
                            var action = ManagementCategoryFragmentDirections.
                            actionManagementCategoryFragmentToCategoryEditorFragment()
                                .setCategory(item)
                            navController.navigate(action)
                        }
                    })


                    if(mCategories!!.size != 0) {
//                        checkParentCate()
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.d("SonDebug", "failed")
                }
            })
    }




    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}