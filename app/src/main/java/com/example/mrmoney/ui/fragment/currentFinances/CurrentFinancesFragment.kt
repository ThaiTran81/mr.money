package com.example.mrmoney.ui.fragment.currentFinances

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.adapter.ReportAdapter
import com.example.mrmoney.data.model.entity.Report
import com.example.mrmoney.databinding.FragmentReportBinding
import com.example.mrmoney.ui.fragment.Account.TabAccumulation.TabAccumulationViewModel
import com.example.mrmoney.ui.fragment.loan.LoanFragment


class CurrentFinancesFragment: Fragment() {

    companion object {
        fun newInstance() = CurrentFinancesFragment()
    }

    private lateinit var viewModel: CurrentFinancesViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_current_finances, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(CurrentFinancesViewModel::class.java)
        // TODO: Use the ViewModel
    }
}