package com.example.mrmoney.ui.fragment.splash

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.mrmoney.adapter.SlideAdapter
import com.example.mrmoney.databinding.FragmentSplashBinding
import com.example.mrmoney.utils.Prefs
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class SplashFragment: Fragment() {
    private val slideAdapter: SlideAdapter = SlideAdapter()
    private lateinit var viewModel: SplashViewModel
    private lateinit var binding: FragmentSplashBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(SplashViewModel::class.java)
        binding = FragmentSplashBinding.inflate(inflater, container, false)
        binding.splashViewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.dataSet.observe(viewLifecycleOwner, Observer {
            binding.viewPager2.adapter = slideAdapter
            slideAdapter.setItems(it)
            TabLayoutMediator(
                binding.indicator,
                binding.viewPager2,
                object : TabLayoutMediator.TabConfigurationStrategy {
                    override fun onConfigureTab(tab: TabLayout.Tab, position: Int) {
                        binding.viewPager2.setCurrentItem(tab.position, true)
                    }
                }).attach()
        })
        viewModel.startNavigation.observe(viewLifecycleOwner, Observer {
            if (it) {
                this.findNavController()
                    .navigate(SplashFragmentDirections.actionSplashFragmentToAuthFragment())
                Prefs.getInstance(requireContext())!!.hasCompletedWalkthrough = false
                viewModel.doneNavigation()
            }
        })
        binding.viewPager2.registerOnPageChangeCallback(viewModel.pagerCallBack)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.viewPager2.unregisterOnPageChangeCallback(viewModel.pagerCallBack)
    }
}