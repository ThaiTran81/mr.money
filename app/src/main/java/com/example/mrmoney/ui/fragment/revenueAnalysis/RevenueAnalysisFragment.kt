package com.example.mrmoney.ui.fragment.revenueAnalysis

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.mrmoney.R
import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.databinding.TabRevenAndExpenseBinding
import com.example.mrmoney.utils.CurrencyUltils
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import java.util.*
import kotlin.collections.ArrayList


class RevenueAnalysisFragment(val filter: Int, val type: TransactionType) : Fragment() {

    companion object {
        fun newInstance(filter: Int, type: TransactionType) = RevenueAnalysisFragment(filter, type)
    }

    private lateinit var viewModel: RevenueAnalysisViewModel
    private lateinit var binding: TabRevenAndExpenseBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = TabRevenAndExpenseBinding.inflate(inflater, container, false)
        if (type == TransactionType.INCOME) binding.tvTotal.text = "Tổng thu"
        else binding.tvTotal.text = "Tổng chi"
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(RevenueAnalysisViewModel::class.java)
        viewModel.initFilter(filter)
        viewModel.expenseTotal.observe(viewLifecycleOwner, Observer {
            if (type == TransactionType.EXPENSE) {
                binding.tvTotal.text = CurrencyUltils.moneyFormatter(it, "VND")
            }
        })
        viewModel.revenueTotal.observe(viewLifecycleOwner, Observer {
            if (type == TransactionType.INCOME) {
                binding.tvTotal.text = CurrencyUltils.moneyFormatter(it, "VND")
            }
        })
        viewModel.transactions.observe(viewLifecycleOwner, Observer {
            setDataPieChart()
        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setDataPieChart() {
        if (viewModel.transactions.value == null) {
            binding.pcTab.clear()
            return
        }
        val pieEntries: ArrayList<PieEntry> = ArrayList()
        val label = ""

        //initializing data
        val typeAmountMap: MutableMap<String, Double> = TreeMap(Collections.reverseOrder())
        for (it in viewModel.transactions.value!!) {
            if (it!!.transactionType != type.name) continue
            if (typeAmountMap.containsKey(it!!.category)) {
                typeAmountMap.put(it.category, typeAmountMap.getValue(it.category) + it.amount)
            } else {
                typeAmountMap.put(it.category, it.amount)
            }
        }


        val amountMap: MutableMap<String, Double> = TreeMap(Collections.reverseOrder())
        amountMap.put("Khác", 0.0)
        var count = 0

        for (it in typeAmountMap) {
            if (count < 4) {
                amountMap.put(it.key, it.value)
                count++
            } else {
                amountMap.put("Khác", amountMap.getValue("Khác") + it.value)

            }
        }

        for (it in amountMap) {
            if (type == TransactionType.EXPENSE)
                it.setValue(it.value / viewModel.expenseTotal.value!! * 100)
            else
                it.setValue(it.value / viewModel.revenueTotal.value!! * 100)
        }

        //initializing colors for the entries
        val colors: ArrayList<Int> = ArrayList()
        colors.add(Color.parseColor("#0450b4"))
        colors.add(Color.parseColor("#fea802"))
        colors.add(Color.parseColor("#d94a8c"))
        colors.add(Color.parseColor("#fe7434"))
        colors.add(Color.parseColor("#38b000"))
        colors.add(Color.parseColor("#6fb1a0"))
        colors.add(Color.parseColor("#15a2a2"))


        val value_colors: ArrayList<Int> = ArrayList()
        value_colors.add(Color.parseColor("#ffffff"))
        //input data and fit data into pie chart entry
        for (type in amountMap.keys) {
            pieEntries.add(PieEntry(amountMap[type]!!.toFloat(), type))
        }

        //collecting the entries with label name
        val pieDataSet = PieDataSet(pieEntries, label)
        //setting text size of the value
        pieDataSet.valueTextSize = 16f
        pieDataSet.formSize = 14f
        //providing color list for coloring different entries
        pieDataSet.colors = colors
        pieDataSet.valueTextColor = Color.parseColor("#ffffff")
        //grouping the data set from entry to chart
        val pieData = PieData(pieDataSet)
        //showing the value of the entries, default true if not set
        pieData.setDrawValues(true)
        binding.pcTab.description.isEnabled = false


        binding.pcTab.data = pieData
        binding.pcTab.invalidate()
    }
}