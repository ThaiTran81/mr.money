package com.example.mrmoney.ui.fragment.Account.TabAccount
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.navigation.NavController
import com.example.mrmoney.R
import com.example.mrmoney.data.model.entity.Transaction
import com.example.mrmoney.data.model.entity.Wallet
import com.example.mrmoney.data.respository.WalletRespository
import com.example.mrmoney.databinding.FragmentAddAccountBinding
import com.example.mrmoney.databinding.FragmentAddAccumulationBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class AddAccountFragment : Fragment() {

    companion object {
        fun newInstance() = AddAccountFragment()
    }

    private var  _binding: FragmentAddAccountBinding? = null
    private val binding get() = _binding!!
    private lateinit var navController:NavController

    private lateinit var viewModel: AddAccountViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddAccountBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var spinnerTypeAccountArray = arrayListOf<String>("Ví tiền mặt",
                                                        "Ví điện tử",
                                                        "Thẻ tín dụng")
        var spinnerTypeAccountAdapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_dropdown_item,spinnerTypeAccountArray)
        binding.spinnerTypeSourceAccount.adapter = spinnerTypeAccountAdapter

        binding.addAccountAppbar.imgCancel.setOnClickListener {
            activity?.onBackPressed()
        }

        binding.addAccountAppbar.imgCheck.setOnClickListener {
            addNewWallet()
        }

        binding.btnSave.setOnClickListener{
            addNewWallet()
        }
    }

    private fun addNewWallet() {
        if(validateInput() == true){
            var newWallet = getNewWallet()

            Firebase.database.reference
                .child("wallets")
                .child(FirebaseAuth.getInstance().currentUser!!.uid)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        for (ds : DataSnapshot in snapshot.children) {
                            var temp = ds.getValue(Wallet::class.java)
                            Log.d("thong","${temp.toString()}")
                            if(temp!!.name.lowercase().equals(newWallet.name.lowercase())){
                                Toast.makeText(requireContext(),"Tên ví đã tồn tại",Toast.LENGTH_SHORT).show()
                                return
                            }
                        }
                        WalletRespository.getInstance().addNewWallet(newWallet)
                        activity?.onBackPressed()
                        Toast.makeText(requireActivity(), "Thêm tài khoản thành công", Toast.LENGTH_SHORT).show()
                    }
                    override fun onCancelled(error: DatabaseError) {
                        Log.d("thong", "${error.details}")
                    }
                })
        }
    }

    private fun validateInput(): Boolean{
        if(binding.tietBalance.text!!.trim().length == 0){
            Toast.makeText(requireActivity(),"Nhập số dư ban đầu", Toast.LENGTH_SHORT).show()
            return false
        }else if(binding.tietNameAccount.text!!.trim().length == 0){
            Toast.makeText(requireActivity(),"Nhập tên tài khoản", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    private fun getNewWallet():Wallet{
        return Wallet(
            binding.tietNameAccount.text.toString(),
            binding.tietBalance.text.toString().toDouble(),
            0,
            binding.tietDescription.text.toString(),
        )
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this).get(AddAccountViewModel::class.java)
        // TODO: Use the ViewModel
    }

}