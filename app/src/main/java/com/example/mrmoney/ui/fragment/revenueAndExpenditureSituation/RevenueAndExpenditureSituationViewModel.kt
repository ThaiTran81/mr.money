package com.example.mrmoney.ui.fragment.revenueAndExpenditureSituation

import android.app.Application
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mrmoney.R
import com.example.mrmoney.data.model.DateFilterType
import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.data.model.entity.Menu
import com.example.mrmoney.data.model.entity.Transaction
import com.example.mrmoney.utils.DateUtils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.temporal.TemporalAdjusters
import java.time.temporal.TemporalField
import java.time.temporal.WeekFields
import java.util.*

class RevenueAndExpenditureSituationViewModel(application: Application): AndroidViewModel(application) {

}