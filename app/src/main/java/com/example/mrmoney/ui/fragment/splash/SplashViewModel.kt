package com.example.mrmoney.ui.fragment.splash

import android.app.Application
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.viewpager2.widget.ViewPager2
import com.example.mrmoney.data.model.SlideSplash
import com.example.mrmoney.R

class SplashViewModel(private val myApplication: Application): AndroidViewModel(myApplication) {
    private val list = listOf(
        SlideSplash(
            ContextCompat.getDrawable(myApplication.applicationContext, R.drawable.splash_img1)!!,
            "Chào mừng đến với MrMoney",
            "Trải nghiệm niềm vui quản lý tài chính."
        ),
        SlideSplash(
            ContextCompat.getDrawable(myApplication.applicationContext, R.drawable.splash_img2)!!,
            "Chào mừng đến với MrMoney",
            "Trải nghiệm niềm vui quản lý tài chính."
        ),
        SlideSplash(
            ContextCompat.getDrawable(myApplication.applicationContext, R.drawable.splash_img3)!!,
            "Chào mừng đến với MrMoney",
            "Trải nghiệm niềm vui quản lý tài chính."
        )
    )

    private val _dataSet = MutableLiveData<List<SlideSplash>>().apply { value = list }
    val dataSet: LiveData<List<SlideSplash>>
        get() = _dataSet

    private val _buttonVisiability = MutableLiveData<Boolean>().apply { value = false }
    val buttonVisiability: LiveData<Boolean>
        get() = _buttonVisiability

    val pagerCallBack = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            _buttonVisiability.value = position == list.size - 1
            super.onPageSelected(position)
        }
    }
    private val _startNavigation = MutableLiveData<Boolean>().apply { value = false }
    val startNavigation: LiveData<Boolean>
        get() = _startNavigation

    fun navigateToAuth() {
        _startNavigation.value = true
    }

    fun doneNavigation() {
        _startNavigation.value = false
    }
}