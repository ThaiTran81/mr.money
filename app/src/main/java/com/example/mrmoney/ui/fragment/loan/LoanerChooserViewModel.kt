package com.example.mrmoney.ui.fragment.loan

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LoanerChooserViewModel: ViewModel() {


     public var selectItem: MutableLiveData<String> = MutableLiveData<String>()


    fun setSelected(data: String){
        selectItem.value = data
        Log.d("thong","set ${selectItem.value}")
    }

    public fun getSelectedItem(): LiveData<String> {
        Log.d("thong","get ${selectItem.value}")
        return selectItem
    }
}