package com.example.mrmoney.ui.fragment.loan.borrow

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mrmoney.data.model.entity.Loaner
import com.example.mrmoney.data.model.entity.Transaction
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class TabBorrowingViewModel(application: Application) : AndroidViewModel(application) {
    private val database = FirebaseDatabase.getInstance()
    private val dbRootRef = database.reference
    private val firebaseAuth = FirebaseAuth.getInstance()
    private var currentUser = firebaseAuth.currentUser

    var totalBorrow = MutableLiveData<Double>()
    var paidBorrow = MutableLiveData<Double>()
    var needPaidBorrow = MutableLiveData<Double>()
    var borrowing = MutableLiveData< ArrayList<Loaner>>()
    var borrowed = MutableLiveData< ArrayList<Loaner>>()

    fun getBorrowingDataFromDB() {
        dbRootRef.child("transactions")
            .child(currentUser!!.uid)
            .orderByChild("transactionType")
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    var total = 0.0
                    var borrower = ArrayList<Transaction>()
                    var loander = HashMap<String,Double?>()
                    var loandered = HashMap<String,Double?>()
                    for(ds : DataSnapshot in snapshot.children) {
                        var temp = ds.getValue(Transaction::class.java)
                        if(temp!!.transactionType.equals("BORROW")) {
                            total += temp!!.amount
                            borrower.add(temp)
                            if (!loander.containsKey(temp.person)) {
                                loander[temp.person] = 0.0
                            }
                            loander[temp.person] = loander[temp.person]?.plus(temp.amount)
                        }
                    }
                    totalBorrow.value = total
                    dbRootRef.child("transactions")
                        .child(currentUser!!.uid)
                        .orderByChild("transactionType")
                        .addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                var paid = 0.0
                                for(ds : DataSnapshot in snapshot.children) {
                                    var tempDS = ds.getValue(Transaction::class.java)
                                    if(tempDS!!.transactionType.equals("EXPENSE")) {
                                        if (tempDS!!.category.equals("Trả nợ")) {
                                            paid += tempDS.amount
                                            if (loander.containsKey(tempDS.person)) {
                                                loander[tempDS.person] =
                                                    loander[tempDS.person]?.minus(tempDS.amount)
                                            } else if (loandered.containsKey(tempDS.person)) {
                                                loandered[tempDS.person] =
                                                    loander[tempDS.person]?.plus(tempDS.amount)
                                            } else {
                                                loandered[tempDS.person] = tempDS.amount
                                            }
                                        }
                                    }
                                }
                                paidBorrow.value = paid
                                needPaidBorrow.value = total - paid

                                if(borrowing.value != null)
                                    borrowing.value!!.clear()
                                else
                                    borrowing.value = ArrayList<Loaner>()
                                for(l in loander) {
                                    borrowing.value!!.add(Loaner(l.key,l.value!!))
                                }
                                if(borrowed.value != null)
                                    borrowed.value!!.clear()
                                else
                                    borrowed.value = ArrayList<Loaner>()
                                for(l in loandered) {
                                    borrowed.value!!.add(Loaner(l.key,l.value!!,true))
                                }

                            }
                            override fun onCancelled(error: DatabaseError) {
                                Log.d("thong", "${error.details}")
                            }
                        })
                }
                override fun onCancelled(error: DatabaseError) {
                    Log.d("thong", "${error.details}")
                }
            })
    }
}