package com.example.mrmoney.ui.fragment.Account.TabAccount

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.adapter.AccountRVAdapter
import com.example.mrmoney.adapter.DataModel
import com.example.mrmoney.data.model.entity.Category
import com.example.mrmoney.data.model.entity.Wallet
import com.example.mrmoney.databinding.FragmentAddAccumulationBinding
import com.example.mrmoney.databinding.TabAccountFragmentBinding
import com.example.mrmoney.ui.fragment.Account.AccountFragmentDirections
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class TabAccountFragment : Fragment() {

    companion object {
        fun newInstance() = TabAccountFragment()
    }
    private var  _binding: TabAccountFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var viewModel: TabAccountViewModel


    private lateinit var mListWallet: ArrayList<Wallet>
    private lateinit var manager: RecyclerView.LayoutManager
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = TabAccountFragmentBinding.inflate(layoutInflater)


        return binding.root
//        return inflater.inflate(R.layout.tab_account_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        manager = LinearLayoutManager(context)
        navController = findNavController()

        Firebase.database.reference.child("wallets")
            .child(FirebaseAuth.getInstance().uid.toString())
            .addValueEventListener(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    mListWallet = ArrayList()
                    for(ds: DataSnapshot in snapshot.children){
                        ds.getValue(Wallet::class.java)?.let {
                            mListWallet!!.add(it)
                            Log.d("SonDebug", "" + it.toString())
                        }
                    }

                    val adapter = AccountRVAdapter(mListWallet,0)
                    binding.tabAccountRcv.layoutManager = manager
                    binding.tabAccountRcv.adapter = adapter
                    adapter.setOnItemClickListener(object: AccountRVAdapter.onItemClickListener{

                        override fun onItemClick(item: Any) {
                            var walletClicked = item as Wallet
                            Log.d("SonDebug", "" + walletClicked)
//                            navController.navigate(AccountFragmentDirections.actionAccountFragmentToAddAccountFragment())
//                            navController.navigate(R.id.action_accountFragment_to_detailAccountFragment)

                            navController.navigate(AccountFragmentDirections.actionAccountFragmentToDetailAccountFragment().setDetailWallet(item as Wallet))
                        }

                        override fun onMoreVertClick(item: Any) {
                            Log.d("SonDebug", "")

                        }
                    })
                }
                override fun onCancelled(error: DatabaseError) {
                    Log.d("SonDebug", "failed")
                }

            })



    }



}