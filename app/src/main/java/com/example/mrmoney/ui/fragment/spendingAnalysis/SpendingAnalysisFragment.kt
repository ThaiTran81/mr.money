package com.example.mrmoney.ui.fragment.spendingAnalysis

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.mrmoney.R


class SpendingAnalysisFragment: Fragment() {

    companion object {
        fun newInstance() = SpendingAnalysisFragment()
    }

    private lateinit var viewModel: SpendingAnalysisViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_spending_analysis, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SpendingAnalysisViewModel::class.java)
        // TODO: Use the ViewModel
    }
}