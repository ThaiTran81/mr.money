package com.example.mrmoney.ui.fragment.other

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mrmoney.R
import com.example.mrmoney.adapter.CurrencyAdapter
import com.example.mrmoney.adapter.ExchangeAdapter
import com.example.mrmoney.data.model.ExchangeRate
import com.example.mrmoney.databinding.FragmentCurrencyConvertBinding
import com.example.mrmoney.utils.CurrencyUltils
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*


class CurrencyConvertFragment : Fragment() {

    companion object {
        fun newInstance() = CurrencyConvertFragment()
    }

    private lateinit var viewModel: CurrencyConvertViewModel


    lateinit var binding: FragmentCurrencyConvertBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCurrencyConvertBinding.inflate(inflater, container, false)
        binding.layoutAppbar.toolbar.setTitle("Tra cứu tỷ giá")

        binding.layoutAppbar.toolbar.setNavigationIcon(R.drawable.ic_round_arrow_back_24)
        binding.layoutAppbar.toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(CurrencyConvertViewModel::class.java)
        // TODO: Use the ViewModel

        viewModel.countryLst.observe(viewLifecycleOwner, Observer {
            binding.spFromCountry.adapter = CurrencyAdapter(it, requireContext())
            binding.spToCountry.adapter = CurrencyAdapter(it, requireContext())
            binding.layoutExchangeLookup.rvExchangeLookup.adapter =
                ExchangeAdapter(ExchangeRate("VN", "VND", 24152.525053, 0), it)
            binding.layoutExchangeLookup.rvExchangeLookup.layoutManager =
                LinearLayoutManager(requireContext())
        })

        binding.etFromCountry.addTextChangedListener {
            val input = binding.etFromCountry.text.toString()
            if (input.isNotBlank()) {
                viewModel.onInput(input.toDouble())
                val selectedUnit = binding.spFromCountry.selectedItem as ExchangeRate
            }
            else viewModel.onInput(0.0)
        }

        viewModel.input.observe(viewLifecycleOwner, Observer {
            val unitFrom = binding.spFromCountry.selectedItem as ExchangeRate
            val unitTo = binding.spToCountry.selectedItem as ExchangeRate
            val res = it * ( unitTo.rate!!/unitFrom.rate!!)
            viewModel.onOutput(res)
        })

        viewModel.output.observe(viewLifecycleOwner, Observer {
            val unitTo = binding.spToCountry.selectedItem as ExchangeRate
            binding.etToCountry.setText(CurrencyUltils.moneyFormatter(it!!, unitTo.currencyCode))
        })


        binding.spFromCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val input = viewModel.input.value
                input?.let {
                    viewModel.onInput(it)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.i("Ignore","")

            }

        }
        binding.spToCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val input = viewModel.input.value
                input?.let {
                    viewModel.onInput(it)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.i("Ignore","")
            }

        }

    }

}