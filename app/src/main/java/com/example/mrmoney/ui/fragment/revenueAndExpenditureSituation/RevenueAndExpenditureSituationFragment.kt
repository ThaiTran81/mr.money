package com.example.mrmoney.ui.fragment.revenueAndExpenditureSituation

import android.app.Application
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import com.example.mrmoney.R
import com.example.mrmoney.adapter.AnalysisVPAdapter
import com.example.mrmoney.data.model.DateFilterType
import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.databinding.FragmentRevenueAndExpenditureSituationBinding
import com.example.mrmoney.ui.fragment.revenueAnalysis.RevenueAnalysisFragment
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator


class RevenueAndExpenditureSituationFragment : Fragment() {

    companion object {
        fun newInstance() = RevenueAndExpenditureSituationFragment()
    }

//    var args: RevenueAndExpenditureSituationFragmentArgs by navArgs<RevenueAndExpenditureSituationFragmentArgs>()

    private lateinit var viewModel: RevenueAndExpenditureSituationViewModel

    private lateinit var binding: FragmentRevenueAndExpenditureSituationBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentRevenueAndExpenditureSituationBinding.inflate(
            inflater, container, false
        )


//        val bundle = this.arguments
        val arguments = RevenueAndExpenditureSituationFragmentArgs.fromBundle(requireArguments())

//        Toast.makeText(requireContext(),arguments.selectedFilter.toString(),Toast.LENGTH_SHORT).show()





        val adapter = AnalysisVPAdapter(requireActivity().supportFragmentManager, requireActivity().lifecycle)
        adapter.add(RevenueAnalysisFragment(arguments.selectedFilter, TransactionType.EXPENSE), "CHI")
        adapter.add(RevenueAnalysisFragment(arguments.selectedFilter, TransactionType.INCOME), "THU")

        binding.viewPager.adapter = adapter

        TabLayoutMediator(binding.tabs, binding.viewPager) { tab, position ->
            tab.text = adapter.fragmentTitleList.get(position)
        }.attach()

        binding.viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL

        binding.revenAndExpenToolbar.setNavigationIcon(R.drawable.ic_round_arrow_back_24)
        binding.revenAndExpenToolbar.setOnClickListener {
            requireActivity().onBackPressed()
        }

        val title = when(arguments.selectedFilter){
            DateFilterType.TODAY.ordinal -> requireActivity().getString(R.string.today)
            DateFilterType.THIS_WEEK.ordinal -> requireActivity().getString(R.string.this_week)
            DateFilterType.THIS_MONTH.ordinal -> requireActivity().getString(R.string.this_month)
            DateFilterType.THIS_YEAR.ordinal -> requireActivity().getString(R.string.this_year)
            else -> {""}
        }

        binding.revenAndExpenToolbar.setTitle(title)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(RevenueAndExpenditureSituationViewModel::class.java)
        // TODO: Use the ViewModel


    }
}