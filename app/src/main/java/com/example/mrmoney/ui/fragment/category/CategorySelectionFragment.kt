package com.example.mrmoney.ui.fragment.category

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.adapter.CategoryRVAdapter
import com.example.mrmoney.adapter.ExpandapleListViewAdapter
import com.example.mrmoney.data.model.entity.Category
import com.example.mrmoney.databinding.FragmentCategorySelectionBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase


class CategorySelectionFragment : Fragment() {

    companion object {
        fun newInstance() = CategorySelectionFragment()
    }



    private lateinit var mCategories: ArrayList<Category>
    private var dbRootRef = Firebase.database.reference
    private var firebaseAuth = FirebaseAuth.getInstance()

    private var _binding: FragmentCategorySelectionBinding? = null
    private val binding get() = _binding!!
    private lateinit var navController: NavController
    private lateinit var categorySelectionViewModel: CategorySelectionViewModel
    private lateinit var manager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCategorySelectionBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = findNavController()
        manager = LinearLayoutManager(context)



        categorySelectionViewModel =
            ViewModelProvider(requireActivity()).get(CategorySelectionViewModel::class.java)
            as CategorySelectionViewModel

        Log.d("thong","loan trong transaction ${categorySelectionViewModel.toString()}")

        getCategoryFromFirebase()



    }

    private fun getCategoryFromFirebase() {
        dbRootRef.child("categories")
            .child(firebaseAuth.currentUser!!.uid)
            .addValueEventListener(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    mCategories = ArrayList()
                    Log.d("SonDeBug", "here")
                    for(ds: DataSnapshot in snapshot.children){
                        val temp: Category? = ds.getValue(Category::class.java)
                        Log.d("SonDeBug", temp.toString())
                        mCategories!!.add(temp!!)
                    }

                    val adapter = CategoryRVAdapter(mCategories,requireActivity())
                    binding.rcvCategory.layoutManager = manager
                    binding.rcvCategory.adapter = adapter
                    adapter.setOnItemClickListener(object: CategoryRVAdapter.onItemClicklistener{
                        override fun onItemClick(item: Category) {
                            categorySelectionViewModel.setSelectedCategory(item)
                            navController.popBackStack()
                        }
                    })


                    if(mCategories!!.size != 0) {
//                        checkParentCate()
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.d("SonDebug", "failed")
                }
            })
    }




    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}