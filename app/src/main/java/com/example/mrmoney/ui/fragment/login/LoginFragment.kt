package com.example.mrmoney.ui.fragment.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.mrmoney.R
import com.example.mrmoney.databinding.FragmentLoginBinding
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException

import com.google.android.gms.tasks.Task
import com.example.mrmoney.REQ_CODE_GG
import com.example.mrmoney.data.model.entity.Wallet
import com.example.mrmoney.data.respository.UserRespository
import com.example.mrmoney.data.respository.WalletRespository
import com.example.mrmoney.ui.fragment.auth.AuthFragmentDirections
import com.example.mrmoney.ui.fragment.home.HomeFragment
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.FacebookSdk
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.firebase.ui.auth.data.model.User
import com.google.android.gms.auth.api.identity.BeginSignInRequest
import com.google.firebase.auth.*

class LoginFragment: Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private lateinit var viewModel: LoginViewModel
    private lateinit var firebaseAuth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        firebaseAuth = FirebaseAuth.getInstance()



        binding.loginViewModel = viewModel
        binding.lifecycleOwner = this

        requireActivity().supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_login_another_method,LoginAnotherMethodFragment())
            .addToBackStack(null)
            .commit()

        setUpListeners()

        viewModel.navigateToHome.observe(viewLifecycleOwner, Observer {
            if (it) {
                this.findNavController()
                    .navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
                viewModel.doneHomeNavigation()
            }
        })

        viewModel.navigateToSignUp.observe(viewLifecycleOwner, Observer {
            if (it) {
                this.findNavController()
                    .navigate(LoginFragmentDirections.actionLoginFragmentToSignUpFragment())
                viewModel.doneSignUpNavigation()
            }
        })
        binding.frameLayout3.visibility = View.GONE

        return binding.root
    }




    private fun setUpListeners() {
        binding.etEmail.doAfterTextChanged { email ->
            viewModel.username = email.toString()
        }
        binding.etPassword.doAfterTextChanged { pass -> viewModel.password = pass.toString() }

        binding.btnLoginEmail.setOnClickListener {
            binding.frameLayout3.visibility = View.VISIBLE
            signInEmail()
        }
    }

    fun signInEmail() {
        val userEmail = binding.etEmail.text.toString()
        val userPassword = binding.etPassword.text.toString()
        firebaseAuth.signInWithEmailAndPassword(userEmail,userPassword)
            .addOnCompleteListener { task ->
                Log.d("thong", "login ------")
                if(task.isSuccessful) {
                    findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
                }else {
                    Log.d("thong", "${task.exception}")
                    binding.frameLayout3.visibility = View.GONE
                }
            }
    }
}