package com.example.mrmoney.ui.fragment.Account.TabAccount

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.adapter.AccountRVAdapter
import com.example.mrmoney.adapter.DataModel
import com.example.mrmoney.adapter.HistoryAccountRVAdapter
import com.example.mrmoney.data.model.entity.Transaction
import com.example.mrmoney.data.model.entity.Wallet
import com.example.mrmoney.databinding.DetailAccountFragmentBinding
import com.example.mrmoney.ui.fragment.Account.AccountFragmentDirections
import com.example.mrmoney.utils.CurrencyUltils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class DetailAccountFragment : Fragment() {

    private var _binding: DetailAccountFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var manager: RecyclerView.LayoutManager
    private lateinit var viewModel: DetailAccountViewModel
    val args: DetailAccountFragmentArgs by navArgs<DetailAccountFragmentArgs>()


    companion object {
        fun newInstance() = DetailAccountFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DetailAccountFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var wallet = args.detailWallet

        Firebase.database.reference.child("transactions")
            .child(FirebaseAuth.getInstance().uid.toString())
            .orderByChild("walletID")
            .startAt(wallet!!.walletId)
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    var mListTransaction = ArrayList<Transaction>()
                    var totalIncome = 0.0
                    var totalExpense = 0.0
                    for(ds: DataSnapshot in snapshot.children){
                        ds.getValue(Transaction::class.java)?.let {
                            mListTransaction!!.add(it)
                            when(it.transactionType.toString()){
                                "EXPENSE" -> {
                                    totalExpense += it.amount
                                }
                                "LEND" -> {
                                    totalIncome += it.amount

                                }
                                "INCOME" -> {
                                    totalIncome += it.amount
                                }
                                "BORROW" -> {
                                    totalExpense += it.amount
                                }
                            }
                        }
                    }
                    binding.tvSpending.text = CurrencyUltils.moneyFormatter(totalExpense,"VND")
                    binding.tvColleting.text = CurrencyUltils.moneyFormatter(totalIncome,"VND")
                    val adapter = HistoryAccountRVAdapter(mListTransaction,0)
                    binding.detailAccountRv.layoutManager = manager
                    binding.detailAccountRv.adapter = adapter
                    Log.d("DetailAccountFragment", "" + mListTransaction.size)
                    binding.wallet = wallet

                    adapter.setOnItemClickListener(object: HistoryAccountRVAdapter.onItemClickListener{
                        override fun onItemClick(position: Int) {
                            Log.d("SonDebug", "failed")
                        }
                    })

                }
                override fun onCancelled(error: DatabaseError) {
                    Log.d("SonDebug", "failed")
                }



            })

        manager = LinearLayoutManager(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DetailAccountViewModel::class.java)
        // TODO: Use the ViewModel
    }

}