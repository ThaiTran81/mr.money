package com.example.mrmoney.ui.fragment.other

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.adapter.SavingDepositAdapter
import com.example.mrmoney.data.model.entity.SavingDeposit
import com.example.mrmoney.databinding.FragmentSavingDepositBinding

class SavingDepositFragment : Fragment() {

    companion object {
        fun newInstance() = SavingDepositFragment()
    }

    private lateinit var viewModel: SavingDepositViewModel
    private lateinit var binding: FragmentSavingDepositBinding
    private lateinit var manager: RecyclerView.LayoutManager

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSavingDepositBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val itemDecoration: RecyclerView.ItemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        var data = SavingDeposit.createSampleData()

        manager = LinearLayoutManager(context)
        var adapter = SavingDepositAdapter(data)

        navController = findNavController()

        adapter.onItemClick = { item ->
            var action = SavingDepositFragmentDirections.actionSavingDepositFragmentToInterestCalculatorFragment(item.type)
            navController.navigate(action)
        }

        binding.savingDepositRV.apply {
            this.adapter = adapter
            layoutManager = manager
            addItemDecoration(itemDecoration)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SavingDepositViewModel::class.java)
        // TODO: Use the ViewModel
    }
}