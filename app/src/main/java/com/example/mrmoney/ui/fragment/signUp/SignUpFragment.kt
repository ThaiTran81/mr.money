package com.example.mrmoney.ui.fragment.signUp

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.mrmoney.R
import com.example.mrmoney.databinding.FragmentSignUpBinding
import com.example.mrmoney.ui.fragment.login.LoginAnotherMethodFragment
import com.example.mrmoney.ui.fragment.login.LoginFragmentDirections
import com.example.mrmoney.utils.FirebaseUtil

class SignUpFragment: Fragment() {
    private lateinit var binding: FragmentSignUpBinding
    private lateinit var viewModel: SignUpViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)
        binding = FragmentSignUpBinding.inflate(inflater, container, false)
        binding.signUpViewModel = viewModel
        binding.lifecycleOwner = this

        requireActivity().supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_login_another_method, LoginAnotherMethodFragment())
            .addToBackStack(null)
            .commit()

        setupListeners()
        setupObservers()

        binding.frameLayout4.visibility = View.GONE

        return binding.root
    }

    private fun setupObservers() {
        viewModel.fullNameError.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                binding.etNameSignUp.error = it
            }
        })
        viewModel.emailError.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                binding.etEmailSignUp.error = it
            }
        })
        viewModel.passwordError.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                binding.etPasswordSignUp.error = it
            }
        })
        viewModel.navigateToHome.observe(viewLifecycleOwner, Observer {
            if (it) {
                this.findNavController()
                    .navigate(SignUpFragmentDirections.actionSignUpFragmentToHomeFragment())
                viewModel
            }
        })
    }

    private fun setupListeners() {
        binding.etEmailSignUp.doAfterTextChanged { email -> viewModel.email = email.toString() }
        binding.etNameSignUp.doAfterTextChanged { fname ->
            viewModel.fullname = fname.toString()
        }
        binding.etPasswordSignUp.doAfterTextChanged { password ->
            viewModel.password = password.toString()
        }
        binding.btnSignUpV.setOnClickListener {
            viewModel.createUser()
        }
    }
}