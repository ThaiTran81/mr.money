package com.example.mrmoney.ui.fragment.other

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.mrmoney.*
import com.example.mrmoney.adapter.UtilAdapter
import com.example.mrmoney.data.model.entity.Menu
import com.example.mrmoney.data.model.entity.User
import com.example.mrmoney.databinding.FragmentOtherBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class OtherFragment : Fragment() {

    private lateinit var binding: FragmentOtherBinding
    private lateinit var navController: NavController
    companion object {
        fun newInstance() = OtherFragment()
    }

    private lateinit var viewModel: OtherViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentOtherBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        viewModel = ViewModelProvider(this).get(OtherViewModel::class.java)

        viewModel.navigateToMoneyConvert.observe(viewLifecycleOwner, Observer {
            if(it){
                navController.navigate(R.id.action_otherFragment_to_currencyConvertFragment)
                viewModel.onNavigateMoneyConvertComplete()
            }
        })

        viewModel.navigateToSavingDeposit.observe(viewLifecycleOwner, Observer {
            if (it){
                navController.navigate(R.id.action_otherFragment_to_savingDepositFragment)
                viewModel.onNavigateSavingDepositComplete()
            }
        })

        viewModel.navigateToManagementCategory.observe(viewLifecycleOwner, Observer {
            if(it){
                navController.navigate(R.id.action_otherFragment_to_managementCategoryFragment)
                viewModel.onNavigateManagementCategoryComplete()
            }
        })

        binding.signoutBtn.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            findNavController().navigate((R.id.action_otherFragment_to_splashFragment))
        }
    }


    fun init(){
        val utilMenu = Menu()

        utilMenu.add(EXPENSE_LIMIT_UTIL,"Hạn mức chi", R.drawable.cate_stock)
        utilMenu.add(CATEGORY_UTIL,"Hạng mục thu/chi", R.drawable.cate_collector)
        utilMenu.add(LIST_SHOPPING_UTIL,"Danh sách mua sắm", R.drawable.cate_market2)
        utilMenu.add(EXCHANGE_LOOKUP, "Tra cứu tỷ giá", R.drawable.cate_bitcoin)
        utilMenu.add(SPLIT_BILL, "Chia đơn", R.drawable.cate_share)
        utilMenu.add(SAVINGS_DEPOSIT, "Tiết kiệm gửi góp", R.drawable.ic_piggy_bank)

        val utilAdapter = UtilAdapter(utilMenu.menus, UtilAdapter.OnClickListener{
            viewModel.navigateTo(it.id)
        })

        binding.layoutUtilGrid.rvUtils.adapter = utilAdapter
        binding.layoutUtilGrid.rvUtils.layoutManager = GridLayoutManager(context, 3)

        FirebaseDatabase.getInstance().reference.child("users")
            .child(FirebaseAuth.getInstance().currentUser!!.uid)
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {

                    if (snapshot.exists()) {
                        val user = snapshot.getValue(User::class.java)!!
                        binding.nameOther.text = user.userName
                        binding.emailOther.text = user.email
                    }

                }

                override fun onCancelled(error: DatabaseError) {
                    Log.d("HomeFragment", "${error.details}")
                }
            })
    }

}