package com.example.mrmoney.ui.fragment.home

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mrmoney.R
import com.example.mrmoney.adapter.ExchangeAdapter
import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.data.model.entity.MenuItem
import com.example.mrmoney.data.model.entity.Transaction
import com.example.mrmoney.data.model.entity.User
import com.example.mrmoney.data.model.entity.Wallet
import com.example.mrmoney.data.respository.TransactionRepository
import com.example.mrmoney.databinding.FragmentHomeBinding
import com.example.mrmoney.utils.CurrencyUltils
import com.example.mrmoney.utils.DateUtils
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private lateinit var navController: NavController
    private val database = FirebaseDatabase.getInstance()
    private val dbRootRef = database.reference
    private val firebaseAuth = FirebaseAuth.getInstance()
    private var currentUser = firebaseAuth.currentUser
    private var mUser :User? = null
    private lateinit var homeViewModel: HomeViewModel

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container, false)

        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        currentUser = firebaseAuth.currentUser


        binding.layoutExpenseStats.spTimeFilter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                Log.i("HomeFragment","time filter")
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                Log.i("HomeFragment","time filter")
            }
        }

        binding.layoutExpenseStats.cvExpenseStats.setOnClickListener {
            homeViewModel.onFilterDateClick(binding.layoutExpenseStats.spTimeFilter.selectedItem as MenuItem)
        }

        binding.layoutStatsBarchart.cvBarchart.setOnClickListener {
            navController.navigate(HomeFragmentDirections.actionHomeFragmentToRevenueAnalysisDetailFragment(TransactionType.EXPENSE.ordinal))
        }

        navController = this.findNavController()

        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        homeViewModel.initProfileUser()
        homeViewModel.getTotal()
        val spTimeAdapter = ArrayAdapter(
            requireActivity(),
            android.R.layout.simple_spinner_dropdown_item,
            homeViewModel.statsTimes.menus
        )
        binding.layoutExpenseStats.spTimeFilter.adapter = spTimeAdapter

        binding.layoutTotalBalance.ibtnShowBalance.setOnClickListener {
            homeViewModel.onShowBalanceClick()
        }



        binding.layoutExpenseStats.cvExpenseStats.setOnClickListener {
            homeViewModel.onFilterDateClick(binding.layoutExpenseStats.spTimeFilter.selectedItem as MenuItem)
        }
        homeViewModel.navigateToRevenueAndExpenditure.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer {

                it?.let {

                    navController.navigate(HomeFragmentDirections.actionHomeFragmentToRevenueAndExpenditureSituationFragment(it.id))
                    homeViewModel.onRevenueAndExpenditureNavigated()
                }

            })

        homeViewModel.showTotal.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it){
                binding.layoutTotalBalance.ibtnShowBalance.setImageResource(R.drawable.ic_visibility)
                binding.layoutTotalBalance.tvTotalBalance.visibility = View.VISIBLE
                binding.layoutTotalBalance.mask.visibility = View.GONE
            }
            else{
                binding.layoutTotalBalance.ibtnShowBalance.setImageResource(R.drawable.ic_visibility_off)
                binding.layoutTotalBalance.tvTotalBalance.visibility = View.GONE
                binding.layoutTotalBalance.mask.visibility = View.VISIBLE
            }
        })

        homeViewModel.user.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            binding.homeToolBar.title = "Chào, ${it!!.userName}"
        })

        homeViewModel.totalBalace.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            binding.layoutTotalBalance.tvTotalBalance.text = it
        })

        binding.layoutStatsBarchart.cvBarchart.setOnClickListener {
            navController.navigate(R.id.action_homeFragment_to_revenueAnalysisDetailFragment)
        }


        binding.layoutTotalBalance.cvTotalBalance.setOnClickListener {
            navController.navigate(R.id.action_homeFragment_to_currentFinancesFragment)
        }

        binding.layoutExpenseStats.spTimeFilter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            val sp =  binding.layoutExpenseStats.spTimeFilter
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                homeViewModel.onDateFilterClick(homeViewModel.statsTimes.menus.get(position))
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.e("HomeFragment", "Date filter not selected")
            }

        }

        binding.layoutExchangeLookup.cvLookup.setOnClickListener {
            navController.navigate(R.id.action_homeFragment_to_currencyConvertFragment)
        }

        homeViewModel.transactions.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            setDataPieChart()
        })

        homeViewModel.revenueTotal.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            binding.layoutExpenseStats.tvIncome.text = CurrencyUltils.moneyFormatter(it,"VND")
        })

        homeViewModel.expenseTotal.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            binding.layoutExpenseStats.tvExpense.text = CurrencyUltils.moneyFormatter(it,"VND")
        })

        homeViewModel.expCurYeartransactions.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            setBarChartExpense()
        })

        setViewExchangeLookup()
//        addNewTransaction()
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setDataPieChart() {
        if(homeViewModel.transactions.value == null) {
            binding.layoutExpenseStats.pcStatsByCategory.clear()
            return
        }
        val pieEntries: ArrayList<PieEntry> = ArrayList()
        val label = ""

        //initializing data
        val typeAmountMap: MutableMap<String, Double> = TreeMap(Collections.reverseOrder())
        for(it in homeViewModel.transactions.value!!){
            if(it!!.transactionType == TransactionType.INCOME.name) continue
            if (typeAmountMap.containsKey(it!!.category)){
                typeAmountMap.put(it.category, typeAmountMap.getValue(it.category)+it.amount)
            }else {
                typeAmountMap.put(it.category, it.amount)
            }
        }


        val amountMap: MutableMap<String, Double> = TreeMap(Collections.reverseOrder())
        amountMap.put("Khác", 0.0)
        var count = 0

        for(it in typeAmountMap){
            if (count < 4){
                amountMap.put(it.key, it.value)
                count++
            }else {
                amountMap.put("Khác", amountMap.getValue("Khác")+it.value)

            }
        }

        for(it in amountMap){
            it.setValue(it.value /homeViewModel.expenseTotal.value!!*100)
        }

        //initializing colors for the entries
        val colors: ArrayList<Int> = ArrayList()
        colors.add(Color.parseColor("#0450b4"))
        colors.add(Color.parseColor("#fea802"))
        colors.add(Color.parseColor("#d94a8c"))
        colors.add(Color.parseColor("#fe7434"))
        colors.add(Color.parseColor("#38b000"))
        colors.add(Color.parseColor("#6fb1a0"))
        colors.add(Color.parseColor("#15a2a2"))


        val value_colors: ArrayList<Int> = ArrayList()
        value_colors.add(Color.parseColor("#ffffff"))
        //input data and fit data into pie chart entry
        for (type in amountMap.keys) {
            pieEntries.add(PieEntry(amountMap[type]!!.toFloat(), type))
        }

        //collecting the entries with label name
        val pieDataSet = PieDataSet(pieEntries, label)
        //setting text size of the value
        pieDataSet.valueTextSize = 16f
        pieDataSet.formSize = 14f
        //providing color list for coloring different entries
        pieDataSet.colors = colors
        pieDataSet.valueTextColor = Color.parseColor("#ffffff")
        //grouping the data set from entry to chart
        val pieData = PieData(pieDataSet)
        //showing the value of the entries, default true if not set
        pieData.setDrawValues(true)
        binding.layoutExpenseStats.pcStatsByCategory.description.isEnabled = false


        binding.layoutExpenseStats.pcStatsByCategory.data = pieData
        binding.layoutExpenseStats.pcStatsByCategory.animateXY(2000, 1000)
        binding.layoutExpenseStats.pcStatsByCategory.invalidate()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onResume() {
        super.onResume()
        homeViewModel.getTotal()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setViewExchangeLookup() {
        val recyclerView = binding.layoutExchangeLookup.rvExchangeLookup
        homeViewModel.initExchangeLookup()
        val base = homeViewModel.getBaseRate()
        val adapter = ExchangeAdapter(base!!, homeViewModel.exchangeLst.value!!)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
    }

//    fun addNewTransaction() {
//        var a = arrayListOf<Transaction>(
//            Transaction(
//                "Ăn uống",
//                TransactionType.EXPENSE,
//                Date().time.toString(),
//                "",
//                5000.0,
//                "-N1D6x5iNB-eIa8mzNXZ"
//            ),
//            Transaction(
//                "Lương",
//                TransactionType.INCOME,
//                (Date().time-16568465).toString(),
//                "",
//                1005000.0,
//                "-N1D6x5iNB-eIa8mzNXZ"
//            ),
//            Transaction(
//                "Nhà cửa",
//                TransactionType.EXPENSE,
//                (Date().time-156854).toString(),
//                "",
//                500000.0,
//                "-N1D6x5iNB-eIa8mzNXZ"
//            ),
//            Transaction(
//                "Ăn uống",
//                TransactionType.EXPENSE,
//                (Date().time-65486).toString(),
//                "",
//                57000.0,
//                "-N1D6x5iNB-eIa8mzNXZ"
//            ),
//            Transaction(
//                "Đi lại",
//                TransactionType.EXPENSE,
//                (Date().time-98946).toString(),
//                "",
//                87000.0,
//                "-N1D6x5iNB-eIa8mzNXZ"
//            ),
//            Transaction(
//                "Ăn uống",
//                TransactionType.EXPENSE,
//                (Date().time-979852).toString(),
//                "",
//                75000.0,
//                "-N1D6x5iNB-eIa8mzNXZ"
//            ),
//            Transaction(
//                "Sức khỏe",
//                TransactionType.EXPENSE,
//                (Date().time-36477).toString(),
//                "",
//                5000.0,
//                "-N1D6x5iNB-eIa8mzNXZ"
//            ),
//            Transaction(
//                "Ăn uống",
//                TransactionType.EXPENSE,
//                (Date().time-24545).toString(),
//                "",
//                975000.0,
//                "-N1D6x5iNB-eIa8mzNXZ"
//            ),
//            Transaction(
//                "Ăn uống",
//                TransactionType.EXPENSE,
//                (Date().time-99752).toString(),
//                "",
//                502400.0,
//                "-N1D6x5iNB-eIa8mzNXZ"
//            ),
//            Transaction(
//                "Lương",
//                TransactionType.EXPENSE,
//                (Date().time-64876).toString(),
//                "",
//                500000.0,
//                "-N1D6x5iNB-eIa8mzNXZ"
//            )
//        )
//
//        for (trn in a) {
//            TransactionRepository.getInstance().addNewTransaction(trn)
//        }
//    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setBarChartExpense(){

        val year = homeViewModel.firstDayOfYear.value!!.year.toInt()


        binding.layoutStatsBarchart.tvDateRange.text = "1/$year - 12/$year"
        val entries: ArrayList<BarEntry> = ArrayList()
//        entries.add(BarEntry(1f, 4f))
//        entries.add(BarEntry(2f, 10f))
//        entries.add(BarEntry(3f, 2f))
//        entries.add(BarEntry(4f, 15f))
//        entries.add(BarEntry(5f, 13f))
//        entries.add(BarEntry(6f, 2f))

        var map = HashMap<Int, Double>()

        for(it in homeViewModel.expCurYeartransactions.value!!){
                val key = DateUtils.covertTimestampToDatetime(it!!.time).month.value
            if(map.containsKey(key)){
                map.put(key, map.getValue(key)+it.amount)
            }
            else map.put(key, it.amount)
        }

        for (i in 1 until 12){
            if(map.containsKey(i)){
                entries.add(BarEntry(i.toFloat(), map.getValue(i).toFloat()))
            }
            else entries.add(BarEntry(i.toFloat(), 0f))
        }

        val barDataSet = BarDataSet(entries, "")
        barDataSet.setColors(*ColorTemplate.COLORFUL_COLORS)

        val data = BarData(barDataSet)
        binding.layoutStatsBarchart.bcExpense.data = data


        //hide grid lines
        binding.layoutStatsBarchart.bcExpense.axisLeft.setDrawGridLines(false)
        binding.layoutStatsBarchart.bcExpense.xAxis.setDrawGridLines(false)
        binding.layoutStatsBarchart.bcExpense.xAxis.setDrawAxisLine(false)

        //remove right y-axis
        binding.layoutStatsBarchart.bcExpense.axisRight.isEnabled = false

        //remove legend
        binding.layoutStatsBarchart.bcExpense.legend.isEnabled = false


        //remove description label
        binding.layoutStatsBarchart.bcExpense.description.isEnabled = false


        //add animation
        binding.layoutStatsBarchart.bcExpense.animateY(3000)

        binding.layoutStatsBarchart.bcExpense.xAxis.position = XAxis.XAxisPosition.BOTTOM

        //draw chart
        binding.layoutStatsBarchart.bcExpense.invalidate()
    }

}