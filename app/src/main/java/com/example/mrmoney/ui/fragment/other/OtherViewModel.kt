package com.example.mrmoney.ui.fragment.other

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mrmoney.CATEGORY_UTIL
import com.example.mrmoney.EXCHANGE_LOOKUP
import com.example.mrmoney.SAVINGS_DEPOSIT

class OtherViewModel : ViewModel() {

    var _navigateToMoneyConvert = MutableLiveData<Boolean>()
    val navigateToMoneyConvert: LiveData<Boolean>
    get() = _navigateToMoneyConvert

    var _navigateToSavingDeposit = MutableLiveData<Boolean>()
    val navigateToSavingDeposit: LiveData<Boolean>
        get() = _navigateToSavingDeposit

    var _navigateToManagementCategory = MutableLiveData<Boolean>()
    val navigateToManagementCategory: LiveData<Boolean>
        get() = _navigateToManagementCategory


    fun navigateTo(menuId: Int){
        when(menuId){
            EXCHANGE_LOOKUP -> _navigateToMoneyConvert.value = true
            SAVINGS_DEPOSIT-> _navigateToSavingDeposit.value = true
            CATEGORY_UTIL -> _navigateToManagementCategory.value = true
        }
    }

    fun onNavigateMoneyConvertComplete(){
        _navigateToMoneyConvert.value = false
    }

    fun onNavigateSavingDepositComplete(){
        _navigateToSavingDeposit.value = false
    }

    fun onNavigateManagementCategoryComplete(){
        _navigateToManagementCategory.value = false
    }
}