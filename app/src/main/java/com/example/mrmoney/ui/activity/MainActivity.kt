package com.example.mrmoney.ui.activity

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.blongho.country_data.World
import com.example.mrmoney.R
import com.example.mrmoney.data.respository.CurrencyExchangeDAO
import com.example.mrmoney.data.respository.DBConnection
import com.example.mrmoney.databinding.ActivityMainBinding
import com.example.mrmoney.utils.Prefs
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {
    //    private lateinit var  bottomNavigationView: BottomNavigationView
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    @SuppressLint("ResourceAsColor")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        DBConnection.getInstance(this)
        // Enable offline work
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true)

        // Keep data fresh
        if (isOnline(this)) {
            Log.d("MainActivity", "user")
            val dbRef = Firebase.database.reference
            dbRef.keepSynced(true)
        }

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        World.init(application)
        binding.bottomAppBar.visibility = View.GONE
        binding.fab.visibility = View.GONE

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController

        val navInflater = navController.navInflater

        val graph_main = navInflater.inflate(R.navigation.my_nav_graph)


        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.splashFragment ||
                destination.id == R.id.authFragment ||
                destination.id == R.id.loginFragment ||
                destination.id == R.id.signUpFragment
            ) {
                binding.bottomAppBar.visibility = View.GONE
                binding.fab.visibility = View.GONE
            } else if (destination.id == R.id.addAccountFragment || destination.id == R.id.addBankBookFragment) {
                binding.bottomAppBar.visibility = View.GONE
                binding.fab.visibility = View.GONE
            } else {
                binding.bottomAppBar.visibility = View.VISIBLE
                binding.fab.visibility = View.VISIBLE
            }
        }
//        graph_main.startDestination = R.id.homeFragment


        val cur = FirebaseAuth.getInstance().currentUser
        if (!Prefs.getInstance(this)!!.hasCompletedWalkthrough!! && cur != null) {


//            Log.d("thong"," --- ${cur.email}")
            graph_main.startDestination = R.id.homeFragment
        } else {
            graph_main.startDestination = R.id.splashFragment
        }

        navController.graph = graph_main


        binding.fab.setOnClickListener {
//            navController.navigate(R.id.action_homeFragment_to_transactionFragment)
            var menu = binding.bottomNavigationView.menu
            menu.getItem(2).isEnabled = true
            menu.performIdentifierAction(R.id.transactionFragment,0)
            menu.getItem(2).isEnabled = false
        }

        binding.bottomNavigationView.setupWithNavController(navController)
    }
}

@RequiresApi(Build.VERSION_CODES.M)
fun isOnline(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (connectivityManager != null) {
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                return true
            }
        }
    }
    return false
}