package com.example.mrmoney.ui.fragment.other

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mrmoney.data.model.ExchangeRate
import com.example.mrmoney.data.respository.DBConnection
import kotlinx.coroutines.launch

class CurrencyConvertViewModel(application: Application) : AndroidViewModel(application) {
    // TODO: Implement the ViewModel
    var countryLst = MutableLiveData<List<ExchangeRate>>()
    var input = MutableLiveData<Double>()
    var output = MutableLiveData<Double>()

    init {
        getData()
    }

    fun getData(){
        viewModelScope.launch {
            countryLst.value = DBConnection.getInstance(getApplication()).currencyExchangeDAO.getAll()
        }
    }

    fun onInput(num : Double){
        input.value = num
    }

    fun onOutput(num: Double){
        output.value = num
    }
}