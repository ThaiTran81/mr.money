package com.example.mrmoney.ui.fragment.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.mrmoney.R
import com.example.mrmoney.databinding.FragmentRevenueAndExpenditureSituationBinding

class RevenueAndExpenditureFragment: Fragment() {
    private lateinit var binding: FragmentRevenueAndExpenditureSituationBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentRevenueAndExpenditureSituationBinding.inflate(inflater, container, false)

        return binding.root
    }
}