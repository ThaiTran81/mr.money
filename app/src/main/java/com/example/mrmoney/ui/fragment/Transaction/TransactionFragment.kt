package com.example.mrmoney.ui.fragment.Transaction

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.graphics.Color
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.get
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.mrmoney.R
import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.data.model.entity.Category
import com.example.mrmoney.data.model.entity.Transaction
import com.example.mrmoney.data.respository.TransactionRepository
import com.example.mrmoney.data.model.entity.Wallet
import com.example.mrmoney.databinding.FragmentTransactionBinding
import com.example.mrmoney.ui.fragment.Account.AccountSelectionViewModel
import com.example.mrmoney.ui.fragment.category.CategorySelectionViewModel
import com.example.mrmoney.ui.fragment.loan.LoanerChooserViewModel
import com.example.mrmoney.utils.DateUtils
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TransactionFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TransactionFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private val myCalendar = Calendar.getInstance()
    private var walletSelected: Wallet? = null
    private var _binding: FragmentTransactionBinding? = null
    private val binding get() = _binding!!
    private lateinit var navController: NavController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onResume() {

        super.onResume()
        val categories = resources.getStringArray(R.array.categories)
        var arrayAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_item, categories)
        binding.transactionAppbar.snCategory.autoCompleteTextView.setAdapter(arrayAdapter)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentTransactionBinding.inflate(inflater, container, false)

        updateEtTime()
        updateEtDate()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        navController = Navigation.findNavController(view)

        var loanerChooserViewModel =
            ViewModelProvider(requireActivity()).get(LoanerChooserViewModel::class.java)
                    as LoanerChooserViewModel
        Log.d("thong","loan trong transaction ${loanerChooserViewModel.toString()}")

        loanerChooserViewModel.getSelectedItem().observe(viewLifecycleOwner, {
            Log.d("thong","frag $it")
            binding.transactionContent.tvPerson.text = it
        })

        var categorySelectionViewModel =
            ViewModelProvider(requireActivity()).get(CategorySelectionViewModel::class.java)
                    as CategorySelectionViewModel
        Log.d("thong","loan trong transaction ${categorySelectionViewModel.toString()}")
        categorySelectionViewModel.getSelectedCategory().observe(viewLifecycleOwner,{
                binding.transactionContent.tvCategory.text = it.name

                var type: String? = null
                Log.d("TransactionFragment", it.transactionType.toString())
                when (it.transactionType.toString()) {
                    "BORROW" -> {
                        type = "Đi vay"
                        Log.d("TracsactionFragment", "Selected 2 " + type)
                        binding.transactionContent.llPerson.visibility = View.VISIBLE
                    }
                    "LEND" -> {
                        type = "Cho vay"
                        Log.d("TracsactionFragment", "Selected 3" + type)
                        binding.transactionContent.llPerson.visibility = View.VISIBLE
                    }
                    "EXPENSE" -> {
                        type = "Chi tiền"
                        Log.d("TracsactionFragment", "Selected 0 " + type)
                    }
                    "INCOME" -> {
                        type = "Thu tiền"
                        Log.d("TracsactionFragment", "Selected 1 " + type)
                    }

                }
                if (it.name.equals("Thu nợ") || it.name.equals("Trả nợ")) {
                    binding.transactionContent.llPerson.visibility = View.VISIBLE
                }

                binding.transactionAppbar.snCategory.autoCompleteTextView.setText(
                    type
                )
        })



        var accountSelectionViewModel =
            ViewModelProvider(requireActivity()).get(AccountSelectionViewModel::class.java)
                    as AccountSelectionViewModel

        accountSelectionViewModel.getSelectedWallet().observe(viewLifecycleOwner, {
            walletSelected = it
            binding.transactionContent.tvSourceMoney.text = it.name
        })


        // handle dropdown selection
        binding.transactionAppbar.snCategory.autoCompleteTextView.setOnItemClickListener { adapterView, view, i, l ->
            if (adapterView.getItemAtPosition(i).equals("Chi tiền")){
                binding.transactionContent.llPerson.visibility = View.GONE
                val cate = binding.transactionContent.tvCategory.text.toString()
                if(cate.equals("Thu nợ") || cate.equals("Trả nợ")){
                    binding.transactionContent.llPerson.visibility = View.VISIBLE
                }
                binding.transactionContent.etMoney.setTextColor(Color.parseColor("#FF3378"))
                }
            else if (adapterView.getItemAtPosition(i).equals("Thu tiền")){
                binding.transactionContent.llPerson.visibility = View.GONE
                binding.transactionContent.etMoney.setTextColor(Color.parseColor("#41E948"))
            }else {
                binding.transactionContent.llPerson.visibility = View.VISIBLE
                if (adapterView.getItemAtPosition(i).equals("Cho vay")) {
                    binding.transactionContent.tvCategory.text = "Cho vay"
                    binding.transactionContent.etMoney.setTextColor(Color.parseColor("#FF3378"))
                    categorySelectionViewModel.setSelectedCategory(
                        Category (
                            name = "Cho vay",
                            transactionType = TransactionType.LEND.toString()
                        )
                    )

                }
                else if (adapterView.getItemAtPosition(i).equals("Đi vay")) {
                    binding.transactionContent.tvCategory.text = "Đi vay"
                    categorySelectionViewModel.setSelectedCategory(
                        Category (
                            name = "Đi vay",
                            transactionType = TransactionType.BORROW.toString()
                        )
                    )
                    binding.transactionContent.etMoney.setTextColor(Color.parseColor("#41E948"))
                }
            }
        }

        binding.transactionContent.llPerson.setOnClickListener {
            var action = TransactionFragmentDirections.actionTransactionFragmentToLoanerChooserFragment(
                binding.transactionContent.tvCategory.text.toString()
            )
            navController.navigate(action)
        }

        binding.transactionContent.llCategory.setOnClickListener {
            navController.navigate(R.id.action_transactionFragment_to_categorySelectionFragment)
        }


        binding.transactionContent.etDate.setOnClickListener {
            setOnDatePickerSetListener()
        }

        binding.transactionContent.etTime.setOnClickListener {
            setOnTimePickerSetListener()
        }

        binding.transactionContent.llSourceMoney.setOnClickListener {
            navController.navigate(R.id.action_transactionFragment_to_accountSelectionFragment)
        }

        binding.transactionContent.btnSave.setOnClickListener {
            if(validateInput() == true){
                var transaction = getTransaction()
                TransactionRepository.getInstance().addNewTransaction(transaction!!)
                Toast.makeText(requireContext(), "Thêm giao dịch thành công",Toast.LENGTH_SHORT).show()
                navController.popBackStack()
            }
        }

        binding.transactionAppbar.imgCheck.setOnClickListener {
            if(validateInput() == true){
                var transaction = getTransaction()
                TransactionRepository.getInstance().addNewTransaction(transaction!!)
                Toast.makeText(requireContext(), "Thêm giao dịch thành công",Toast.LENGTH_SHORT).show()
                navController.popBackStack()
            }
        }

    }

    fun setOnDatePickerSetListener(){
        var date = DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, month)
            myCalendar.set(Calendar.DAY_OF_MONTH, day)
            updateEtDate()
        }
        DatePickerDialog(requireActivity(),
            date,
            myCalendar.get(Calendar.YEAR),myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    fun setOnTimePickerSetListener(){
        var hour = myCalendar.get(Calendar.HOUR_OF_DAY)
        var minute = myCalendar.get(Calendar.MINUTE)

        var timePickerDialog = TimePickerDialog(
            requireActivity(),
            TimePickerDialog.OnTimeSetListener { timePicker, i, i2 ->
                updateEtTime()
            }, hour, minute,true)
        timePickerDialog.show()
    }

    private fun updateEtTime(){
        var myFormat:String = "HH:mm"
        var timeFormat:SimpleDateFormat = SimpleDateFormat(myFormat, Locale.US)
        binding.transactionContent.etTime.setText(timeFormat.format(myCalendar.time))
    }

    private fun updateEtDate() {
        var myFormat:String = "MM/dd/yyyy"
        var dateFormat:SimpleDateFormat = SimpleDateFormat(myFormat, Locale.US)
        binding.transactionContent.etDate.setText(dateFormat.format(myCalendar.time))
    }

    fun validateInput():Boolean{
        if(binding.transactionContent.etDate.text!!.length == 0){
            Toast.makeText(requireContext(),"Hãy nhập đủ thông tin",Toast.LENGTH_SHORT).show()
            return false

        }else if(binding.transactionContent.etMoney.text.toString().toDouble() == 0.0){
            Toast.makeText(requireContext(),"Hãy nhập đủ thông tin",Toast.LENGTH_SHORT).show()
            return false

        }else if(binding.transactionContent.tvCategory.text!!.trim().length == 0){
            Toast.makeText(requireContext(),"Hãy nhập đủ thông tin",Toast.LENGTH_SHORT).show()
            return false

        }else if(binding.transactionContent.tvSourceMoney.text!!.trim().length == 0){
            Toast.makeText(requireContext(),"Hãy nhập đủ thông tin",Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    fun getTransaction(): Transaction?{

        var datetime = DateUtils.dateTimeToTimeStamp(
            binding.transactionContent.etDate.text.toString() + " " +
                binding.transactionContent.etTime.text.toString())


        var transaction = Transaction(
            binding.transactionContent.tvCategory.text.toString(),
            TransactionType.EXPENSE.toString(),
            datetime!!,
            binding.transactionContent.etDescription.text.toString(),
            binding.transactionContent.etMoney.text.toString().toDouble(),
            walletSelected!!.walletId,
            "",
            ""
        )



        when(binding.transactionAppbar.snCategory.autoCompleteTextView.text.toString()){
            "Chi tiền" -> {
                transaction.transactionType = TransactionType.EXPENSE.toString()
            }
            "Thu tiền" -> {
                transaction.transactionType = TransactionType.INCOME.toString()
            }
            "Cho vay" -> {
                transaction.transactionType = TransactionType.LEND.toString()
                transaction.person = binding.transactionContent.tvPerson.text.toString()
            }
            "Đi vay" -> {
                transaction.transactionType = TransactionType.BORROW.toString()
                transaction.person = binding.transactionContent.tvPerson.text.toString()
            }
        }

        return transaction
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TransactionFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TransactionFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}