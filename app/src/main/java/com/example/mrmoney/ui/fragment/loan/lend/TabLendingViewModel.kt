package com.example.mrmoney.ui.fragment.loan.lend

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mrmoney.data.model.entity.Loaner
import com.example.mrmoney.data.model.entity.Transaction
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class TabLendingViewModel(application: Application) : AndroidViewModel(application) {
    private val database = FirebaseDatabase.getInstance()
    private val dbRootRef = database.reference
    private val firebaseAuth = FirebaseAuth.getInstance()
    private var currentUser = firebaseAuth.currentUser


    var totalLend = MutableLiveData<Double>()
    var paidLend = MutableLiveData<Double>()
    var needPaidLend = MutableLiveData<Double>()
    var lending = MutableLiveData< ArrayList<Loaner>>()
    var lended = MutableLiveData< ArrayList<Loaner>>()

    init {
    }

    fun getLendingDataFromDB() {
        dbRootRef.child("transactions")
            .child(currentUser!!.uid)
            .orderByChild("transactionType")
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    var total = 0.0
                    var borrower = ArrayList<Transaction>()
                    var loander = HashMap<String,Double?>()
                    var loandered = HashMap<String,Double?>()
                    for(ds : DataSnapshot in snapshot.children) {
                        var temp = ds.getValue(Transaction::class.java)
                        if(temp!!.transactionType.equals("LEND")) {
                            total += temp!!.amount
                            borrower.add(temp)
                            if (!loander.containsKey(temp.person)) {
                                loander[temp.person] = 0.0
                            }
                            loander[temp.person] = loander[temp.person]?.plus(temp.amount)
                        }
                    }
                    totalLend.value = total
                    dbRootRef.child("transactions")
                        .child(currentUser!!.uid)
                        .orderByChild("transactionType")
                        .addValueEventListener(object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                var paid = 0.0
                                for(ds : DataSnapshot in snapshot.children) {
                                    var tempDS = ds.getValue(Transaction::class.java)
                                    if(tempDS!!.transactionType.equals("INCOME")) {
                                        if (tempDS!!.category.equals("Thu nợ")) {
                                            paid += tempDS.amount
                                            if (loander.containsKey(tempDS.person)) {
                                                loander[tempDS.person] =
                                                    loander[tempDS.person]?.minus(tempDS.amount)
                                            } else if (loandered.containsKey(tempDS.person)) {
                                                loandered[tempDS.person] =
                                                    loander[tempDS.person]?.plus(tempDS.amount)
                                            } else {
                                                loandered[tempDS.person] = tempDS.amount
                                            }
                                        }
                                    }
                                }
                                paidLend.value = paid
                                needPaidLend.value = total - paid

                                val lTemp = ArrayList<Loaner>()
                                for(l in loander) {
                                    lTemp.add(Loaner(l.key,l.value!!))
                                }
                                lending.value = lTemp

                                val ledTemp = ArrayList<Loaner>()
                                for(l in loandered) {
                                    ledTemp.add(Loaner(l.key,l.value!!,true))
                                }
                                lended.value = ledTemp

                            }
                            override fun onCancelled(error: DatabaseError) {
                                Log.d("thong", "${error.details}")
                            }
                        })
                }
                override fun onCancelled(error: DatabaseError) {
                    Log.d("thong", "${error.details}")
                }
            })
    }
}