package com.example.mrmoney.ui.fragment.category

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mrmoney.data.model.entity.Category

class CategorySelectionViewModel : ViewModel() {
    private val selectedCategory: MutableLiveData<Category> = MutableLiveData()
    private val selectedChooserName: MutableLiveData<String> = MutableLiveData()

    public fun setSelectedChooserName(data:String){
        selectedChooserName.value = data
    }

    public fun getSelectedChooserName(): LiveData<String>{
        return selectedChooserName
    }

    public fun setSelectedCategory(data:Category){
        selectedCategory.value = data
    }

    public fun getSelectedCategory(): LiveData<Category>{
        return selectedCategory
    }

}