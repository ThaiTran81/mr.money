package com.example.mrmoney.ui.fragment.loan.lend

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.adapter.LoanerAdapter
import com.example.mrmoney.data.model.LoanType
import com.example.mrmoney.data.model.LoanerType
import com.example.mrmoney.data.model.entity.Loan
import com.example.mrmoney.data.model.entity.LoanDetail
import com.example.mrmoney.data.model.entity.Loaner
import com.example.mrmoney.databinding.TabLendingFragmentBinding
import com.example.mrmoney.ui.fragment.loan.LoanFragmentDirections
import com.example.mrmoney.utils.CurrencyUltils
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList
import android.os.Bundle as Bundle1


class TabLendingFragment : Fragment() {

    companion object {
        fun newInstance() = TabLendingFragment()
    }

    private lateinit var viewModel: TabLendingViewModel
    private lateinit var binding: TabLendingFragmentBinding
    private lateinit var lending_manager: RecyclerView.LayoutManager
    private lateinit var lended_manager: RecyclerView.LayoutManager

    private var lending_data = ArrayList<Loaner>()
    private var lended_data = ArrayList<Loaner>()

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle1?
    ): View? {
        binding = TabLendingFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle1?) {
        super.onViewCreated(view, savedInstanceState)

        val itemDecoration: RecyclerView.ItemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        viewModel = ViewModelProvider(this).get(TabLendingViewModel::class.java)

       var data = LoanDetail.createDataSample()
        setData(data)
        navController = findNavController()


        viewModel.getLendingDataFromDB()

        viewModel.paidLend.observe(viewLifecycleOwner,{
            binding.lendingHasValueTV.text = CurrencyUltils.moneyFormatter(it,"VND")
            binding.lendingSummaryPB.progress = it.toInt()
        })

        viewModel.totalLend.observe(viewLifecycleOwner,{
            binding.lendingTotalSummaryValueTV.text = CurrencyUltils.moneyFormatter(it,"VND")
            binding.lendingSummaryPB.max = it.toInt()
        })

        viewModel.needPaidLend.observe(viewLifecycleOwner,{
            binding.lendingMustValueTV.text = CurrencyUltils.moneyFormatter(it,"VND")
        })

        lending_manager = LinearLayoutManager(context)
        viewModel.lending.observe(viewLifecycleOwner,{
            Log.d("thong","--- ${it.toString()}")
            var lending_adapter = LoanerAdapter(it)

            lending_adapter.onItemClick = { loaner ->
                var action = LoanFragmentDirections.actionLoanFragmentToLoanDetailFragment(loaner.name, LoanerType.LENDING)
                navController.navigate(action)
            }

            binding.lendingRV.apply {
                this.adapter = lending_adapter
                layoutManager = lending_manager
                addItemDecoration(itemDecoration)
            }
        })

        viewModel.lended.observe(viewLifecycleOwner,{
            var lending_adapter = LoanerAdapter(it)

            lending_adapter.onItemClick = { loaner ->
                var action = LoanFragmentDirections.actionLoanFragmentToLoanDetailFragment(loaner.name, LoanerType.LENDING)
                navController.navigate(action)
            }

            binding.lendingRV.apply {
                this.adapter = lending_adapter
                layoutManager = lending_manager
                addItemDecoration(itemDecoration)
            }
        })

    }

    fun setSummaryData(data: List<LoanDetail>) {
        var hasAmount: Double = 0.0
        var totalAmount: Double = 0.0

        for (item in data) {
            if (item.type == LoanType.LEND) {
                totalAmount += item.amount
            }
            if (item.type == LoanType.LEND_PAY) {
                hasAmount += item.amount
            }
        }

        binding.lendingMustValueTV.text = CurrencyUltils.moneyFormatter(totalAmount - hasAmount, "VND")
        binding.lendingHasValueTV.text = CurrencyUltils.moneyFormatter(hasAmount, "VND")
        binding.lendingTotalSummaryValueTV.text = CurrencyUltils.moneyFormatter(totalAmount, "VND")
        binding.lendingSummaryPB.max = totalAmount.toInt()
        binding.lendingSummaryPB.progress = hasAmount.toInt()
    }

    fun setData(data :List<LoanDetail>) {
        var names = ArrayList<String>()
        var temp_data = ArrayList<LoanDetail>()

        for (item in data) {
            if (item.loaner == LoanerType.LENDING) {
                temp_data.add(item)
                names.add(item.name)
            }
        }
        names = names.distinct() as ArrayList<String>

        setSummaryData(temp_data)
        var hasAmount: Double = 0.0
        var totalAmount: Double = 0.0

        for (name in names) {
            for (item in temp_data) {
                if (item.name == name) {
                    if (item.type == LoanType.LEND) {
                        totalAmount += item.amount
                    }
                    if (item.type == LoanType.LEND_PAY) {
                        hasAmount += item.amount
                    }
                }
            }
            /*if (totalAmount - hasAmount > 0.0) {
                lending_data.add(Loaner(name, Loan(totalAmount - hasAmount, hasAmount, totalAmount, LoanerType.LENDING)))
            }
            else {
                lended_data.add(Loaner(name, Loan(0.0, hasAmount, totalAmount, LoanerType.LENDING)))
            }*/
            totalAmount = 0.0
            hasAmount = 0.0
        }
    }

}