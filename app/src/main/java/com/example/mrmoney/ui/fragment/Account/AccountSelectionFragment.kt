package com.example.mrmoney.ui.fragment.Account

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.adapter.AccountRVAdapter
import com.example.mrmoney.adapter.DataModel
import com.example.mrmoney.data.model.entity.Wallet
import com.example.mrmoney.databinding.AccountSelectionFragmentBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class AccountSelectionFragment : Fragment() {

    companion object {
        fun newInstance() = AccountSelectionFragment()
    }

    private lateinit var viewModel: AccountSelectionViewModel

    private var _binding: AccountSelectionFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var manager: RecyclerView.LayoutManager
    private lateinit var navController: NavController
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = AccountSelectionFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var data = listOf(
            DataModel("1","1000000"),
            DataModel("2","2000000"),
            DataModel("3","3000000"),
            DataModel("4","4000000"),
            DataModel("5","4000000"),
            DataModel("6","4000000"),
            DataModel("7","5000000")
        )

        manager = LinearLayoutManager(context)
        navController = findNavController()
        var accountSelectionViewModel =
            ViewModelProvider(requireActivity()).get(AccountSelectionViewModel::class.java)
                    as AccountSelectionViewModel

        Firebase.database.reference.child("wallets")
            .child(FirebaseAuth.getInstance().uid.toString())
            .addValueEventListener(object : ValueEventListener {
                var mListWallet = ArrayList<Wallet>()
                override fun onDataChange(snapshot: DataSnapshot) {
                    for(ds: DataSnapshot in snapshot.children){
                        ds.getValue(Wallet::class.java)?.let {
                            mListWallet!!.add(it)
                            Log.d("SonDebug", "" + it.toString())
                        }
                    }

                    val adapter = AccountRVAdapter(mListWallet,0)
                    binding.rcvAccountSelection.adapter = adapter
                    binding.rcvAccountSelection.layoutManager = manager
                    adapter.setOnItemClickListener(object: AccountRVAdapter.onItemClickListener{

                        override fun onItemClick(item: Any) {
                            item as Wallet
                            Log.i("Hahaha", "You clicked on: " + item)
//                navController.navigate(R.id.action_accountFragment_to_detailAccountFragment)
                            accountSelectionViewModel.setSelectedWallet(item)
                            navController.popBackStack()
                        }

                        override fun onMoreVertClick(item: Any) {
                            item as Wallet
                            Log.i("Hahaha", "You clicked on: " + item)
//                navController.navigate(R.id.action_accountFragment_to_detailAccountFragment)
                            accountSelectionViewModel.setSelectedWallet(item)
                            navController.popBackStack()
                        }
                    })
                }
                override fun onCancelled(error: DatabaseError) {
                    Log.d("SonDebug", "failed")
                }

            })

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(AccountSelectionViewModel::class.java)
        // TODO: Use the ViewModel
    }

}