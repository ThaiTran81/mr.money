package com.example.mrmoney.ui.fragment.loan

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.adapter.LoanerChooserRVAdapter
import com.example.mrmoney.data.model.LoanerType
import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.data.model.entity.LoanDetail
import com.example.mrmoney.data.model.entity.Transaction
import com.example.mrmoney.databinding.FragmentLoanerChooserBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.example.mrmoney.databinding.LoanDetailBinding
import com.facebook.gamingservices.GameRequestDialog.show
import com.facebook.share.widget.CreateAppGroupDialog.show

class LoanerChooserFragment: Fragment() {

    companion object {
        fun newInstance() = LoanerChooserFragment()
    }

    private val database = FirebaseDatabase.getInstance()
    private val dbRootRef = database.reference
    private val transactionNode = dbRootRef.child("transactions")
    private val firebaseAuth = FirebaseAuth.getInstance().currentUser
    private lateinit var viewModel: LoanerChooserViewModel
    private var _binding: FragmentLoanerChooserBinding? = null
    private val binding get() = _binding!!
    private lateinit var manager: RecyclerView.LayoutManager

    private lateinit var navController: NavController
    private lateinit var itemDecoration: RecyclerView.ItemDecoration

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLoanerChooserBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(LoanerChooserViewModel::class.java)
        itemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
    navController = findNavController()
        manager = LinearLayoutManager(context)

        loadDataFireBase()


        Log.d("thong","loan trong loanfrag ${viewModel.toString()}")
        binding.imgCheck.setOnClickListener {
            val name = binding.loanerACTV.text.toString().trim()
                viewModel.setSelected(name)
            navController.popBackStack()
        }

    }

    private fun loadDataFireBase() {
        val loanerType = LoanerChooserFragmentArgs.fromBundle(requireArguments()).loanerType
        transactionNode.child(firebaseAuth!!.uid)
            .addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                var setPerson: HashSet<String> = HashSet<String>()
                for(dss : DataSnapshot in snapshot.children) {
                    val temp = dss.getValue(Transaction::class.java)
                    if(temp!!.person != null) {
                        when(loanerType) {
                            "Cho vay" -> if(temp!!.transactionType.equals(TransactionType.LEND.name)) setPerson.add(temp!!.person)
                            "Đi vay" -> if(temp!!.transactionType.equals(TransactionType.BORROW.name)) setPerson.add(temp!!.person)
                        }

                    }
                }

                if(!setPerson.isEmpty()) {
                    var data = ArrayList<String>()
                    for(i in setPerson) {
                        data.add(i)
                    }
                    val data1 = data.distinct()

                    var adapter = LoanerChooserRVAdapter(data1)

                    adapter.onItemClick = { loaner ->
                        viewModel.setSelected(loaner)
                        navController.popBackStack()
                    }

                    binding.loanerChooserRV.apply {
                        this.adapter = adapter
                        layoutManager = manager
                        addItemDecoration(itemDecoration)
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Log.d("thong", "${error.details}")
            }
        })
    }

    fun setData(data :Array<LoanDetail>, loanerType: String): ArrayList<String> {
        var names = ArrayList<String>()
        var removeItem = ArrayList<String>()

        if (loanerType == "Cho vay") {
            binding.loanerTypeTV.text = "Người vay"
            for (item in data) {
                if (item.loaner == LoanerType.LENDING) {
                    names.add(item.name)
                    removeItem.add(item.name)
                }
            }
        }
        else {
            binding.loanerTypeTV.text = "Người cho vay"
            for (item in data) {
                if (item.loaner == LoanerType.BORROWING) {
                    names.add(item.name)
                    removeItem.add(item.name)
                }
            }
        }
        names = names.distinct() as ArrayList<String>
        removeItem = removeItem.distinct() as ArrayList<String>


        /*if (loanerName != null) {
            for (item in removeItem) {
                if (!item.contains(loanerName, ignoreCase = true)) {
                    names.remove(item)
                }
            }
        }*/

        return names
    }

}