package com.example.mrmoney.ui.fragment.Account.TabBankBook

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mrmoney.R

class DetailBankBookFragment : Fragment() {

    companion object {
        fun newInstance() = DetailBankBookFragment()
    }

    private lateinit var viewModel: DetailBankBookViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.detail_bank_book_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DetailBankBookViewModel::class.java)
        // TODO: Use the ViewModel
    }

}