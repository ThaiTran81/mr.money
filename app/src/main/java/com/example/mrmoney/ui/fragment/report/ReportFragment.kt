package com.example.mrmoney.ui.fragment.report

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.adapter.ReportAdapter
import com.example.mrmoney.data.model.entity.Report
import com.example.mrmoney.databinding.FragmentReportBinding


class ReportFragment: Fragment() {

    companion object {
        fun newInstance() = ReportFragment()
    }

    private lateinit var viewModel: ReportViewModel
    private lateinit var binding: FragmentReportBinding
    private lateinit var manager: RecyclerView.LayoutManager

    private lateinit var navController: NavController
    private lateinit var toolbar: androidx.appcompat.widget.Toolbar

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentReportBinding.inflate(layoutInflater)
        return  binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var data = Report.createReportMenu()

        manager = GridLayoutManager(context, 2)
        var adapter = ReportAdapter(data)

        navController = Navigation.findNavController(view)

        adapter.onItemClick = { report ->
            if (report.name == "Tài chính hiện tại") {
                navController.navigate(R.id.action_reportFragment_to_currentFinancesFragment)
            }
            else if (report.name == "Tình hình thu chi") {
                navController.navigate(ReportFragmentDirections.actionReportFragmentToRevenueAndExpenditureSituationFragment(0))
            }
            else if (report.name == "Phân tích chi tiêu") {
                navController.navigate(R.id.action_reportFragment_to_spendingAnalysisFragment)
            }
            else if (report.name == "Phân tích thu") {
                navController.navigate(R.id.action_reportFragment_to_revenueAnalysisFragment)
            }
            else if (report.name == "Theo dõi vay nợ") {
                navController.navigate(R.id.action_reportFragment_to_loanFragment)
            }
        }

        binding.reportRV.apply {
            this.adapter = adapter
            layoutManager = manager
        }


    }

//    private fun replaceFragment(fragment: Fragment) {
//        var fragmentManager: FragmentManager = parentFragmentManager
//        var fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
//        fragmentTransaction.replace(R.id.nav_host_fragment, fragment)
//        fragmentTransaction.commit()
//    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ReportViewModel::class.java)


        // TODO: Use the ViewModel
    }


//    lateinit var reports: ArrayList<Report>
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.fragment_report)
//        // Lookup the recyclerview in activity layout
//        val rvReports = findViewById<RecyclerView>(R.id.contactsRV) as RecyclerView
//        // Initialize contacts
//        contacts = Contact.createContactsList(20)
//        // Create adapter passing in the sample user data
//        val adapter = ContactsAdapter(contacts)
//        // Attach the adapter to the recyclerview to populate items
//        rvContacts.adapter = adapter
//        // Set layout manager to position the items
//        rvContacts.layoutManager =  GridLayoutManager(this, 2) //LinearLayoutManager(this)
//    }
}