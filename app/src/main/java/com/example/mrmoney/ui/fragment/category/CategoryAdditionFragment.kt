package com.example.mrmoney.ui.fragment.category

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.navArgs
import com.example.mrmoney.data.model.entity.Category
import com.example.mrmoney.data.respository.CategoryRespository
import com.example.mrmoney.databinding.FragmentCategoryEditorBinding

class CategoryAdditionFragment :Fragment(){
    private var _binding: FragmentCategoryEditorBinding? = null
    private val binding get() = _binding!!
    private lateinit var navController: NavController
    val args: CategoryAdditionFragmentArgs by navArgs<CategoryAdditionFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCategoryEditorBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.editAppBar.visibility = View.INVISIBLE
        Log.d("ignoreson", args.typeCategory)
        binding.addAccountAppbar.categoryEditorNameTV.text = "Thêm hạng mục chi tiêu"
        binding.addAccountAppbar.imgCancel.setOnClickListener {
            navController.popBackStack()
        }
        binding.addAccountAppbar.imgCheck.setOnClickListener {
            var newCategory = Category(args.typeCategory,binding.categoryNameEditorTV.text.toString(),"categoryIconEditorImgV")
            Toast.makeText(requireContext(),"Thêm hạng mục thành công", Toast.LENGTH_SHORT).show()
            CategoryRespository.getInstance().addCategory(newCategory)
            navController.popBackStack()
        }



    }
}