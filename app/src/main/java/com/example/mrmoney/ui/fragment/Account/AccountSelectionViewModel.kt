package com.example.mrmoney.ui.fragment.Account

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mrmoney.data.model.entity.Wallet

class AccountSelectionViewModel : ViewModel() {
    private val selectedWallet: MutableLiveData<Wallet> = MutableLiveData()


    public fun setSelectedWallet(data:Wallet){
        selectedWallet.value = data
    }

    public fun getSelectedWallet(): LiveData<Wallet> {
        return selectedWallet
    }
}