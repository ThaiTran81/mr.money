package com.example.mrmoney.ui.fragment.Account.TabBankBook

import android.accounts.Account
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.adapter.AccountRVAdapter
import com.example.mrmoney.adapter.DataModel
import com.example.mrmoney.databinding.TabAccountFragmentBinding

class TabBankBookFragment : Fragment() {

    companion object {
        fun newInstance() = TabBankBookFragment()
    }

    private lateinit var viewModel: TabBankBookViewModel
    private lateinit var manager: RecyclerView.LayoutManager
    private lateinit var navController: NavController

    private var  _binding: TabAccountFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = TabAccountFragmentBinding.inflate(inflater, container, false)
//        _binding = TabBankBookFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var data = listOf(
            DataModel("1","7000000"),
            DataModel("2","6000000"),
            DataModel("3","5000000"),
            DataModel("4","4000000"),
            DataModel("5","3000000"),
            DataModel("6","2000000"),
            DataModel("7","1000000")
        )

        manager = LinearLayoutManager(context)

        navController = findNavController()

        val adapter = AccountRVAdapter(data,1)
        binding.tabAccountRcv.layoutManager = manager
        binding.tabAccountRcv.adapter = adapter
        adapter.setOnItemClickListener(object: AccountRVAdapter.onItemClickListener{

            override fun onItemClick(item: Any) {
                navController.navigate(R.id.action_accountFragment_to_detailBankBookFragment)
            }

            override fun onMoreVertClick(item: Any) {
                TODO("Not yet implemented")
            }


        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(TabBankBookViewModel::class.java)
        // TODO: Use the ViewModel
    }

}