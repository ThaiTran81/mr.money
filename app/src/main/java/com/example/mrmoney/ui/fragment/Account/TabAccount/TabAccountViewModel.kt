package com.example.mrmoney.ui.fragment.Account.TabAccount

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.mrmoney.data.model.entity.Wallet
import com.example.mrmoney.utils.CurrencyUltils
import java.util.*

class TabAccountViewModel : ViewModel() {
    var wallet = MutableLiveData<Wallet>()
    val money: LiveData<String>
    get() = Transformations.map(wallet,){
        CurrencyUltils.moneyFormatter(it.balance, "VND")
    }
}