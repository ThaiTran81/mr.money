package com.example.mrmoney.ui.fragment.report

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.mrmoney.data.model.entity.Wallet
import com.example.mrmoney.utils.CurrencyUltils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.launch

class CurrentFinanceViewModel(application: Application) : AndroidViewModel(application) {
    var totalBalace = MutableLiveData<String?>()
    var wallets = MutableLiveData<ArrayList<Wallet>>()
    private val database = FirebaseDatabase.getInstance()
    private val dbRootRef = database.reference
    private val firebaseAuth = FirebaseAuth.getInstance()
    private var currentUser = firebaseAuth.currentUser

    init {
        wallets.value = null
        getTotal()
    }

    fun getTotal() {
        viewModelScope.launch {
            dbRootRef.child("wallets")
                .child(currentUser!!.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        var total: Double = 0.0
                        val arr = ArrayList<Wallet>()
                        if (snapshot.exists()) {
                            for (ds: DataSnapshot in snapshot.children) {
                                var temp = ds.getValue(Wallet::class.java)
                                arr.add(temp!!)
                                total += temp!!.balance.toDouble()
                            }
                        }
                        wallets.value = arr
                        totalBalace.value = CurrencyUltils.moneyFormatter(total, "VND")
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.d("thong", "${error.details}")
                    }
                })
        }

    }
}