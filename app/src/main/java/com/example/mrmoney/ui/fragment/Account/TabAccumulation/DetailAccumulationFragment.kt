package com.example.mrmoney.ui.fragment.Account.TabAccumulation

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.adapter.DataModel
import com.example.mrmoney.adapter.HistoryAccountRVAdapter
import com.example.mrmoney.databinding.DetailAccountFragmentBinding
import com.example.mrmoney.databinding.DetailAccumulationFragmentBinding

class DetailAccumulationFragment : Fragment() {

    private var _binding: DetailAccumulationFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var manager: RecyclerView.LayoutManager
    private lateinit var viewModel: DetailAccumulationViewModel

    companion object {
        fun newInstance() = DetailAccumulationFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DetailAccumulationFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var data = listOf(
            DataModel("1","1000000"),
            DataModel("2","2000000"),
            DataModel("3","3000000"),
            DataModel("4","4000000"),
            DataModel("5","5000000")
        )

        manager = LinearLayoutManager(context)
//
//        binding.testRcv.apply {
//            adapter = AccountRVAdapter(data)
//            layoutManager = manager
//        }


        val adapter = HistoryAccountRVAdapter(data,1)
        binding.detailAccountRv.layoutManager = manager
        binding.detailAccountRv.adapter = adapter

        adapter.setOnItemClickListener(object: HistoryAccountRVAdapter.onItemClickListener{
            override fun onItemClick(position: Int) {
                TODO("Not yet implemented")
            }

        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DetailAccumulationViewModel::class.java)
        // TODO: Use the ViewModel
    }

}