package com.example.mrmoney.ui.fragment.home

import android.app.Application
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.mrmoney.R
import com.example.mrmoney.data.model.DateFilterType
import com.example.mrmoney.data.model.ExchangeRate
import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.data.model.entity.*
import com.example.mrmoney.data.respository.DBConnection
import com.example.mrmoney.utils.CurrencyUltils
import com.example.mrmoney.utils.DateUtils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.launch
import java.text.NumberFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.temporal.TemporalAdjusters.firstDayOfYear
import java.time.temporal.TemporalAdjusters.lastDayOfYear
import java.time.temporal.TemporalField
import java.time.temporal.WeekFields
import java.util.*


@RequiresApi(Build.VERSION_CODES.O)
class HomeViewModel(application: Application) : AndroidViewModel(application) {
    private val database = FirebaseDatabase.getInstance()
    private val dbRootRef = database.reference
    private val firebaseAuth = FirebaseAuth.getInstance()
    private var currentUser = firebaseAuth.currentUser
    var totalBalace = MutableLiveData<String?>()
    var transactions = MutableLiveData<ArrayList<Transaction?>>()
    lateinit var statsTimes: Menu
    var user = MutableLiveData<User?>()
    val countryCode = "VN"

    var revenueTotal = MutableLiveData<Double>()
    var expenseTotal = MutableLiveData<Double>()

    var expenseCurYearTotal = MutableLiveData<Double>()


    var expCurYeartransactions = MutableLiveData<ArrayList<Transaction?>>()


    private var _navigateToRevenueAndExpenditure = MutableLiveData<MenuItem?>()


    val navigateToRevenueAndExpenditure: MutableLiveData<MenuItem?>
        get() = _navigateToRevenueAndExpenditure

    var exchangeLst = MutableLiveData<List<ExchangeRate>?>()

    var showTotal = MutableLiveData<Boolean>()

    var dateFilter = MutableLiveData<LocalDate>()

    var selectedFilter = MutableLiveData<Int>()

    var firstDayOfYear = MutableLiveData<LocalDate>()
    var lastDayOfYear = MutableLiveData<LocalDate>()


    init {

        totalBalace.value = CurrencyUltils.moneyFormatter(0.0,"VND")

        val menu = Menu()
        val context = getApplication<Application>()
        showTotal.value = true
        menu.add(
            DateFilterType.TODAY.ordinal,
            context.resources.getString(R.string.today),
            null
        )
        menu.add(
            DateFilterType.THIS_WEEK.ordinal,
            context.resources.getString(R.string.this_week),
            null
        )
        menu.add(
            DateFilterType.THIS_MONTH.ordinal,
            context.resources.getString(R.string.this_month),
            null
        )
        menu.add(
            DateFilterType.THIS_YEAR.ordinal,
            context.resources.getString(R.string.this_year),
            null
        )
        statsTimes = menu

//        base on menuid above
        selectedFilter.value = DateFilterType.TODAY.ordinal
        Log.d("thong", "-------------")
        initExchangeLookup()
        getTotal()
        getTransactions()
        getTransactionsByCurrentYear()
    }

    fun onShowBalanceClick() {
        showTotal.value = showTotal.value == false
    }

    fun initProfileUser() {
        viewModelScope.launch {
            FirebaseDatabase.getInstance().reference.child("users")
                .child(FirebaseAuth.getInstance().currentUser!!.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        if (snapshot.exists()) {
                            user.value = snapshot.getValue(User::class.java)!!
                            Log.d("HomeFragment", "user${user.value!!.userName}")
                        }

                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.d("HomeFragment", "${error.details}")
                    }
                })
        }

    }

    fun onFilterDateClick(menuItem: MenuItem) {
        _navigateToRevenueAndExpenditure.value = menuItem
    }

    fun onRevenueAndExpenditureNavigated() {
        _navigateToRevenueAndExpenditure.value = null
    }

    fun initExchangeLookup() {
        exchangeLst.value =
            DBConnection.getInstance(getApplication()).currencyExchangeDAO.getOnlyVisible()
        if (exchangeLst.value!!.size == 0) {
            exchangeLst.value =
                DBConnection.getInstance(getApplication()).currencyExchangeDAO.getLimit(5)
        }

        Log.i("HomeFragment", exchangeLst.value!!.size.toString())

    }

    fun getBaseRate(): ExchangeRate? {
//        for(i in 0 until exchangeLst.value!!.size){
//            println(exchangeLst.value!!.get(i))
//            if (exchangeLst.value!!.get(i).countryCode.equals(countryCode, true))
//                return exchangeLst.value!!.get(i)
//        }

        return ExchangeRate("VN", "VND", 24152.525053, 0)
    }

    fun moneyFormatter(amount: Double, currencyCode: String): String {
        val format: NumberFormat = NumberFormat.getCurrencyInstance()
        format.maximumFractionDigits = 0
        format.currency = Currency.getInstance(currencyCode)
        return format.format(amount)
    }

    fun getTotal() {
        viewModelScope.launch {
            dbRootRef.child("wallets")
                .child(currentUser!!.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        var total: Double = 0.0
                        if (snapshot.exists()) {
                            for (ds: DataSnapshot in snapshot.children) {
                                var temp = ds.getValue(Wallet::class.java)
                                total += temp!!.balance.toDouble()
                            }
                        }
                        totalBalace.value = CurrencyUltils.moneyFormatter(total, "VND")
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.d("thong", "${error.details}")
                    }
                })
        }

    }

    fun getTransactions() {
        viewModelScope.launch {
            dbRootRef.child("transactions")
                .child(currentUser!!.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        var arr = ArrayList<Transaction?>()
                        if (snapshot.exists()) {
                            for (ds: DataSnapshot in snapshot.children) {
                                var temp = ds.getValue(Transaction::class.java)
                                arr.add(temp)
                                Log.i("HomeFagment", "get transaction sucess")
                            }
                            filterTransactionByDate(arr)
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.d("thong", "${error.details}")
                    }
                })

        }
    }


    fun filterTransactionByDate(lst: ArrayList<Transaction?>?) {
        when (selectedFilter.value) {
            DateFilterType.TODAY.ordinal -> filterByRange(lst, LocalDate.now(), LocalDate.now())
            DateFilterType.THIS_WEEK.ordinal -> {
                val now = LocalDate.now()
                val fieldISO: TemporalField = WeekFields.of(Locale.getDefault()).dayOfWeek()
                filterByRange(lst, now.with(fieldISO, 1), now.with(fieldISO, 1).plusDays(6))
            }
            DateFilterType.THIS_MONTH.ordinal -> {
                val now = LocalDate.now()
                val start = now.withDayOfMonth(1)
                val end = now.withDayOfMonth(now.month.length(now.isLeapYear))
                filterByRange(lst, start, end)
            }
            DateFilterType.THIS_YEAR.ordinal -> {
                val now = LocalDate.now()
                val firstDay = now.with(firstDayOfYear())
                val lastDay = now.with(lastDayOfYear())

                filterByRange(lst, firstDay, lastDay)
            }
        }
    }


    fun filterByRange(lst: ArrayList<Transaction?>?, from: LocalDate, to: LocalDate) {
        val arr = lst

        if (arr != null) {
            var expense = 0.0
            var revenue = 0.0
            for (it in arr) {
                val date = DateUtils.covertTimestampToDatetime(it!!.time)

                Log.i("HomeFragment", date.toString())
                if (date.toLocalDate().isBefore(from) && date.toLocalDate().isAfter(to)) {
                    arr!!.remove(it)
                    continue
                }
                if (it.transactionType.equals(TransactionType.EXPENSE.name))
                    expense += it.amount
                if (it.transactionType.equals(TransactionType.INCOME.name))
                    revenue += it.amount
            }

            revenueTotal.value = revenue
            expenseTotal.value = expense
            transactions.value = arr
        } else transactions.value = null

    }

    fun onDateFilterClick(menuItem: MenuItem) {
        selectedFilter.value = menuItem.id
        transactions.value = null
        expenseTotal.value = 0.0
        revenueTotal.value = 0.0
        getTransactions()
    }

    fun initDataExpense(arr: ArrayList<Transaction?>?) {
        val now = LocalDate.now()
        firstDayOfYear.value = now.with(firstDayOfYear())
        lastDayOfYear.value = now.with(lastDayOfYear())

        if (arr != null) {
            var expense = 0.0
            for (it in arr) {
                val date = DateUtils.covertTimestampToDatetime(it!!.time)

                Log.i("HomeFragment", date.toString())
                if (date.toLocalDate().isBefore(firstDayOfYear.value) && date.toLocalDate()
                        .isAfter(lastDayOfYear.value)
                ) {
                    arr!!.remove(it)
                    continue
                }
                if (it.transactionType.equals(TransactionType.EXPENSE.name))
                    expense += it.amount

            }
            expenseCurYearTotal.value = expense
            expCurYeartransactions.value = arr
        } else expCurYeartransactions.value = null
    }

    fun getTransactionsByCurrentYear() {
        viewModelScope.launch {
            dbRootRef.child("transactions")
                .child(currentUser!!.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        var arr = ArrayList<Transaction?>()
                        if (snapshot.exists()) {
                            for (ds: DataSnapshot in snapshot.children) {
                                var temp = ds.getValue(Transaction::class.java)
                                arr.add(temp)
                                Log.i("HomeFagment", "get transaction sucess")
                            }
                            initDataExpense(arr)
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.d("thong", "${error.details}")
                    }
                })
        }
    }
}
