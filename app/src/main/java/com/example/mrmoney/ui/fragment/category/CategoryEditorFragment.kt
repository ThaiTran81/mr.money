package com.example.mrmoney.ui.fragment.category

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavArgs
import androidx.navigation.NavController
import androidx.navigation.fragment.navArgs
import com.example.mrmoney.data.model.entity.Category
import com.example.mrmoney.data.respository.CategoryRespository
import com.example.mrmoney.databinding.FragmentCategoryEditorBinding
import com.example.mrmoney.databinding.FragmentCategorySelectionBinding
import com.example.mrmoney.databinding.FragmentManagementCategoryBinding
import com.example.mrmoney.ui.fragment.Account.TabAccount.DetailAccountFragmentArgs

class CategoryEditorFragment: Fragment() {
    private var _binding: FragmentCategoryEditorBinding? = null
    private val binding get() = _binding!!
    private lateinit var navController: NavController
    val args: CategoryEditorFragmentArgs by navArgs<CategoryEditorFragmentArgs>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCategoryEditorBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.d("CategorySon", args.category.toString())
        binding.addAccountAppbar.categoryEditorNameTV.setText("Sửa hạng mục chi tiêu ")
//        binding.categoryIconEditorImgV
        binding.categoryNameEditorTV.setText(args.category!!.name)
        binding.btnSave.setOnClickListener {
            var newCategory = Category(args.category!!.transactionType, binding.categoryNameEditorTV.text.toString(),args.category!!.iconID)
            CategoryRespository.getInstance().updateCategory(args.category!!, newCategory)
            Toast.makeText(requireContext(), "Thành công", Toast.LENGTH_SHORT).show()
        }

        binding.btnDelete.setOnClickListener{
            CategoryRespository.getInstance().deleteCategory(args.category!!)
            Toast.makeText(requireContext(), "Xóa thành công", Toast.LENGTH_SHORT).show()
        }

        binding.addAccountAppbar.imgCheck.visibility = View.INVISIBLE
        binding.addAccountAppbar.imgCancel.setOnClickListener {
            navController.popBackStack()
        }
    }
}