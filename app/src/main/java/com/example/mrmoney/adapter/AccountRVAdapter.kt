package com.example.mrmoney.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.data.model.entity.Wallet
import com.example.mrmoney.databinding.ItemTabAccountBinding
import com.example.mrmoney.databinding.ItemTabAccumulationBinding
import com.example.mrmoney.databinding.ItemTabBankBookBinding
import com.example.mrmoney.utils.CurrencyUltils

//class AccountRVAdapter(private val data: List<Any>): RecyclerView.Adapter<AccountRVAdapter.AccountViewHolder>()
class AccountRVAdapter(private val data: List<Any>, var type: Int)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var mListener : onItemClickListener
    interface onItemClickListener{
        fun onItemClick(item: Any)
        fun onMoreVertClick(item: Any)
    }

    fun setOnItemClickListener(listener: onItemClickListener){

        mListener = listener

    }

    inner class AccountViewHolder(val binding: ItemTabAccountBinding, listener: onItemClickListener): RecyclerView.ViewHolder(binding.root){
        fun bind(item: Wallet){
            binding.wallet = item;
        }

        init {
            binding.cvItemTabAccount.setOnClickListener{
                listener.onItemClick(data[adapterPosition])

            }
            binding.imgMoreVert.setOnClickListener {
                listener.onMoreVertClick(data[adapterPosition])
            }

        }
    }

    inner class BankBookViewHolder(val binding: ItemTabBankBookBinding, listener: onItemClickListener):RecyclerView.ViewHolder(binding.root){
        fun bind(item: DataModel){
            binding.listAccountSource = item;
        }

        init {
            binding.cvItemBankBook.setOnClickListener{
                listener.onItemClick(data[adapterPosition])
                listener.onMoreVertClick(data[adapterPosition])
            }
        }
    }

    inner class AccumulationViewHolder(val binding: ItemTabAccumulationBinding, listener: onItemClickListener) :RecyclerView.ViewHolder(binding.root){
        fun bind(item: DataModel){
            binding.listAccountSource = item;
        }

        init {
            binding.cvAccumulation.setOnClickListener{
                listener.onItemClick(data[adapterPosition])
                listener.onMoreVertClick(data[adapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        when(type){
            0->{
                val listAccountSourceBinding = ItemTabAccountBinding.inflate(inflater, parent,false)
                return AccountViewHolder(listAccountSourceBinding, mListener)
            }
            1->{
                val listBankBookSourceBinding  = ItemTabBankBookBinding.inflate(inflater, parent, false)
                return BankBookViewHolder(listBankBookSourceBinding, mListener)
            }
            else->{
                val listAccumulationSourceBinding  = ItemTabAccumulationBinding.inflate(inflater, parent, false)
                return AccumulationViewHolder(listAccumulationSourceBinding, mListener)
            }
        }
    }

//    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {
//        holder.bind(data[position])
//
//    }

    override fun getItemCount(): Int {
        if(data == null){
            return 0
        }
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var myHolder: RecyclerView.ViewHolder

        when(type ){
            0 -> {
                myHolder = holder as AccountViewHolder
                myHolder.bind(data[position] as Wallet)
            }
            1 -> {
                myHolder = holder as BankBookViewHolder
                myHolder.bind(data[position] as DataModel)
            }
            2 -> {
                myHolder = holder as AccumulationViewHolder
                myHolder.bind(data[position] as DataModel)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }
}