package com.example.mrmoney.adapter

import android.content.Context
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.data.model.entity.Category
import com.example.mrmoney.data.model.entity.MenuItem


class CheckboxCategoryAdapter(val data: ArrayList<Category?>?, val itemStateArray: SparseBooleanArray) :
    RecyclerView.Adapter<CheckboxCategoryAdapter.ViewHolder>() {
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_list_checkbox, parent, false)

        return CheckboxCategoryAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val category = data!!.get(position)
        holder.ivCateIcon.setImageDrawable(context.getDrawable(R.drawable.cate_bitcoin))
        holder.tvCate.text = category!!.name

        if (itemStateArray.get(position, false)) {
            holder.checkBox.setChecked(true);
            itemStateArray.put(position, true);
        }
        else  {
            holder.checkBox.setChecked(false);
            itemStateArray.put(position, false);
        }


        holder.itemView.setOnClickListener {
            if (!itemStateArray.get(position, false)) {
                holder.checkBox.setChecked(true);
                itemStateArray.put(position, true);
            }
            else  {
                holder.checkBox.setChecked(false);
                itemStateArray.put(position, false);
            }
        }
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivCateIcon = view.findViewById<ImageView>(R.id.ivIconCate)
        val tvCate = view.findViewById<TextView>(R.id.tvCategoryName)
        val checkBox = view.findViewById<CheckBox>(R.id.checkBox)
    }

}