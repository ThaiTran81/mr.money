package com.example.mrmoney.adapter

import android.app.Activity
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.data.model.entity.Category
import com.example.mrmoney.databinding.ItemChildCategoryBinding


class CategoryRVAdapter(private val data: List<Category>, var activity: Activity)
    : RecyclerView.Adapter<CategoryRVAdapter.CategoryViewHolder>()  {

    interface onItemClicklistener{
        fun onItemClick(item: Category)
    }


    private lateinit var mListener:onItemClicklistener

    fun setOnItemClickListener(listener: onItemClicklistener){

        mListener = listener

    }

    inner class CategoryViewHolder(val binding: ItemChildCategoryBinding, listener: onItemClicklistener, val activity: Activity): RecyclerView.ViewHolder(binding.root){
        fun bind(item: Category){
//            var uri = "@drawable/"+ item.iconID
//            var imageResource = activity.resources.getIdentifier(uri, null, activity.packageName)
//            var res = activity.resources.getDrawable(imageResource)
//            Log.d("categoryRVAdapter", "" + res)
//            binding.civCategory.setImageDrawable(res)
            binding.category = item
            val fnm = item.iconID //  this is image file name

            val PACKAGE_NAME: String = activity.getApplicationContext().getPackageName()
            val imgId: Int = activity.getResources().getIdentifier("$PACKAGE_NAME:drawable/$fnm",
                null,
                null)
//    Bitmap bitmap = BitmapFactory.decodeResource(getResources(),imgId);
//    Bitmap bitmap = BitmapFactory.decgodeResource(getResources(),imgId);
            binding.civCategory.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), imgId))



        }
        init {
            binding.llCategoryChild.setOnClickListener {
                listener.onItemClick(data[adapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CategoryRVAdapter.CategoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val listCategorySourceBinding = ItemChildCategoryBinding.inflate(inflater, parent, false)
        return CategoryViewHolder(listCategorySourceBinding, mListener, activity)
    }

    override fun onBindViewHolder(holder: CategoryRVAdapter.CategoryViewHolder, position: Int) {
        val myHolder = holder
        myHolder.bind(data[position])
    }

    override fun getItemCount(): Int {
        if(data == null){
            return 0
        }
        return data.size
    }


}