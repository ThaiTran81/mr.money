package com.example.mrmoney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.data.model.entity.MenuItem

class UtilAdapter(private val data: List<MenuItem>, val onClickListener: OnClickListener): RecyclerView.Adapter<UtilAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):UtilAdapter.ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_util_list, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: UtilAdapter.ViewHolder, position: Int) {
        val menuItem = data.get(position)
        holder.ivUtilIcon.setImageDrawable(context.getDrawable(menuItem.icon!!))
        holder.tvUtil.text = menuItem.label

        holder.itemView.setOnClickListener {
            onClickListener.onClick(menuItem)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val ivUtilIcon = view.findViewById<ImageView>(R.id.ivUtilIcon)
        val tvUtil = view.findViewById<TextView>(R.id.tvUtil)
    }

    class OnClickListener(val clickListener: (menuItem:MenuItem) -> Unit) {
        fun onClick(menuItem:MenuItem) = clickListener(menuItem)
    }
}