package com.example.mrmoney.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.data.model.entity.SavingDeposit

class SavingDepositAdapter(private val items : List<SavingDeposit>) :
    RecyclerView.Adapter<SavingDepositAdapter.ViewHolder>() {

    var onItemClick: ((SavingDeposit) -> Unit)? = null

    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val iconImageView = listItemView.findViewById<ImageView>(R.id.savingDepositItemIconImgV)
        val labelTextView = listItemView.findViewById<TextView>(R.id.savingDepositItemLabelTV)
        val contentTextView = listItemView.findViewById<TextView>(R.id.savingDepositItemDesTV)

        init {
            listItemView.setOnClickListener{ onItemClick?.invoke(items[adapterPosition]) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            SavingDepositAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val reportView = inflater.inflate(R.layout.item_list_saving_deposit, parent, false)
        // Return a new holder instance
        return ViewHolder(reportView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: SavingDepositAdapter.ViewHolder, position: Int) {
        // Get the data model based on position
        val item: SavingDeposit = items.get(position)
        // Set item views based on your views and data model
        val imageView = holder.iconImageView
        imageView.setImageResource(item.icon!!)
        val textViewLabel = holder.labelTextView
        textViewLabel.setText(item.label)
        val textViewContent = holder.contentTextView
        textViewContent.setText(item.content)
    }

}