package com.example.mrmoney.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.data.model.entity.Report


class ReportAdapter(private val reports : List<Report>) :
    RecyclerView.Adapter<ReportAdapter.ViewHolder>() {

    var onItemClick: ((Report) -> Unit)? = null

    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val iconImageView = listItemView.findViewById<ImageView>(R.id.iconImgV)
        val nameTextView = listItemView.findViewById<TextView>(R.id.nameTV)

        init {
            listItemView.setOnClickListener{ onItemClick?.invoke(reports[adapterPosition]) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ReportAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val reportView = inflater.inflate(R.layout.fragment_report_list_item, parent, false)
        // Return a new holder instance
        return ViewHolder(reportView)
    }
    override fun getItemCount(): Int {
        return reports.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Get the data model based on position
        val report: Report = reports.get(position)
        // Set item views based on your views and data model
        val imageView = holder.iconImageView
        imageView.setImageResource(report.icon!!)
        val textView = holder.nameTextView
        textView.setText(report.name)
    }
}