package com.example.mrmoney.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.data.model.entity.LoanDetail
import com.example.mrmoney.data.model.entity.Loaner
import java.text.FieldPosition

class LoanerChooserRVAdapter(private val loaners: List<String>) :
    RecyclerView.Adapter<LoanerChooserRVAdapter.ViewHolder>() {

    var onItemClick: ((String) -> Unit)? = null
    var onBtnClick: ((String) -> Unit)? = null

    inner class ViewHolder(listItemView: View): RecyclerView.ViewHolder(listItemView) {
        val iconImageView = listItemView.findViewById<TextView>(R.id.loanerChooserIconTV)
        val nameTextView = listItemView.findViewById<TextView>(R.id.loanerChooserNameTV)

        init {
            listItemView.setOnClickListener{ onItemClick?.invoke(loaners[adapterPosition] as String) }
            val editImgViewTemp = listItemView.findViewById<ImageView>(R.id.loanerChooserEditImgV)
            editImgViewTemp.setOnClickListener{
                onBtnClick?.invoke(loaners[adapterPosition] as String)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            LoanerChooserRVAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val loanerChooserRecyclerView = inflater.inflate(R.layout.item_list_loaner_chooser, parent, false)
        // Return a new holder instance
        return ViewHolder(loanerChooserRecyclerView)
    }

    override fun getItemCount(): Int {
        return loaners.size
    }

    override fun onBindViewHolder(holder: LoanerChooserRVAdapter.ViewHolder, position: Int) {
        // Get the data model based on position
        val loaner: String = loaners.get(position) as String
        // Set item views based on your views and data model
        val textViewIcon = holder.iconImageView
        textViewIcon.setText(loaner.get(0).toUpperCase().toString())
        val textViewName = holder.nameTextView
        textViewName.setText(loaner)
    }
}