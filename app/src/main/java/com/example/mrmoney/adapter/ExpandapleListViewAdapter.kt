package com.example.mrmoney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.mrmoney.R
import com.example.mrmoney.data.model.entity.Category
import com.google.api.Distribution

class ExpandapleListViewAdapter(private val context:Context
                                , private val parentCate: ArrayList<Category>
                                , private val childCate: HashMap<String,ArrayList<Category>>):
                                BaseExpandableListAdapter() {
    override fun getGroupCount(): Int {
        return parentCate.size
    }

    override fun getChildrenCount(p0: Int): Int {
        return if(childCate.containsKey(parentCate[p0].name)) {
            childCate[parentCate[p0].name]!!.size
        } else{
            0
        }
    }

    override fun getGroup(p0: Int): Any {
        return parentCate[p0].name
    }

    override fun getChild(p0: Int, p1: Int): Any {
        return childCate[parentCate[p0].name]!![p1].name
    }

    override fun getGroupId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getChildId(p0: Int, p1: Int): Long {
        return p1.toLong()
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    interface HandleOnItemClickListener{
        fun onItemChildClick(position: Int)
    }

    private lateinit var mListener : HandleOnItemClickListener


    fun setOnItemClickListener(listener: HandleOnItemClickListener){

        mListener = listener

    }

    override fun getGroupView(p0: Int, p1: Boolean, p2: View?, p3: ViewGroup?): View {

        var convertView = p2
        val parenCateTitle = getGroup(p0) as String

        if (convertView == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.item_parent_category, null)

        }

        val parentCateTv = convertView!!.findViewById<TextView>(R.id.tvNameCategory)
        parentCateTv.setText(parenCateTitle)

        return convertView

    }

    override fun getChildView(p0: Int, p1: Int, p2: Boolean, p3: View?, p4: ViewGroup?): View {
        var convertView = p3
        val childCateTitle = getChild(p0, p1) as String

        if (convertView == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.item_child_category, null)

        }

        val childCateTv = convertView!!.findViewById<TextView>(R.id.tvNameCategory)
        childCateTv.setText(childCateTitle)

        return convertView
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean {
        return true
    }
}