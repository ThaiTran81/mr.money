package com.example.mrmoney.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.databinding.SlideBinding
import com.example.mrmoney.data.model.SlideSplash

class SlideAdapter: RecyclerView.Adapter<SplashViewHolder>() {
    var list: List<SlideSplash> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SplashViewHolder {
        val view = SlideBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SplashViewHolder(view)
    }

    override fun onBindViewHolder(holder: SplashViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setItems(newList: List<SlideSplash>) {
        list = newList
        notifyDataSetChanged()
    }

}

class SplashViewHolder(private val binding: SlideBinding): RecyclerView.ViewHolder(binding.root) {
    fun bind(slideContent: SlideSplash) {
        binding.content = slideContent
    }
}