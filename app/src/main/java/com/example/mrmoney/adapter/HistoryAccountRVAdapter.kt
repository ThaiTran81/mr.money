package com.example.mrmoney.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.data.model.entity.Transaction
import com.example.mrmoney.databinding.*

class HistoryAccountRVAdapter (private val data: List<Any>, val type: Int)
    :RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var mListener : onItemClickListener
    interface onItemClickListener{
        fun onItemClick(position: Int)
    }

    fun setOnItemClickListener(listener: onItemClickListener){

        mListener = listener

    }

    inner class HistoryAccountViewHolder(val binding: CardItemDetailAccountHistoryBinding, listener: HistoryAccountRVAdapter.onItemClickListener)
        : RecyclerView.ViewHolder(binding.root){
        fun bind(item: Transaction){
            binding.transaction = item;
        }

        init {
            binding.cvAccountHistory.setOnClickListener{
                listener.onItemClick(adapterPosition)
            }
        }
    }

    inner class HistoryAccumulationViewHolder(val binding: CardItemDetailAccumulationHistoryBinding, listener: HistoryAccountRVAdapter.onItemClickListener)
        : RecyclerView.ViewHolder(binding.root){
        fun bind(item: DataModel){
            binding.listAccountSource = item
        }

        init {
            binding.cvAccumulationHistory.setOnClickListener{
                listener.onItemClick(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
    : RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        when(type){
            0->{
                val listAccountHistoryBinding = CardItemDetailAccountHistoryBinding.inflate(inflater, parent,false)
                return HistoryAccountViewHolder(listAccountHistoryBinding, mListener)
            }
            else ->{
                val listAccumulationHistoryBinding  = CardItemDetailAccumulationHistoryBinding.inflate(inflater, parent, false)
                return HistoryAccumulationViewHolder(listAccumulationHistoryBinding, mListener)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var myHolder: RecyclerView.ViewHolder

        when(type ){
            0 -> {
                myHolder = holder as HistoryAccountRVAdapter.HistoryAccountViewHolder
                myHolder.bind(data[position] as Transaction)
            }
            else -> {
                myHolder = holder as HistoryAccountRVAdapter.HistoryAccumulationViewHolder
                myHolder.bind(data[position] as DataModel)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    override fun getItemCount(): Int {
        return data.size
    }
}