package com.example.mrmoney.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.data.model.entity.Loan
import com.example.mrmoney.data.model.entity.Loaner
import com.example.mrmoney.data.model.entity.Report
import com.example.mrmoney.databinding.ItemTabAccountBinding
import com.example.mrmoney.databinding.LoanParticipantListItemBinding
import com.example.mrmoney.databinding.TabLendingFragmentBinding
import com.example.mrmoney.utils.CurrencyUltils
import java.text.NumberFormat
import java.util.*

class LoanerAdapter(private val loaners: List<Loaner>) :
    RecyclerView.Adapter<LoanerAdapter.ViewHolder>() {

    var onItemClick: ((Loaner) -> Unit)? = null

    inner class ViewHolder(listItemView: View): RecyclerView.ViewHolder(listItemView) {
        val iconImageView = listItemView.findViewById<TextView>(R.id.loan_participant_iconTV)
        val nameTextView = listItemView.findViewById<TextView>(R.id.loan_participant_nameTV)
        val valueTextView = listItemView.findViewById<TextView>(R.id.loan_participant_valueTV)

        init {
            listItemView.setOnClickListener{ onItemClick?.invoke(loaners[adapterPosition]) }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LoanerAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val loanerView = inflater.inflate(R.layout.loan_participant_list_item, parent, false)
        // Return a new holder instance
        return ViewHolder(loanerView)
    }

    override fun onBindViewHolder(holder: LoanerAdapter.ViewHolder, position: Int) {
            // Get the data model based on position
            val loaner: Loaner = loaners.get(position)
            Log.d("thong","${loaners.toString()}")
            // Set item views based on your views and data model
            val imageView = holder.iconImageView
            imageView.setText(loaner.name.get(0).toUpperCase().toString())
            val textView = holder.nameTextView
            textView.setText(loaner.name)
            val valueView = holder.valueTextView
            if (loaner.isFinish) {
                valueView.setText("")
            } else {
                valueView.setText(CurrencyUltils.moneyFormatter(loaner.amount, "VND"))
            }

    }

    override fun getItemCount(): Int {
        return loaners.size
    }

}


