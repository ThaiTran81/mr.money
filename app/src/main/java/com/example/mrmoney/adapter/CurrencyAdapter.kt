package com.example.mrmoney.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.blongho.country_data.World
import com.example.mrmoney.R
import com.example.mrmoney.data.model.ExchangeRate
import com.example.mrmoney.utils.CurrencyUltils
import com.google.android.material.imageview.ShapeableImageView


class CurrencyAdapter(val data: List<ExchangeRate>,val context: Context) : BaseAdapter() {
    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return data.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflter = (LayoutInflater.from(context));
        val view = inflter.inflate(R.layout.item_spiner_currency, null)
        val icon = view.findViewById<ShapeableImageView>(R.id.ivCountry)
        val text = view.findViewById<TextView>(R.id.tvCurrency)
        icon.setImageResource(World.getFlagOf(data.get(position).countryCode))
        text.text = "${data.get(position).countryCode}(${CurrencyUltils.getCurrencySymbol(data.get(position).countryCode)})"
        return view
    }
}