package com.example.mrmoney.adapter

data class DataModel(val sourceName: String, val balance: String)