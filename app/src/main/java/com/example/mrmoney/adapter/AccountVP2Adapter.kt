package com.example.mrmoney.adapter

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.mrmoney.ui.fragment.Account.TabAccount.TabAccountFragment
import com.example.mrmoney.ui.fragment.Account.TabAccumulation.TabAccumulationFragment
import com.example.mrmoney.ui.fragment.Account.TabBankBook.TabBankBookFragment

public class AccountVP2Adapter(fragmentActivity: FragmentActivity?) :
    FragmentStateAdapter(fragmentActivity!!) {

    override fun getItemCount(): Int {

        return 3

    }

    override fun createFragment(position: Int): Fragment {
        var fragment:Fragment
        if(position == 0){
            fragment = TabAccountFragment.newInstance()

            Log.d("Hahaha", "Tài khoản")

        }else if(position==1){
            fragment = TabBankBookFragment.newInstance()
            Log.d("Hahaha", "Sổ tiết kiệm")

        }else{
            fragment = TabAccumulationFragment.newInstance()
            Log.d("Hahaha", "Sổ tích lũy")

        }
        return fragment
    }




}
