package com.example.mrmoney.adapter

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.mrmoney.ui.fragment.category.income.ManagementIncomeCategoryFragment
import com.example.mrmoney.ui.fragment.category.expense.ManagementExpenseCategoryFragment

class CategoryVP2Adapter (fragmentActivity: FragmentActivity?) :
    FragmentStateAdapter(fragmentActivity!!) {

    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        var fragment: Fragment
        if(position == 0){
            fragment = ManagementIncomeCategoryFragment()
            Log.d("CategoryDebug", "0")

        }else {
            Log.d("CategoryDebug", "1")
            fragment = ManagementExpenseCategoryFragment()
        }
        Log.d("hahaha", "12345")
        return fragment
    }




}
