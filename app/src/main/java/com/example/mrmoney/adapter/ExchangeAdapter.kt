package com.example.mrmoney.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.blongho.country_data.World
import com.example.mrmoney.R
import com.example.mrmoney.data.model.ExchangeRate
import com.example.mrmoney.utils.CurrencyUltils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class ExchangeAdapter(val baseRate: ExchangeRate?, val data: List<ExchangeRate> ): RecyclerView.Adapter<ExchangeAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExchangeAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val view = inflater.inflate(R.layout.item_exchange_lookup, parent, false)

        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val tvUnit = view.findViewById<TextView>(R.id.tvCurrencyUnit)
        val tvUnitCode = view.findViewById<TextView>(R.id.tvCurrencyUnitCode)
        val tvExchange = view.findViewById<TextView>(R.id.tvExchange)
        val ivFlag = view.findViewById<ImageView>(R.id.ivFlag)

        fun bind(item: ExchangeRate) {
            tvUnitCode.text = item.currencyCode
            tvUnit.text = CurrencyUltils.getCurrencySymbol(item.countryCode)
            tvExchange.text = "1 ${item.currencyCode}  ≈ ${Math.floor(baseRate!!.rate!! / item.rate!!)} ${baseRate.currencyCode} "
            ivFlag.setImageResource(World.getFlagOf(item.countryCode))
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data.get(position)
        holder.bind(item)
    }

}