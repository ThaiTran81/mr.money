package com.example.mrmoney.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.mrmoney.ui.fragment.loan.borrow.TabBorrowingFragment
import com.example.mrmoney.ui.fragment.loan.lend.TabLendingFragment

public class LoanVP2Adapter(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity!!) {

    override fun getItemCount(): Int {
        return 2;
    }

    override fun createFragment(position: Int): Fragment {
        var fragment: Fragment
        if(position == 0){
            fragment = TabLendingFragment.newInstance()

        } else{
            fragment = TabBorrowingFragment.newInstance()
        }
        return fragment
    }
}