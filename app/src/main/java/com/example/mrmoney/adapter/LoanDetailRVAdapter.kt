package com.example.mrmoney.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mrmoney.R
import com.example.mrmoney.data.model.LoanType
import com.example.mrmoney.data.model.entity.LoanDetail
import com.example.mrmoney.utils.CurrencyUltils
import java.text.NumberFormat
import java.util.*

class LoanDetailRVAdapter(private val activities : List<LoanDetail>) :
    RecyclerView.Adapter<LoanDetailRVAdapter.ViewHolder>() {

    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val iconImageView = listItemView.findViewById<ImageView>(R.id.iconImgV)
        val typeTextView = listItemView.findViewById<TextView>(R.id.loanDetailTypeTV)
        val dateTextView = listItemView.findViewById<TextView>(R.id.loanDetailDateTV)
        val loanDetailDescriptionTextView = listItemView.findViewById<TextView>(R.id.loanDetailDescriptionTV)
        val loanDetailAmount = listItemView.findViewById<TextView>(R.id.loanDetailAmountTV)
        val loanDetailAccountTextView = listItemView.findViewById<TextView>(R.id.loanDetailAccountTV)

        init {

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            LoanDetailRVAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val loanDetailRecyclerView = inflater.inflate(R.layout.loan_detail_list_item, parent, false)
        // Return a new holder instance
        return ViewHolder(loanDetailRecyclerView)
    }

    override fun getItemCount(): Int {
        return activities.size
    }

    override fun onBindViewHolder(holder: LoanDetailRVAdapter.ViewHolder, position: Int) {
        // Get the data model based on position
        val loanDetail: LoanDetail = activities.get(position)
        // Set item views based on your views and data model
        val imageView = holder.iconImageView
        val textViewType = holder.typeTextView
        val textViewDate = holder.dateTextView
        val textViewDescription = holder.loanDetailDescriptionTextView
        val textViewAmount = holder.loanDetailAmount
        val textViewAccount = holder.loanDetailAccountTextView

        when (loanDetail.type) {
            LoanType.BORROW -> {
                imageView.setImageResource(R.drawable.ic_cate_di_vay)
                textViewType.setText("Đi vay")
            }
            LoanType.BORROW_PAY -> {
                imageView.setImageResource(R.drawable.ic_cate_tra_no)
                textViewType.setText("Trả nợ")
                textViewAmount.setTextColor(Color.parseColor("#FF3378"))
            }
            LoanType.LEND -> {
                imageView.setImageResource(R.drawable.ic_cate_cho_vay)
                textViewType.setText("Cho vay")
                textViewAmount.setTextColor(Color.parseColor("#FF3378"))
            }
            LoanType.LEND_PAY -> {
                imageView.setImageResource(R.drawable.ic_cate_thu_no)
                textViewType.setText("Thu nợ")
            }
        }
        textViewDate.setText(loanDetail.date)
        textViewDescription.setText(loanDetail.description)
        textViewAmount.setText(CurrencyUltils.moneyFormatter(loanDetail.amount, "VND"))
        textViewAccount.setText(loanDetail.wallet)
    }

}