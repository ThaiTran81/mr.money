package com.example.mrmoney.data.model.entity

import com.example.mrmoney.data.model.LoanType
import com.example.mrmoney.data.model.LoanerType
import java.text.DateFormat
import java.util.*
import kotlin.collections.ArrayList

data class LoanDetail(
    val name: String,
    val date: String,
    val description: String,
    val amount: Double,
    val wallet: String,
    val type: LoanType,
    val loaner: LoanerType
) {
    companion object {
        fun createDataSample() : ArrayList<LoanDetail> {
            val loanDetails = ArrayList<LoanDetail>()

            loanDetails.add(LoanDetail("TA", DateFormat.getDateInstance().format(Date("01/05/2022")), "Trả nợ TA", 200000.0, "Ví", LoanType.BORROW_PAY, LoanerType.BORROWING))
            loanDetails.add(LoanDetail("TA", DateFormat.getDateInstance().format(Date("02/05/2022")), "Trả nợ TA", 50000.0, "Ví", LoanType.BORROW_PAY, LoanerType.BORROWING))

            loanDetails.add(LoanDetail("NTT", DateFormat.getDateInstance().format(Date("01/05/2022")), "Vay NTT", 50000.0, "Ví", LoanType.BORROW, LoanerType.BORROWING))

            loanDetails.add(LoanDetail("NVA", DateFormat.getDateInstance().format(Date("29/04/2022")), "Trả nợ NVA", 60000.0, "Ví", LoanType.BORROW_PAY, LoanerType.BORROWING))
            loanDetails.add(LoanDetail("NVA", DateFormat.getDateInstance().format(Date("28/04/2022")), "Trả nợ NVA", 20000.0, "Ví", LoanType.BORROW_PAY, LoanerType.BORROWING))

            loanDetails.add(LoanDetail("TA", DateFormat.getDateInstance().format(Date("28/04/2022")), "Vay TA", 300000.0, "Ví", LoanType.BORROW, LoanerType.BORROWING))

            loanDetails.add(LoanDetail("NTT", DateFormat.getDateInstance().format(Date("28/04/2022")), "NTT trả nợ", 20000.0, "Ví", LoanType.LEND_PAY, LoanerType.LENDING))
            loanDetails.add(LoanDetail("NTT", DateFormat.getDateInstance().format(Date("28/04/2022")), "Cho NTT vay", 30000.0, "Ví", LoanType.LEND, LoanerType.LENDING))
            loanDetails.add(LoanDetail("NTT", DateFormat.getDateInstance().format(Date("26/04/2022")), "Cho NTT vay", 50000.0, "Ví", LoanType.LEND, LoanerType.LENDING))
            loanDetails.add(LoanDetail("NTT", DateFormat.getDateInstance().format(Date("25/04/2022")), "NTT trả nợ", 30000.0, "Ví", LoanType.LEND_PAY, LoanerType.LENDING))

            loanDetails.add(LoanDetail("ABC", DateFormat.getDateInstance().format(Date("25/04/2022")), "ABC trả nợ", 50000.0, "Ví", LoanType.LEND_PAY, LoanerType.LENDING))
            loanDetails.add(LoanDetail("ABC", DateFormat.getDateInstance().format(Date("25/04/2022")), "Cho ABC vay", 400000.0, "Ví", LoanType.LEND, LoanerType.LENDING))

            loanDetails.add(LoanDetail("XCT", DateFormat.getDateInstance().format(Date("25/04/2022")), "XCT trả nợ", 40000.0, "Ví", LoanType.LEND_PAY, LoanerType.LENDING))
            loanDetails.add(LoanDetail("XCT", DateFormat.getDateInstance().format(Date("25/04/2022")), "Cho XCT vay", 40000.0, "Ví", LoanType.LEND, LoanerType.LENDING))

            loanDetails.add(LoanDetail("NVA", DateFormat.getDateInstance().format(Date("25/04/2022")), "Vay NVA", 80000.0, "Ví", LoanType.BORROW, LoanerType.BORROWING))
            loanDetails.add(LoanDetail("NVA", DateFormat.getDateInstance().format(Date("24/04/2022")), "Trả nợ NVA", 20000.0, "Ví", LoanType.BORROW_PAY, LoanerType.BORROWING))
            loanDetails.add(LoanDetail("NVA", DateFormat.getDateInstance().format(Date("24/04/2022")), "Vay NVA", 20000.0, "Ví", LoanType.BORROW, LoanerType.BORROWING))

            loanDetails.add(LoanDetail("NTT", DateFormat.getDateInstance().format(Date("24/04/2022")), "NTT trả nợ", 50000.0, "Ví", LoanType.LEND_PAY, LoanerType.LENDING))
            loanDetails.add(LoanDetail("NTT", DateFormat.getDateInstance().format(Date("24/04/2022")), "Cho NTT vay", 40000.0, "Ví", LoanType.LEND, LoanerType.LENDING))
            loanDetails.add(LoanDetail("NTT", DateFormat.getDateInstance().format(Date("24/04/2022")), "Cho NTT vay", 50000.0, "Ví", LoanType.LEND, LoanerType.LENDING))

            return  loanDetails
        }
    }
}