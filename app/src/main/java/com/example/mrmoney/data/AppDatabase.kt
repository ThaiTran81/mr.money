package com.example.mrmoney.data

import android.util.Log
import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.data.model.entity.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import java.util.concurrent.CountDownLatch

class AppDatabase private constructor() {
    private val database = FirebaseDatabase.getInstance()
    private val dbRootRef = database.reference
    private val userNode = dbRootRef.child("users")
    private val walletNode = dbRootRef.child("wallets")
    private val transactionNode = dbRootRef.child("transactions")
    private val categoryNode = dbRootRef.child("categories")
    private var result: Boolean = false
    private var UID: String = ""

    init {
        UID = Firebase.auth.currentUser!!.uid
    }

    companion object {
        private var appDatabase: AppDatabase? = null

        fun getInstance(): AppDatabase {
            if (appDatabase == null)
                appDatabase =
                    AppDatabase()
            return appDatabase!!
        }
    }


    private fun getWalletFromDB() {
        walletNode.child(UID).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (ds : DataSnapshot in snapshot.children) {
                    ds.getValue(Wallet::class.java)?.let {
                        var temp = it
                        temp.walletId = ds.key.toString()

                    }
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Log.d("thong", "${error.details}")
            }
        })
    }

     fun getCategoryFromDB() {
        categoryNode.child(UID).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (ds : DataSnapshot in snapshot.children) {
                    ds.getValue(Category::class.java)?.let {
                        var temp = it
                        temp.name = ds.key.toString()
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Log.d("thong", "${error.details}")
            }
        })
    }

    private  fun getTransactionFromDB() {
        transactionNode.child(UID).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (ds : DataSnapshot in snapshot.children) {
                    val walletKey = ds.key.toString()
                    for(dss : DataSnapshot in ds.children) {
                        dss.getValue(Transaction::class.java)?.let {
                            var temp = it
                            temp.transactionID = dss.key.toString()
                            temp.walletID = walletKey
                        }
                    }
                }
            }
            override fun onCancelled(error: DatabaseError) {
                Log.d("thong", "${error.details}")
            }
        })
    }

    fun updateWallet(wallet: Wallet) {
        walletNode.child(UID).child(wallet.walletId).setValue(wallet)
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    Log.d("thong" ,"update wallet finish")

                }else{
                    Log.d("thong","update wallet faild ${task.exception}")
                }
            }
    }


    fun addWallet(wallet: Wallet) : Boolean {
        val key = walletNode.child(UID).push().key
        result = false
        if(key != null) {
            wallet.walletId = key
            walletNode.child(UID).child(key).setValue(wallet)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        Log.d("thong","add wallet success")
                    }else{
                        Log.d("thong","add wallet failure")
                    }
                }
        }
        return result
    }

    fun deleteWallet(wallet: Wallet) : Boolean {
        result = false
        walletNode.child(UID).child(wallet.walletId).removeValue()
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    Log.d("thong","Delete wallet successfully!!!")
                }else{
                    Log.d("thong","Delete wallet failure ${task.exception}")
                }
            }
        return result
    }

    fun getTransactionByTime(from: java.util.Date, to: java.util.Date): ArrayList<Transaction>? {

        return null
    }

    fun getTransactionByCategory(category: String): ArrayList<Transaction>? {
            transactionNode.child(UID).addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (ds : DataSnapshot in snapshot.children) {
                        val walletKey = ds.key.toString()
                        for(dss : DataSnapshot in ds.children) {
                            dss.getValue(Transaction::class.java)?.let {
                                if(it.category.equals(category)) {
                                    var temp = it
                                    temp.transactionID = dss.key.toString()
                                    temp.walletID = walletKey
                                }
                            }
                        }
                    }
                }
                override fun onCancelled(error: DatabaseError) {
                    Log.d("thong", "${error.details}")
                }
            })
        return  null
    }

    fun getTransactionByType(transactionType: TransactionType): ArrayList<Transaction>? {
            transactionNode.child(UID).addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (ds : DataSnapshot in snapshot.children) {
                        val walletKey = ds.key.toString()
                        for(dss : DataSnapshot in ds.children) {
                            dss.getValue(Transaction::class.java)?.let {
                                if(it.transactionType.equals(transactionType.name)) {
                                    var temp = it
                                    temp.transactionID = dss.key.toString()
                                    temp.walletID = walletKey
                                }
                            }
                        }
                    }
                }
                override fun onCancelled(error: DatabaseError) {
                    Log.d("thong", "${error.details}")
                }
            })
        return  null
    }

    fun getTransactionByWallet(walletID: String): ArrayList<Transaction>? {
            transactionNode.child(UID).child(walletID).addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    for(dss : DataSnapshot in snapshot.children) {
                        dss.getValue(Transaction::class.java)?.let {
                            var temp = it
                            temp.transactionID = dss.key.toString()
                            temp.walletID = walletID
                        }
                    }
                }
                override fun onCancelled(error: DatabaseError) {
                    Log.d("thong", "${error.details}")
                }
            })
        return  null
    }

    fun addTransaction(transaction: Transaction) {
        result = false
        val key = transactionNode.child(UID).push().key
        if(key != null) {
            transaction.transactionID = key
            transactionNode.child(UID).child(key).setValue(transaction)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        Log.d("thong","add transaction success")
                        walletNode.child(UID)
                            .child(transaction.walletID)
                            .get()
                            .addOnCompleteListener{ it ->
                                if(it.isSuccessful) {

                                    val wallet = it.getResult().getValue(Wallet::class.java)
                                    Log.d("thong"," 1 - ${wallet.toString()}")
                                    if (wallet != null) {
                                        when(transaction.transactionType) {
                                            TransactionType.INCOME.name -> wallet!!.balance += transaction.amount
                                            TransactionType.EXPENSE.name -> wallet!!.balance -= transaction.amount
                                            TransactionType.BORROW.name -> wallet!!.balance += transaction.amount
                                            TransactionType.LEND.name -> wallet!!.balance -= transaction.amount
                                        }
                                        Log.d("thong","2 - ${wallet.toString()}")
                                        updateWallet(wallet)
                                    }
                                }
                            }


                    }else{
                        Log.d("thong","add transaction failure ${task.exception}")
                    }
                }
        }
    }

    fun updateTransaction(transactionOld: Transaction, transactionNew: Transaction) {
        transactionNode.child(UID)
            .child(transactionOld.transactionID).setValue(transactionNew)
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    Log.d("thong","Update transaction successfull!!")
                    var wallet =  walletNode.child(UID)
                        .child(transactionOld.walletID)
                        .get().getResult().getValue(Wallet::class.java)
                    if (wallet != null) {
                        when(transactionOld.transactionType) {
                            TransactionType.INCOME.name -> wallet!!.balance -= transactionOld.amount
                            TransactionType.EXPENSE.name -> wallet!!.balance += transactionOld.amount
                        }
                        when(transactionNew.transactionType) {
                            TransactionType.INCOME.name -> wallet!!.balance += transactionNew.amount
                            TransactionType.EXPENSE.name -> wallet!!.balance -= transactionNew.amount
                        }
                        updateWallet(wallet)
                    }
                }else{
                    Log.d("thong", "Update transaction failure ${task.exception}")
                }
            }
    }

    fun deleteTransaction(transaction: Transaction) {
        transactionNode
            .child(UID)
            .child(transaction.transactionID).removeValue()
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    Log.d("thong","Delete transaction successfully!!!")
                    var wallet =  walletNode.child(UID)
                        .child(transaction.walletID)
                        .get().getResult().getValue(Wallet::class.java)
                    if (wallet != null) {
                        when(transaction.transactionType) {
                            TransactionType.INCOME.name -> wallet!!.balance -= transaction.amount
                            TransactionType.EXPENSE.name -> wallet!!.balance += transaction.amount
                        }
                        updateWallet(wallet)
                    }

                }else{
                    Log.d("thong","Delete transaction failure ${task.exception}")
                }
            }
    }

    fun addCategory(category: Category): Boolean {
        result = false
        categoryNode.child(UID).child(category.name).setValue(category)
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    Log.d("thong","add category successfully!!!")
                    result = true
                }else {
                    Log.d("thong","add category transaction failure ${task.exception}")
                }
            }
        return result
    }

    fun updateCategory(old:Category, new: Category): Boolean {
        result = false
        categoryNode.child(UID).child(old.name).setValue(new)
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    Log.d("thong" ,"update finish")

                }else{
                    Log.d("thong","${task.exception}")
                }
            }
        return result
    }

    fun deleteCategory(category: Category): Boolean {
        result = false
        categoryNode.child(UID).child(category.name).removeValue()
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {

                    Log.d("thong","Delete successfully!!!")
                }else{
                    Log.d("thong","${task.exception}")
                }
            }
        return result
    }


}