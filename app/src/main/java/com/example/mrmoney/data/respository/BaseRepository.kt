package com.example.mrmoney.data.respository

import com.example.mrmoney.data.AppDatabase

abstract class BaseRepository {
    protected val mAppDatabase = AppDatabase.getInstance()
}