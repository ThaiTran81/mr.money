package com.example.mrmoney.data.model.entity

import com.example.mrmoney.data.model.LoanType
import com.example.mrmoney.data.model.LoanerType
import com.example.mrmoney.data.model.TransactionType

data class Loan(
    val mustAmount: Double,
    val hasAmount: Double,
    val totalAmount: Double,
    val type: LoanerType,

    /*val amount: Double = 0.0,
    val person: String =  "",
    //val type: String = TransactionType.LEND.name,
    val paid: Double = 0.0*/
)
