package com.example.mrmoney.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currency_exchange")
data class ExchangeRate(
    @PrimaryKey
    @ColumnInfo(name = "country_code")
    val countryCode: String,
    @ColumnInfo(name = "currency_code")
    val currencyCode: String,
    @ColumnInfo(name = "rate")
    val rate: Double? = 0.0,
    @ColumnInfo(name = "visible")
    val visible: Int? = 0
)
