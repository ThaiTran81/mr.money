package com.example.mrmoney.data.respository

import com.example.mrmoney.data.model.entity.Category

class CategoryRespository private constructor() : BaseRepository() {
    companion object {
        private var instance: CategoryRespository? = null

        fun getInstance(): CategoryRespository {
            if (instance == null)
                instance = CategoryRespository()
            return instance!!
        }
    }

    fun getCategories()  {
        return mAppDatabase.getCategoryFromDB()!!
    }

    fun addCategory(category: Category) : Boolean {
        return mAppDatabase.addCategory(category)
    }

    fun updateCategory(old:Category, new: Category) : Boolean {
        return mAppDatabase.updateCategory(old,new)
    }

    fun deleteCategory(category: Category): Boolean {
        return mAppDatabase.deleteCategory(category)
    }
}