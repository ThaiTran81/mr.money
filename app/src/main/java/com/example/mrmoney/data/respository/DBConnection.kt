package com.example.mrmoney.data.respository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.mrmoney.data.model.ExchangeRate

@Database(entities = [ExchangeRate::class], version = 1, exportSchema = true)
abstract class DBConnection : RoomDatabase() {

    /**
     * Connects the database to the DAO.
     */
    abstract val currencyExchangeDAO: CurrencyExchangeDAO

    /**
     * Define a companion object, this allows us to add functions on the SleepDatabase class.
     *
     * For example, clients can call `SleepDatabase.getInstance(context)` to instantiate
     * a new SleepDatabase.
     */
    companion object {

        @Volatile
        private var INSTANCE: DBConnection? = null

        fun getInstance(context: Context): DBConnection {
            // Multiple threads can ask for the database at the same time, ensure we only initialize
            // it once by using synchronized. Only one thread may enter a synchronized block at a
            // time.
            synchronized(this) {
                // Copy the current value of INSTANCE to a local variable so Kotlin can smart cast.
                // Smart cast is only available to local variables.
                var instance = INSTANCE
                // If instance is `null` make a new database instance.
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        DBConnection::class.java,
                        "mr_money_db"
                    ).createFromAsset("mrMoney_db.db")
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build()
                    // Assign INSTANCE to the newly created database.
                    INSTANCE = instance
                }
                // Return instance; smart cast to be non-null.
                return instance
            }
        }
    }
}