package com.example.mrmoney.data.model.entity

import android.media.Image
import com.example.mrmoney.R

class Report(val name: String? = null, val icon: Int? = null) {
    companion object {
        fun createReportMenu() :ArrayList<Report> {
            val reports = ArrayList<Report>()
            reports.add(Report("Tài chính hiện tại", R.drawable.ic_current_finances))
            reports.add(Report("Tình hình thu chi", R.drawable.ic_revenue_and_expenditure_situation))
            reports.add(Report("Phân tích chi tiêu", R.drawable.ic_spending_analysis))
            reports.add(Report("Phân tích thu", R.drawable.ic_revenue_analysis))
            reports.add(Report("Theo dõi vay nợ", R.drawable.ic_debt_tracking))
            reports.add(Report("Đối tượng thu chi", R.drawable.ic_revenue_and_expenditure_object))
            reports.add(Report("Chuyến đi/Sự kiện", R.drawable.ic_trip_event))
            reports.add(Report("Phân tích tài chính", R.drawable.ic_financial_analysis))
            return  reports
        }
    }
}
