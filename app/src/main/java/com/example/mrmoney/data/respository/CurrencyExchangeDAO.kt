package com.example.mrmoney.data.respository

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.mrmoney.data.model.ExchangeRate

@Dao
public interface CurrencyExchangeDAO {
    @Query("SELECT * FROM currency_exchange")
    fun getAll(): List<ExchangeRate>?

    @Query("SELECT * FROM currency_exchange LIMIT :num")
    fun getLimit(num: Int): List<ExchangeRate>

    @Query("SELECT * FROM currency_exchange WHERE visible>0")
    fun getOnlyVisible(): List<ExchangeRate>?

    @Insert
    fun insert(exchangeRate: ExchangeRate)
}