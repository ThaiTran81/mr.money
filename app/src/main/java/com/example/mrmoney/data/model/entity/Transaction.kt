package com.example.mrmoney.data.model.entity

import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.utils.CurrencyUltils
import com.example.mrmoney.utils.DateUtils
import com.google.type.DateTime

data class Transaction(
    var category: String = "",
    var transactionType: String = TransactionType.EXPENSE.toString(),
    var time: String = "",
    var description: String = "",
    var amount: Double = 0.0,
    var walletID: String = "",
    var person: String = "",
    var transactionID: String = "",
){
    fun formatBalance(): String?{
        return CurrencyUltils.moneyFormatter(amount,"VND")
    }

    fun formatDateTime():String?{
        return DateUtils.timeStampToDateTime(time)
    }
}

