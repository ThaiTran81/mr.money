package com.example.mrmoney.data.model.entity


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FilterDate(var day: Int?, var month: Int?, var year: Int?) : Parcelable{
}