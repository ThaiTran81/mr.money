package com.example.mrmoney.data.model.entity

import com.example.mrmoney.data.model.LoanType

data class Loaner(
    val name: String,
    val amount: Double,
    val isFinish: Boolean = false
)
