package com.example.mrmoney.data.model.entity

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable
import com.example.mrmoney.data.model.TransactionType

@SuppressLint("ParcelCreator")
data class Category(
    var transactionType: String = TransactionType.EXPENSE.name,
    var name: String = "",
    var iconID: String =  ""

) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString()
    ) {
    }

    companion object {
        val BigCategory = arrayListOf(
            Category (
                name = "Ăn uống",
                iconID = "ic_cate_an_uong",
                transactionType = TransactionType.EXPENSE.toString()
            ),
            Category (
                name = "Lương",
                iconID = "ic_cate_luong",
                transactionType = TransactionType.INCOME.toString()
            ),
            Category (
                name = "Dịch vụ sinh hoạt",
                iconID = "ic_cate_dich_vu_sinh_hoat",
                transactionType = TransactionType.EXPENSE.toString()
            ),
            Category (
                name = "Đi lại",
                iconID = "ic_cate_di_lai",
                transactionType = TransactionType.EXPENSE.toString()
            ),
            Category (
                name = "Hường thụ",
                iconID = "ic_cate_huong_thu",
                transactionType = TransactionType.EXPENSE.toString()
            ),
            Category (
                name = "Nhà cửa",
                iconID = "ic_cate_nha_cua",
                transactionType = TransactionType.EXPENSE.toString()
            ),
            Category (
                name = "Sức khỏe",
                iconID = "ic_cate_suc_khoe",
                transactionType = TransactionType.EXPENSE.toString()
            ),
            Category (
                name = "Trang phục",
                iconID = "ic_cate_trang_phuc",
                transactionType = TransactionType.EXPENSE.toString()
            ),
            Category (
                name = "Trả nợ",
                iconID = "ic_cate_tra_no",
                transactionType = TransactionType.EXPENSE.toString()
            ),
            Category (
                name = "Thu nợ",
                iconID = "ic_cate_thu_no",
                transactionType = TransactionType.INCOME.toString()
            ),
            Category (
                name = "Cho vay",
                iconID = "ic_cate_cho_vay",
                transactionType = TransactionType.LEND.toString()
            ),
            Category (
                name = "Đi vay",
                iconID = "ic_cate_di_vay",
                transactionType = TransactionType.BORROW.toString()
            ),
        )

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(transactionType)
        parcel.writeString(name)
        parcel.writeString(iconID)
    }

    override fun describeContents(): Int {
        return 0
    }


}
