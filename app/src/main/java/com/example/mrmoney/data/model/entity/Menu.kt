package com.example.mrmoney.data.model.entity



class Menu {

    private var _menus: ArrayList<MenuItem>

    val menus: ArrayList<MenuItem>
        get() = _menus

    constructor() {
        _menus = ArrayList()
    }

    fun add(id: Int, label: String, icon: Int?) {
        val menuItem = MenuItem(id, label, icon)
        _menus.add(menuItem)
    }
}