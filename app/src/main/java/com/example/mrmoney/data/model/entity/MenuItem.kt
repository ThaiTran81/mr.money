package com.example.mrmoney.data.model.entity

data class MenuItem(var id: Int, var label: String, var icon: Int?){
    override fun toString(): String {
        return label
    }
}