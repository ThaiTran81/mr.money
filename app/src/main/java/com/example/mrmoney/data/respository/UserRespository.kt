package com.example.mrmoney.data.respository

import android.util.Log
import com.example.mrmoney.data.model.entity.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class UserRespository private constructor() : BaseRepository() {
    private var mUser: User? = null
    private var mAuth = Firebase.auth.currentUser


    companion object {
        private var instance: UserRespository? = null

        fun getInstance(): UserRespository {
            if (instance == null)
                instance = UserRespository()
            return instance!!
        }
    }

    fun isChangePassAvailable(): Boolean {
        return mUser!!.authMethod.equals("password")
    }

    fun getCurUser() : User? {
        return mUser
    }

    fun changePassword(newPass: String) {
        mAuth!!.updatePassword(newPass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful){
                    Log.d("thong","changed pass")
                }
            }
    }
}