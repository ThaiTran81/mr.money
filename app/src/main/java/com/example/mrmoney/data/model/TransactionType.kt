package com.example.mrmoney.data.model

enum class TransactionType {
    INCOME, EXPENSE, LEND, BORROW
}