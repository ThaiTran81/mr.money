package com.example.mrmoney.data.model.entity

data class User(
    var userID: String = "",
    var email: String = "",
    var userName: String = "",
    var authMethod: String = "password",
    var active: Boolean = false
)
