package com.example.mrmoney.data.model

enum class LoanType {
    BORROW, LEND, BORROW_PAY, LEND_PAY
}