package com.example.mrmoney.data.model

import java.util.Currency

data class OurCurrency(
    val code: String,
    val name: String,
    val symbol: String
) {
    constructor(fiatCurrency: Currency) : this(
        code = fiatCurrency.currencyCode,
        name = fiatCurrency.displayName,
        symbol = fiatCurrency.symbol
    )
}
