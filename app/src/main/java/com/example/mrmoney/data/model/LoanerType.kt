package com.example.mrmoney.data.model

enum class LoanerType {
    LENDING, BORROWING
}