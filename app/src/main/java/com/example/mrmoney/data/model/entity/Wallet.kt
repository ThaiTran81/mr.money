package com.example.mrmoney.data.model.entity

import android.os.Parcel
import android.os.Parcelable
import androidx.lifecycle.Transformations
import com.example.mrmoney.utils.CurrencyUltils

data class Wallet(
    var name     : String =  "",
    var balance  : Double = 0.0,
    var iconID   : Int    =   0,
    var description: String = "",
    var walletId : String =  ""


) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readDouble(),
        parcel.readInt(),
        parcel.readString().toString()
    ) {
    }

    fun formatBalance(): String{
        return CurrencyUltils.moneyFormatter(balance,"VND")
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeDouble(balance)
        parcel.writeInt(iconID)
        parcel.writeString(walletId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Wallet> {
        override fun createFromParcel(parcel: Parcel): Wallet {
            return Wallet(parcel)
        }

        override fun newArray(size: Int): Array<Wallet?> {
            return arrayOfNulls(size)
        }
    }

}
