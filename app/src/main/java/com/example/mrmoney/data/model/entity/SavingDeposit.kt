package com.example.mrmoney.data.model.entity

import android.media.Image
import com.example.mrmoney.R

class SavingDeposit(val icon: Int? = null, val label: String? = null, val content: String? = null, val type: Int) {
    companion object {
        fun createSampleData() :ArrayList<SavingDeposit> {
            val items = ArrayList<SavingDeposit>()

            items.add(SavingDeposit(R.drawable.ic_money_final, "Tính số tiền nhận được cuối kỳ", "Nhập số tiền gốc cần gửi và thời gian tích lũy, bạn sẽ biết số tiền nhận được cuối kỳ", 0))
            items.add(SavingDeposit(R.drawable.ic_money_per_month, "Tính số tiền gốc cần gửi", "Nhập số tiền bạn muốn đạt được trong tương lai và thời gian tích lũy, bạn sẽ biết số tiền gốc cần gửi", 1))

            return  items
        }
    }
}
