package com.example.mrmoney.data.model

enum class AuthProviderType {
    IVY, GOOGLE
}