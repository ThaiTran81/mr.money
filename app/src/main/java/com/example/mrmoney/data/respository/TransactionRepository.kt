package com.example.mrmoney.data.respository

import com.example.mrmoney.data.model.TransactionType
import com.example.mrmoney.data.model.entity.Transaction
import java.util.*
import kotlin.collections.ArrayList

class TransactionRepository private constructor() : BaseRepository() {
    companion object {
        private var instance: TransactionRepository? = null

        fun getInstance(): TransactionRepository {
            if (instance == null)
                instance = TransactionRepository()
            return instance!!
        }
    }

    fun getTransaction() {
        return
    }

    fun getTransactionByWallet(walletID: String): ArrayList<Transaction>? {
        return mAppDatabase.getTransactionByWallet(walletID)
    }

    fun getTransactionByType(transactionType: TransactionType): ArrayList<Transaction>? {
        return mAppDatabase.getTransactionByType(transactionType)
    }

    fun getTransactionByCategory(category: String): ArrayList<Transaction>? {
        return mAppDatabase.getTransactionByCategory(category)
    }

    fun getTransactionByTime(from: Date, to: Date): ArrayList<Transaction>? {
        return mAppDatabase.getTransactionByTime(from,to)
    }

    fun addNewTransaction(transaction: Transaction)  {
        mAppDatabase.addTransaction(transaction)
    }

    fun updateTransaction(transactionOld: Transaction, transactionNew: Transaction)  {
         mAppDatabase.updateTransaction(transactionOld,transactionNew)
    }

    fun deleteTransaction(transaction: Transaction) {
         mAppDatabase.deleteTransaction(transaction)
    }

}