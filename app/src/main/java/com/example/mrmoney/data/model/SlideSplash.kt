package com.example.mrmoney.data.model

import android.graphics.drawable.Drawable

data class SlideSplash(
    val image: Drawable,
    val header: String,
    val description: String)
