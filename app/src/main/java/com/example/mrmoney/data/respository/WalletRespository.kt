package com.example.mrmoney.data.respository

import android.util.Log
import com.example.mrmoney.data.model.entity.Wallet

class WalletRespository private constructor() : BaseRepository() {

    companion object {
        private var instance: WalletRespository? = null

        fun getInstance(): WalletRespository {
            if (instance == null)
                instance = WalletRespository()
            return instance!!
        }
    }

    fun getWalletList() : ArrayList<Wallet>? {
        return null
    }

    fun isDuplicateName(wallet: Wallet) : Boolean {

        return false
    }

    fun updateWallet(wallet: Wallet) {
        mAppDatabase.updateWallet(wallet)
    }

    fun addNewWallet(wallet: Wallet) : Pair<Boolean, String> {
        if(isDuplicateName(wallet)) {
            return Pair(false,"Tên ví đã tồn tại")
        }

        return if( mAppDatabase.addWallet(wallet) ){
            return Pair(true,"Thêm ví thành công")
        }else {
            return Pair(false,"Thêm ví thất bại")
        }
    }

    fun deleteWallet(wallet: Wallet) : Boolean {
        return mAppDatabase.deleteWallet(wallet)
    }
}