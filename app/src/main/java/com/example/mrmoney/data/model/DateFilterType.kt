package com.example.mrmoney.data.model

enum class DateFilterType {
    TODAY, THIS_WEEK, THIS_MONTH, THIS_YEAR
}