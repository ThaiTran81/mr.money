package com.example.mrmoney

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mrmoney.adapter.CheckboxCategoryAdapter
import com.example.mrmoney.databinding.FragmentChoseCategoryBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentChoseCategory.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentChoseCategory : Fragment() {

    lateinit var binding: FragmentChoseCategoryBinding
    lateinit var viewModel: RevenueAnalysisViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentChoseCategoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(RevenueAnalysisViewModel::class.java)

        viewModel.categories.observe(viewLifecycleOwner, Observer {
            binding.rvCategory.adapter = CheckboxCategoryAdapter(it, viewModel.itemStateArray.value!!)
            binding.rvCategory.layoutManager = LinearLayoutManager(requireContext())
        })

        binding.btnOk.setOnClickListener {
            viewModel.setOnSubmitFilter()
            findNavController().popBackStack()
        }
        binding.btnBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

    }

}