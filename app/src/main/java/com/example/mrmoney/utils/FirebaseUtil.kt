package com.example.mrmoney.utils

import com.example.mrmoney.data.model.entity.User
import com.example.mrmoney.data.respository.CategoryRespository
import com.example.mrmoney.data.respository.TransactionRepository
import com.example.mrmoney.data.respository.UserRespository
import com.example.mrmoney.data.respository.WalletRespository
import com.google.firebase.database.FirebaseDatabase


class FirebaseUtil private constructor() {
    private val mUser: User? = null
    private val userRes = UserRespository.getInstance()
    private val walletRes = WalletRespository.getInstance()
    private val tranasactionRes = TransactionRepository.getInstance()
    private val categoryRes = CategoryRespository.getInstance()

    companion object {
        private var instance: FirebaseUtil? = null

        fun getInstance(): FirebaseUtil {
            if (instance == null)
                instance = FirebaseUtil()
            return instance!!
        }
    }

    fun checkWallet(uid: String, name: String) {

    }

    fun createWallet() {

    }
}