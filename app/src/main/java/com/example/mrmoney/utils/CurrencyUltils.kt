package com.example.mrmoney.utils

import java.text.NumberFormat
import java.util.*

object CurrencyUltils {

        //to retrieve currency code
        fun getCurrencyCode(countryCode: String?): String? {
            return Currency.getInstance(Locale("", countryCode)).getCurrencyCode()
        }

        //to retrieve currency symbol
        fun getCurrencySymbol(countryCode: String?): String? {
            return Currency.getInstance(Locale("", countryCode)).getSymbol()
        }

        fun moneyFormatter(amount: Double, currencyCode: String): String {
            val format: NumberFormat = NumberFormat.getCurrencyInstance()
            format.maximumFractionDigits = 0
            format.currency = Currency.getInstance(currencyCode)
            return format.format(amount)
        }

}