package com.example.mrmoney.utils

import android.os.Build
import androidx.annotation.RequiresApi
import java.security.Timestamp
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

object DateUtils {
    var sdf = SimpleDateFormat("dd/MM/yyyy hh:mm")
    fun timeStampToDateTime(s: String): String? {
        try {
            val netDate = Date((s.toLong()))
            return sdf.format(netDate)
        } catch (e: Exception) {
            return e.toString()
        }
    }

    fun dateTimeToTimeStamp(d: String): String? {
        val date = sdf.parse(d)
        return date.time.toString()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun covertTimestampToDatetime(timestamp: String): LocalDateTime{
        return LocalDateTime.ofInstant(
            Instant.ofEpochMilli(timestamp.toLong()),
            TimeZone.getDefault().toZoneId()
        )
    }

}